<?php

namespace Zeuch\sevDesk\Service;

use GuzzleHttp\Client;

class SevDeskApiClient extends Client {

    /** @var string */
    private $token;

    public function __construct(string $token, array $config = [])
    {
        parent::__construct($config);
        $this->token = $token;
    }

    public function request($method, $uri = '', array $options = [])
    {
        $options['query']['token'] = $this->token;
        return parent::request($method, $uri, $options);
    }
}