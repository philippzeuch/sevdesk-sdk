<?php

namespace Zeuch\sevDesk\Service;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Repo\VoucherPosRepo;
use Zeuch\sevDesk\Repo\VoucherRepo;

class VoucherService
{
    /** @var VoucherRepo */
    private $repo;

    /** @var VoucherPosRepo */
    private $posRepo;

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(VoucherRepo::class);
        $this->posRepo = $c->get(VoucherPosRepo::class);
        $this->container = $c;
    }

}