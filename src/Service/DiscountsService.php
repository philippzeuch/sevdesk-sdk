<?php

namespace Zeuch\sevDesk\Service;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Repo\DiscountsRepo;

class DiscountsService
{
    /** @var DiscountsRepo */
    private $repo;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(DiscountsRepo::class);
    }


}