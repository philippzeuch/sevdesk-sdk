<?php

namespace Zeuch\sevDesk\Service;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Repo\InvoiceRepo;

class InvoiceService
{
    /** @var InvoiceRepo */
    private $repo;

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(InvoiceRepo::class);
        $this->container = $c;
    }

    public function extrahiereRechnungsNummern(?string $string)
    {
        $erkannteRechnungsNummern = array();
        if (!empty($string)) {
            $arr = explode(" ", trim($string));
            foreach ($arr as $item) {
                $matches = array();
                $regex = $this->container->get("SEVDESK_RECHNUNGSNUMMER_REGEX");
                if (!empty($regex) && preg_match("/" . $regex . "/", $item, $matches)) {
                    $erkannteRechnungsNummern[] = strtoupper(str_replace("—", "-", str_replace("–", "-", $matches[0])));
                }
            }
        }

        return $erkannteRechnungsNummern;
    }

    public function extrahiereAuftragsNummern(?string $string)
    {
        $erkannteAuftragsNummern = array();
        if (!empty($string)) {
            $arr = explode(" ", trim($string));
            foreach ($arr as $item) {
                $matches = array();
                $regex = $this->container->get("SEVDESK_AUFTRAGSNUMMER_REGEX");
                if (!empty($regex) && preg_match("/" . $regex . "/", $item, $matches)) {
                    $erkannteAuftragsNummern[] = strtoupper(str_replace("—", "-", str_replace("–", "-", $matches[0])));
                }
            }
        }

        return $erkannteAuftragsNummern;
    }
}