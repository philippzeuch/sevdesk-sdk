<?php

namespace Zeuch\sevDesk\Service;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\CreditNote;
use Zeuch\sevDesk\Utils\BookingCategory;
use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Repo\CreditNotePosRepo;
use Zeuch\sevDesk\Repo\CreditNoteRepo;
use Zeuch\sevDesk\Repo\DiscountsRepo;
use Zeuch\sevDesk\Repo\InvoicePosRepo;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;

class CreditNoteService
{
    /** @var CreditNoteRepo */
    private $repo;

    /** @var CreditNotePosRepo */
    private $posRepo;

    /** @var InvoicePosRepo */
    private $invoicePosRepo;

    /** @var DiscountsRepo */
    private $discountRepo;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(CreditNoteRepo::class);
        $this->posRepo = $c->get(CreditNotePosRepo::class);
        $this->invoicePosRepo = $c->get(InvoicePosRepo::class);
        $this->discountRepo = $c->get(DiscountsRepo::class);
    }

    public function erstelleGutschriftAusRechnung(Invoice $rechnung) :CreditNote
    {
        $gutschrift = $this->repo->createNewInstance();
        $gutschrift->setBookingCategory(BookingCategory::$MINDERLEISTUNG);
        $gutschrift->setRefSrcInvoice($rechnung);
        $gutschrift->setContact($rechnung->getContact());
        $gutschrift->setContactPerson($rechnung->getContactPerson());
        $gutschrift->setAddressName($rechnung->getAddressName());
        $gutschrift->setAddressStreet($rechnung->getAddressStreet());
        $gutschrift->setAddressZip($rechnung->getAddressZip());
        $gutschrift->setAddressCity($rechnung->getAddressCity());
        $gutschrift->setAddressCountry($rechnung->getAddressCountry());
        $gutschrift->setAddress($rechnung->getAddress());

        $gutschrift = $this->repo->saveOrUpdate($gutschrift);

        $i = 1;
        $produkte = $this->invoicePosRepo->findByInvoice($rechnung);
        foreach ($produkte as $produkt) {
            $position = $this->posRepo->createNewInstance();

            $position->setQuantity($produkt->getQuantity());
            $position->setName($produkt->getName());
            $position->setText($produkt->getText());
            $position->setPrice($produkt->getPriceGross());
            $position->setTaxRate($produkt->getTaxRate());
            $position->setUnity($produkt->getUnity());
            $position->setDiscount($produkt->getDiscount());
            $position->setSumDiscount($produkt->getSumDiscount());
            $position->setPositionNumber($i);
            $position->setCreditNote($gutschrift);

            $this->posRepo->saveOrUpdate($position);
            $i++;
        }

        $discounts = $this->discountRepo->findByInvoice($rechnung);
        foreach ($discounts as $discount) {
            $discount->setId(null);
            $discount->setUpdateType(SevDeskUpdateType::NEW);
            $discount->setObject($gutschrift);

            $this->discountRepo->saveOrUpdate($discount);
        }

        return $this->repo->markAsSent($gutschrift);
    }

    /**
     * @param CreditNote[] $gutschriften
     * @return float
     */
    public function summiereBruttoBetragVonMehrerenGutschriften(array $gutschriften): float
    {
        $summe = 0.0;
        foreach ($gutschriften as $gutschrift) {
            $summe = $summe + $gutschrift->getSumGross();
        }
        return $summe;
    }
}