<?php

namespace Zeuch\sevDesk\Service;

use DateTime;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Model\CreditNote;
use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Repo\CheckAccountRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;
use Zeuch\sevDesk\Repo\CreditNoteRepo;
use Zeuch\sevDesk\Repo\InvoiceRepo;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

class CheckAccountService
{
    /** @var CheckAccountRepo */
    protected $repo;

    /** @var CheckAccountTransactionRepo */
    protected $transactionRepo;

    /** @var CheckAccountTransactionService */
    protected $transactionService;

    /** @var InvoiceService */
    protected $invoiceService;

    /** @var InvoiceRepo */
    protected $invoiceRepo;

    /** @var LoggerInterface */
    protected $logger;

    /** @var CreditNoteRepo */
    protected $creditNoteRepo;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(CheckAccountRepo::class);
        $this->transactionRepo = $c->get(CheckAccountTransactionRepo::class);
        $this->transactionService = $c->get(CheckAccountTransactionService::class);
        $this->invoiceService = $c->get(InvoiceService::class);
        $this->invoiceRepo = $c->get(InvoiceRepo::class);
        $this->creditNoteRepo = $c->get(CreditNoteRepo::class);
        $this->logger = $c->get(LoggerInterface::class);
    }

    public function rechnungAufKasseBuchen(Invoice $rechnung, DateTime $timestamp) : ?CheckAccountTransaction
    {
        $kasse = $this->repo->getKasse();
        $this->invoiceRepo->bookAmount($rechnung, $rechnung->getSumGross(), $timestamp, $kasse, null);

        return $this->transactionRepo->findByPaymentPurpose($rechnung->getInvoiceNumber())[0];
    }

    public function gutschriftAufKasseBuchen(CreditNote $gutschrift, DateTime $timestamp) : ?CheckAccountTransaction
    {
        $kasse = $this->repo->getKasse();
        $this->creditNoteRepo->bookAmount($gutschrift, $gutschrift->getSumGross() * -1, $timestamp, $kasse, null);

        return $this->transactionRepo->findByPaymentPurpose($gutschrift->getCreditNoteNumber())[0];
    }

    protected function transaktionAbschliessen(CheckAccountTransaction $transaction)
    {
        try {
            $this->transactionRepo->abschliessen($transaction);
        } catch (GuzzleException $e) {
            throw new Exception("Buchung '" . $transaction->getEntryText() . "' konnte nicht abgeschlossen werden.\\n" . $e->getMessage());
        }
    }
}