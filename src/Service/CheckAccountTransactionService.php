<?php

namespace Zeuch\sevDesk\Service;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Model\Order;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;
use Zeuch\sevDesk\Repo\InvoiceRepo;
use Zeuch\sevDesk\Repo\OrderRepo;

class CheckAccountTransactionService
{
    private $repo;
    private $invoiceRepo;
    private $orderRepo;

    public function __construct(ContainerInterface $c)
    {
        $this->repo = $c->get(CheckAccountTransactionRepo::class);
        $this->orderRepo = $c->get(OrderRepo::class);
        $this->invoiceRepo = $c->get(InvoiceRepo::class);
    }

    public function createNewAndSave(CheckAccount $account, float $amount, string $payerName,
                                     string $verwendungsZweck, $datum, string $compareHash): CheckAccountTransaction
    {
        $transaction = $this->repo->createNewInstance();
        $transaction->setCheckAccount($account);
        $transaction->setAmount($amount);
        $transaction->setPaymtPurpose($verwendungsZweck);
        $transaction->setEntryDate($datum);
        $transaction->setValueDate($datum);
        $transaction->setCompareHash($compareHash);
        $transaction->setPayeePayerName($payerName);

        return $this->repo->saveOrUpdate($transaction);
    }

    public function auftraegeVerbuchenAufTransaktion(array $auftragsNummern, CheckAccountTransaction $transaction,
                                                     CheckAccount $konto)
    {
        $betragTransaktion = $transaction->getAmount();
        /** @var Order[] $orders */
        $orders = array();
        /** @var Invoice[] $invoices */
        $invoices = array();
        $summe = 0;

        foreach ($auftragsNummern as $nummer) {
            $order = $this->orderRepo->findByOrderNumber($nummer);
            if ($order !== null) {
                $invoice = $this->invoiceRepo->findByOrder($order);
                if ($invoice === null) {
                    $orders[] = $order;
                    $summe = $summe + $order->getSumGross();
                } else {
                    $invoices[] = $invoice;
                    $summe = $summe + $invoice->getSumGross();
                }
            }
        }

        if ($betragTransaktion === $summe) {
            foreach ($orders as $order) {
                $invoices[] = $this->invoiceRepo->createInvoiceFromOrder($order);
            }

            foreach ($invoices as $invoice) {
                $this->invoiceRepo->bookAmount($invoice, $invoice->getSumGross(), $transaction->getValueDate(), $konto, $transaction);
            }
        }
    }
}