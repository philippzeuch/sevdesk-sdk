<?php

namespace Zeuch\sevDesk\Utils;

class CheckAccountTransactionStatus
{
    public static $CREATED = "100";

    public static $LINKED = "200";

    public static $PRIVAT = "300";

    public static $BOOKED_AUTOMATICALLY = "350";

    public static $BOOKED = "400";
}