<?php

namespace Zeuch\sevDesk\Utils;

class BookingType
{
    /**
     * Normal booking / partial booking
     */
    const N = "N";

    /**
     * Reduced amount due to discount (Skonto)
     */
    const CB = "CB";

    /**
     * Reduced/Higher amount due to currency fluctuations
     */
    const CF = "CF";

    /**
     * Reduced/Higher amount due to other reasons
     */
    const O = "O";

    /**
     * Higher amount due to reminder charges
     */
    const OF = "OF";

    /**
     * Reduced amount due to the monetary traffic costs
     */
    const MTC = "MTC";
}