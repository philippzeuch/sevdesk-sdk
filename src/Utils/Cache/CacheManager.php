<?php

namespace Zeuch\sevDesk\Utils\Cache;

class CacheManager
{

    public function getCache(string $class) : ?Cache
    {
        return new Cache($class);
    }

    public function clearCache(string $class, string $id = "")
    {
        $cache = $this->getCache($class);
        if (!empty($id)) {
            $cache->setContent($id, null);
        } else {
            $cache->destroy();
        }
    }
}