<?php

namespace Zeuch\sevDesk\Utils\Cache;

class Cache
{
    private $cacheLocation;

    private $content;

    public function __construct($cacheLocation)
    {
        $this->cacheLocation = $cacheLocation;
        $this->content = &$_SESSION['cache']{$cacheLocation};
    }

    public function getContent(string $cacheId)
    {
        return $this->content[$cacheId];
    }

    public function setContent(string $cacheId, $content)
    {
        $this->content[$cacheId] = $content;
    }

    public function destroy()
    {
        unset($_SESSION['cache']{$this->cacheLocation});
        $this->content = [];
    }
}