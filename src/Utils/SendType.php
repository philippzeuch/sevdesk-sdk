<?php

namespace Zeuch\sevDesk\Utils;

class SendType
{
    const PRINTED = "VPR";

    const DOWNLOADED = "VPDF";

    const MAILED = "VM";

    const POSTAL = "VP";
}