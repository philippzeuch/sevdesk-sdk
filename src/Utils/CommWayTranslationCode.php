<?php

namespace Zeuch\sevDesk\Utils;

class CommWayTranslationCode
{
    const COMM_WAY_KEY_MOBILE = "COMM_WAY_KEY_MOBILE";

    const COMM_WAY_KEY_WORK = "COMM_WAY_KEY_WORK";

    const COMM_WAY_KEY_PRIVAT = "COMM_WAY_KEY_PRIVAT";
}