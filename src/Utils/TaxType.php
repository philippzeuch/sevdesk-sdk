<?php

namespace Zeuch\sevDesk\Utils;

class TaxType
{
    const DEFAULT = "default";

    const EU = "eu";

    const NOT_EU = "noteu";

    const CUSTOM = "custom";
}