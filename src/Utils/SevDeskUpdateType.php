<?php

namespace Zeuch\sevDesk\Utils;

class SevDeskUpdateType {

    const UPDATE = "PUT";

    const NEW = "POST";

    const REMOVE = "DELETE";
}