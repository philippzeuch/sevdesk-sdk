<?php

namespace Zeuch\sevDesk\Utils;

class OrderStatus
{
    const CREATED = 100;

    const DELIVERED = 200;

    const CLOSED = 280;

    const REJECTED = 300;

    const ACCEPTED = 500;

    const SOMECALCULATED = 750;

    const CALCULATED = 1000;
}