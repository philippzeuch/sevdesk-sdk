<?php

namespace Zeuch\sevDesk\Utils;

class InvoiceType
{
    public static $RE = "RE";

    public static $MA = "MA";

    public static $WKR = "WKR";
}