<?php

namespace Zeuch\sevDesk\Utils;

class ContactStatus
{
    const ACTIVE = 1000;

    const LEAD = 100;

    const PENDING = 500;
}