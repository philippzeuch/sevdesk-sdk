<?php

namespace Zeuch\sevDesk\Utils;

class OrderType
{
    const AUFTRAG = "AB";

    const ANGEBOT = "AN";
}