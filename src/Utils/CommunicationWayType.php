<?php

namespace Zeuch\sevDesk\Utils;

class CommunicationWayType
{
    const EMAIL = "EMAIL";

    const PHONE = "PHONE";

    const WEB = "WEB";

    const MOBILE = "MOBILE";
}