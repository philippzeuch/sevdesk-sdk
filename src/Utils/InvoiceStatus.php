<?php

namespace Zeuch\sevDesk\Utils;

class InvoiceStatus
{
    public static $CREATED = 100;

    public static $DELIVERED = 200;

    public static $PAYED = 1000;
}