<?php

namespace Zeuch\sevDesk\Utils;

class VoucherStatus
{
    public static $CREATED = 50;

    public static $DELIVERED = 100;

    public static $PAYED = 1000;
}