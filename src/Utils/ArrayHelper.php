<?php

namespace Zeuch\sevDesk\Utils;

class ArrayHelper {

    public static function normalize(array $array) : array {
        $newArray = array();
        foreach ($array as $key => $value) {
            $parts = explode("\0", $key);

            $newArray[end($parts)] = $value;
        }
        return $newArray;
    }
}