<?php

namespace Zeuch\sevDesk\Model;

class DefaultSevDeskEntity extends SevDeskEntity {

    public function __construct()
    {
        if (($key = array_search('id', $this->excludedForPersistence)) !== false) {
            unset($this->excludedForPersistence[$key]);
        }
        if (($key = array_search('objectName', $this->excludedForPersistence)) !== false) {
            unset($this->excludedForPersistence[$key]);
        }
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}