<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class Order extends SevDeskEntity
{
    const modelName = "Order";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $orderNumber;

    /** @var DefaultSevDeskEntity|null */
    private $contact;

    /** @var DateTime|null */
    private $orderDate;

    /** @var int */
    private $status;

    /** @var string|null */
    private $headText;

    /** @var string|null */
    private $footText;

    /** @var string|null */
    private $addressName;

    /** @var string|null */
    private $addressStreet;

    /** @var string|null */
    private $header;

    /** @var string|null */
    private $addressZip;

    /** @var string|null */
    private $addressCity;

    /** @var DefaultSevDeskEntity|null */
    private $addressCountry;

    /** @var DefaultSevDeskEntity|null */
    private $createUser;

    /** @var string|null */
    private $deliveryTerms;

    /** @var string|null */
    private $paymentTerms;

    /** @var int|null */
    private $version;

    /** @var DefaultSevDeskEntity|null */
    private $origin;

    /** @var bool|null */
    private $smallSettlement;

    /** @var DefaultSevDeskEntity|null */
    private $contactPerson;

    /** @var float|null */
    private $taxRate;

    /** @var DefaultSevDeskEntity|null */
    private $taxSet;

    /** @var string|null */
    private $taxText;

    /** @var string|null */
    private $addressParentName;

    /** @var DefaultSevDeskEntity|null */
    private $addressContactRef;

    /** @var string|null */
    private $taxType;

    /** @var string|null */
    private $orderType;

    /** @var DateTime|null */
    private $sendDate;

    /** @var string|null */
    private $addressParentName2;

    /** @var string|null */
    private $addressName2;

    /** @var string|null */
    private $addressGender;

    /** @var string|null */
    private $address;

    /** @var string|null */
    private $currency;

    /** @var float|null */
    private $sumNet;

    /** @var float|null */
    private $sumTax;

    /** @var float|null */
    private $sumGross;

    /** @var float|null */
    private $sumDiscounts;

    /** @var float|null */
    private $sumNetForeignCurrency;

    /** @var float|null */
    private $sumTaxForeignCurrency;

    /** @var float|null */
    private $sumGrossForeignCurrency;

    /** @var float|null */
    private $sumDiscountsForeignCurrency;

    /** @var float|null */
    private $weight;

    /** @var DefaultSevDeskEntity|null */
    private $entryType;

    /** @var string|null */
    private $customerInternalNote;

    /** @var bool|null */
    private $showNet;

    /** @var string|null */
    private $sendType;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getOrderNumber(): ?string
    {
        return $this->orderNumber;
    }

    /**
     * @param string|null $orderNumber
     */
    public function setOrderNumber(?string $orderNumber): void
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return Contact|DefaultSevDeskEntity|null
     */
    public function getContact()
    {
        if ($this->contact !== null && !$this->contact instanceof Contact && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Contact::class,
                Contact::modelName, "", [], $this->contact->getId());
            $this->contact = !empty($obj[0]) ? $obj[0] : $this->contact;
        }
        return $this->contact;
    }

    /**
     * @param DefaultSevDeskEntity|null $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return DateTime|null
     */
    public function getOrderDate(): ?DateTime
    {
        return $this->orderDate;
    }

    /**
     * @param DateTime|null $orderDate
     */
    public function setOrderDate(?DateTime $orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getHeadText(): ?string
    {
        return $this->headText;
    }

    /**
     * @param string|null $headText
     */
    public function setHeadText(?string $headText): void
    {
        $this->headText = $headText;
    }

    /**
     * @return string|null
     */
    public function getFootText(): ?string
    {
        return $this->footText;
    }

    /**
     * @param string|null $footText
     */
    public function setFootText(?string $footText): void
    {
        $this->footText = $footText;
    }

    /**
     * @return string|null
     */
    public function getAddressName(): ?string
    {
        return $this->addressName;
    }

    /**
     * @param string|null $addressName
     */
    public function setAddressName(?string $addressName): void
    {
        $this->addressName = $addressName;
    }

    /**
     * @return string|null
     */
    public function getAddressStreet(): ?string
    {
        return $this->addressStreet;
    }

    /**
     * @param string|null $addressStreet
     */
    public function setAddressStreet(?string $addressStreet): void
    {
        $this->addressStreet = $addressStreet;
    }

    /**
     * @return string|null
     */
    public function getHeader(): ?string
    {
        return $this->header;
    }

    /**
     * @param string|null $header
     */
    public function setHeader(?string $header): void
    {
        $this->header = $header;
    }

    /**
     * @return string|null
     */
    public function getAddressZip(): ?string
    {
        return $this->addressZip;
    }

    /**
     * @param string|null $addressZip
     */
    public function setAddressZip(?string $addressZip): void
    {
        $this->addressZip = $addressZip;
    }

    /**
     * @return string|null
     */
    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    /**
     * @param string|null $addressCity
     */
    public function setAddressCity(?string $addressCity): void
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return StaticCountry|DefaultSevDeskEntity|null
     */
    public function getAddressCountry()
    {
        if ($this->addressCountry !== null && !$this->addressCountry instanceof StaticCountry && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(StaticCountry::class,
                StaticCountry::modelName, "", [], $this->addressCountry->getId());
            $this->addressCountry = !empty($obj[0]) ? $obj[0] : $this->addressCountry;
        }
        return $this->addressCountry;
    }

    /**
     * @param DefaultSevDeskEntity|null $addressCountry
     */
    public function setAddressCountry($addressCountry): void
    {
        $this->addressCountry = $addressCountry;
    }

    /**
     * @return SevUser|DefaultSevDeskEntity|null
     */
    public function getCreateUser()
    {
        if ($this->createUser !== null && !$this->createUser instanceof SevUser && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(SevUser::class,
                SevUser::modelName, "", [], $this->createUser->getId());
            $this->createUser = !empty($obj[0]) ? $obj[0] : $this->createUser;
        }
        return $this->createUser;
    }

    /**
     * @param DefaultSevDeskEntity|null $createUser
     */
    public function setCreateUser($createUser): void
    {
        $this->createUser = $createUser;
    }

    /**
     * @return string|null
     */
    public function getDeliveryTerms(): ?string
    {
        return $this->deliveryTerms;
    }

    /**
     * @param string|null $deliveryTerms
     */
    public function setDeliveryTerms(?string $deliveryTerms): void
    {
        $this->deliveryTerms = $deliveryTerms;
    }

    /**
     * @return string|null
     */
    public function getPaymentTerms(): ?string
    {
        return $this->paymentTerms;
    }

    /**
     * @param string|null $paymentTerms
     */
    public function setPaymentTerms(?string $paymentTerms): void
    {
        $this->paymentTerms = $paymentTerms;
    }

    /**
     * @return int|null
     */
    public function getVersion(): ?int
    {
        return $this->version;
    }

    /**
     * @param int|null $version
     */
    public function setVersion(?int $version): void
    {
        $this->version = $version;
    }

    /**
     * @return Order|DefaultSevDeskEntity|null
     */
    public function getOrigin()
    {
        if ($this->origin !== null && !$this->origin instanceof Order && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Order::class,
                Order::modelName, "", [], $this->origin->getId());
            $this->origin = !empty($obj[0]) ? $obj[0] : $this->origin;
        }
        return $this->origin;
    }

    /**
     * @param DefaultSevDeskEntity|null $origin
     */
    public function setOrigin($origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return bool|null
     */
    public function getSmallSettlement(): ?bool
    {
        return $this->smallSettlement;
    }

    /**
     * @param bool|null $smallSettlement
     */
    public function setSmallSettlement(?bool $smallSettlement): void
    {
        $this->smallSettlement = $smallSettlement;
    }

    /**
     * @return SevUser|DefaultSevDeskEntity|null
     */
    public function getContactPerson()
    {
        if ($this->contactPerson !== null && !$this->contactPerson instanceof SevUser && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(SevUser::class,
                SevUser::modelName, "", [], $this->contactPerson->getId());
            $this->contactPerson = !empty($obj[0]) ? $obj[0] : $this->contactPerson;
        }
        return $this->contactPerson;
    }

    /**
     * @param DefaultSevDeskEntity|null $contactPerson
     */
    public function setContactPerson($contactPerson): void
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    /**
     * @param float|null $taxRate
     */
    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return TaxSet|DefaultSevDeskEntity|null
     */
    public function getTaxSet()
    {
        if ($this->taxSet !== null && !$this->taxSet instanceof TaxSet && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(TaxSet::class,
                TaxSet::modelName, "", [], $this->taxSet->getId());
            $this->taxSet = !empty($obj[0]) ? $obj[0] : $this->taxSet;
        }
        return $this->taxSet;
    }

    /**
     * @param DefaultSevDeskEntity|null $taxSet
     */
    public function setTaxSet($taxSet): void
    {
        $this->taxSet = $taxSet;
    }

    /**
     * @return string|null
     */
    public function getTaxText(): ?string
    {
        return $this->taxText;
    }

    /**
     * @param string|null $taxText
     */
    public function setTaxText(?string $taxText): void
    {
        $this->taxText = $taxText;
    }

    /**
     * @return string|null
     */
    public function getAddressParentName(): ?string
    {
        return $this->addressParentName;
    }

    /**
     * @param string|null $addressParentName
     */
    public function setAddressParentName(?string $addressParentName): void
    {
        $this->addressParentName = $addressParentName;
    }

    /**
     * @return ContactAddress|DefaultSevDeskEntity|null
     */
    public function getAddressContactRef()
    {
        if ($this->addressContactRef !== null && !$this->addressContactRef instanceof ContactAddress && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(ContactAddress::class,
                ContactAddress::modelName, "", [], $this->addressContactRef->getId());
            $this->addressContactRef = !empty($obj[0]) ? $obj[0] : $this->addressContactRef;
        }
        return $this->addressContactRef;
    }

    /**
     * @param DefaultSevDeskEntity|null $addressContactRef
     */
    public function setAddressContactRef($addressContactRef): void
    {
        $this->addressContactRef = $addressContactRef;
    }

    /**
     * @return string|null
     */
    public function getTaxType(): ?string
    {
        return $this->taxType;
    }

    /**
     * @param string|null $taxType
     */
    public function setTaxType(?string $taxType): void
    {
        $this->taxType = $taxType;
    }

    /**
     * @return string|null
     */
    public function getOrderType(): ?string
    {
        return $this->orderType;
    }

    /**
     * @param string|null $orderType
     */
    public function setOrderType(?string $orderType): void
    {
        $this->orderType = $orderType;
    }

    /**
     * @return DateTime|null
     */
    public function getSendDate(): ?DateTime
    {
        return $this->sendDate;
    }

    /**
     * @param DateTime|null $sendDate
     */
    public function setSendDate(?DateTime $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    /**
     * @return string|null
     */
    public function getAddressParentName2(): ?string
    {
        return $this->addressParentName2;
    }

    /**
     * @param string|null $addressParentName2
     */
    public function setAddressParentName2(?string $addressParentName2): void
    {
        $this->addressParentName2 = $addressParentName2;
    }

    /**
     * @return string|null
     */
    public function getAddressName2(): ?string
    {
        return $this->addressName2;
    }

    /**
     * @param string|null $addressName2
     */
    public function setAddressName2(?string $addressName2): void
    {
        $this->addressName2 = $addressName2;
    }

    /**
     * @return string|null
     */
    public function getAddressGender(): ?string
    {
        return $this->addressGender;
    }

    /**
     * @param string|null $addressGender
     */
    public function setAddressGender(?string $addressGender): void
    {
        $this->addressGender = $addressGender;
    }

    /**
     * @return string|null
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return float|null
     */
    public function getSumNet(): ?float
    {
        return $this->sumNet;
    }

    /**
     * @param float|null $sumNet
     */
    public function setSumNet(?float $sumNet): void
    {
        $this->sumNet = $sumNet;
    }

    /**
     * @return float|null
     */
    public function getSumTax(): ?float
    {
        return $this->sumTax;
    }

    /**
     * @param float|null $sumTax
     */
    public function setSumTax(?float $sumTax): void
    {
        $this->sumTax = $sumTax;
    }

    /**
     * @return float|null
     */
    public function getSumGross(): ?float
    {
        return $this->sumGross;
    }

    /**
     * @param float|null $sumGross
     */
    public function setSumGross(?float $sumGross): void
    {
        $this->sumGross = $sumGross;
    }

    /**
     * @return float|null
     */
    public function getSumDiscounts(): ?float
    {
        return $this->sumDiscounts;
    }

    /**
     * @param float|null $sumDiscounts
     */
    public function setSumDiscounts(?float $sumDiscounts): void
    {
        $this->sumDiscounts = $sumDiscounts;
    }

    /**
     * @return float|null
     */
    public function getSumNetForeignCurrency(): ?float
    {
        return $this->sumNetForeignCurrency;
    }

    /**
     * @param float|null $sumNetForeignCurrency
     */
    public function setSumNetForeignCurrency(?float $sumNetForeignCurrency): void
    {
        $this->sumNetForeignCurrency = $sumNetForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumTaxForeignCurrency(): ?float
    {
        return $this->sumTaxForeignCurrency;
    }

    /**
     * @param float|null $sumTaxForeignCurrency
     */
    public function setSumTaxForeignCurrency(?float $sumTaxForeignCurrency): void
    {
        $this->sumTaxForeignCurrency = $sumTaxForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumGrossForeignCurrency(): ?float
    {
        return $this->sumGrossForeignCurrency;
    }

    /**
     * @param float|null $sumGrossForeignCurrency
     */
    public function setSumGrossForeignCurrency(?float $sumGrossForeignCurrency): void
    {
        $this->sumGrossForeignCurrency = $sumGrossForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumDiscountsForeignCurrency(): ?float
    {
        return $this->sumDiscountsForeignCurrency;
    }

    /**
     * @param float|null $sumDiscountsForeignCurrency
     */
    public function setSumDiscountsForeignCurrency(?float $sumDiscountsForeignCurrency): void
    {
        $this->sumDiscountsForeignCurrency = $sumDiscountsForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getWeight(): ?float
    {
        return $this->weight;
    }

    /**
     * @param float|null $weight
     */
    public function setWeight(?float $weight): void
    {
        $this->weight = $weight;
    }

    /**
     * @return EntryType|DefaultSevDeskEntity|null
     */
    public function getEntryType()
    {
        if ($this->entryType !== null && !$this->entryType instanceof EntryType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(EntryType::class,
                EntryType::modelName, "", [], $this->entryType->getId());
            $this->entryType = !empty($obj[0]) ? $obj[0] : $this->entryType;
        }
        return $this->entryType;
    }

    /**
     * @param DefaultSevDeskEntity|null $entryType
     */
    public function setEntryType($entryType): void
    {
        $this->entryType = $entryType;
    }

    /**
     * @return string|null
     */
    public function getCustomerInternalNote(): ?string
    {
        return $this->customerInternalNote;
    }

    /**
     * @param string|null $customerInternalNote
     */
    public function setCustomerInternalNote(?string $customerInternalNote): void
    {
        $this->customerInternalNote = $customerInternalNote;
    }

    /**
     * @return bool|null
     */
    public function getShowNet(): ?bool
    {
        return $this->showNet;
    }

    /**
     * @param bool|null $showNet
     */
    public function setShowNet(?bool $showNet): void
    {
        $this->showNet = $showNet;
    }

    /**
     * @return string|null
     */
    public function getSendType(): ?string
    {
        return $this->sendType;
    }

    /**
     * @param string|null $sendType
     */
    public function setSendType(?string $sendType): void
    {
        $this->sendType = $sendType;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}