<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class Voucher
 * @package Zeuch\sevDesk\Model
 */
class Voucher extends SevDeskEntity
{
    const modelName = "Voucher";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DefaultSevDeskEntity|null */
    private $createUser;

    /** @var DateTime|null */
    private $voucherDate;

    /** @var DefaultSevDeskEntity|null */
    private $supplier;

    /** @var string|null */
    private $supplierName;

    /** @var string|null */
    private $description;

    /** @var DefaultSevDeskEntity|null */
    private $document;

    /** @var string|null */
    private $resultDisdar;

    /** @var DefaultSevDeskEntity|null */
    private $documentPreview;

    /** @var DateTime|null */
    private $payDate;

    /** @var int|null */
    private $status;

    /** @var object|null */
    private $object;

    /** @var float|null */
    private $sumNet;

    /** @var float|null */
    private $sumTax;

    /** @var float|null */
    private $sumGross;

    /** @var float|null */
    private $sumNetAccounting;

    /** @var float|null */
    private $sumTaxAccounting;

    /** @var float|null */
    private $sumGrossAccounting;

    /** @var string|null */
    private $taxType;

    /** @var string|null */
    private $creditDebit;

    /** @var bool|null */
    private $hidden;

    /** @var DefaultSevDeskEntity|null */
    private $costCentre;

    /** @var object|null */
    private $origin;

    /** @var string */
    private $voucherType;

    /** @var int|null */
    private $recurringIntervall;

    /** @var DateTime|null */
    private $recurringStartDate;

    /** @var DateTime|null */
    private $recurringNextVoucher;

    /** @var DateTime|null */
    private $recurringLastVoucher;

    /** @var DateTime|null */
    private $recurringEndDate;

    /** @var bool|null */
    private $enshrined;

    /** @var string|null */
    private $inSource;

    /** @var DefaultSevDeskEntity|null */
    private $taxSet;

    /** @var string|null */
    private $iban;

    /** @var string|null */
    private $accountingSpecialCase;

    /** @var DateTime|null */
    private $paymentDeadline;

    /** @var float|null */
    private $tip;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return SevUser|DefaultSevDeskEntity|null
     */
    public function getCreateUser()
    {
        if ($this->createUser !== null && !$this->createUser instanceof SevUser && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(SevUser::class,
                SevUser::modelName, "", [], $this->createUser->getId());
            $this->createUser = !empty($obj[0]) ? $obj[0] : $this->createUser;
        }
        return $this->createUser;
    }

    public function setCreateUser($createUser): void
    {
        $this->createUser = $createUser;
    }

    /**
     * @return DateTime|null
     */
    public function getVoucherDate(): ?DateTime
    {
        return $this->voucherDate;
    }

    /**
     * @param DateTime|null $voucherDate
     */
    public function setVoucherDate(?DateTime $voucherDate): void
    {
        $this->voucherDate = $voucherDate;
    }

    /**
     * @return Contact|DefaultSevDeskEntity|null
     */
    public function getSupplier()
    {
        if ($this->supplier !== null && !$this->supplier instanceof Contact && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Contact::class,
                Contact::modelName, "", [], $this->supplier->getId());
            $this->supplier = !empty($obj[0]) ? $obj[0] : $this->supplier;
        }
        return $this->supplier;
    }

    public function setSupplier($supplier): void
    {
        $this->supplier = $supplier;
    }

    /**
     * @return string|null
     */
    public function getSupplierName(): ?string
    {
        return $this->supplierName;
    }

    /**
     * @param string|null $supplierName
     */
    public function setSupplierName(?string $supplierName): void
    {
        $this->supplierName = $supplierName;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getDocument()
    {
        // TODO: "Document" Model implementieren
        return $this->document;
    }

    public function setDocument($document): void
    {
        $this->document = $document;
    }

    /**
     * @return string|null
     */
    public function getResultDisdar(): ?string
    {
        return $this->resultDisdar;
    }

    /**
     * @param string|null $resultDisdar
     */
    public function setResultDisdar(?string $resultDisdar): void
    {
        $this->resultDisdar = $resultDisdar;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getDocumentPreview()
    {
        // TODO: "Document" Model implementieren
        return $this->documentPreview;
    }

    public function setDocumentPreview($documentPreview): void
    {
        $this->documentPreview = $documentPreview;
    }

    /**
     * @return DateTime|null
     */
    public function getPayDate(): ?DateTime
    {
        return $this->payDate;
    }

    /**
     * @param DateTime|null $payDate
     */
    public function setPayDate(?DateTime $payDate): void
    {
        $this->payDate = $payDate;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return object|null
     */
    public function getObject(): ?object
    {
        return $this->object;
    }

    /**
     * @param object|null $object
     */
    public function setObject(?object $object): void
    {
        $this->object = $object;
    }

    /**
     * @return float|null
     */
    public function getSumNet(): ?float
    {
        return $this->sumNet;
    }

    /**
     * @param float|null $sumNet
     */
    public function setSumNet(?float $sumNet): void
    {
        $this->sumNet = $sumNet;
    }

    /**
     * @return float|null
     */
    public function getSumTax(): ?float
    {
        return $this->sumTax;
    }

    /**
     * @param float|null $sumTax
     */
    public function setSumTax(?float $sumTax): void
    {
        $this->sumTax = $sumTax;
    }

    /**
     * @return float|null
     */
    public function getSumGross(): ?float
    {
        return $this->sumGross;
    }

    /**
     * @param float|null $sumGross
     */
    public function setSumGross(?float $sumGross): void
    {
        $this->sumGross = $sumGross;
    }

    /**
     * @return float|null
     */
    public function getSumNetAccounting(): ?float
    {
        return $this->sumNetAccounting;
    }

    /**
     * @param float|null $sumNetAccounting
     */
    public function setSumNetAccounting(?float $sumNetAccounting): void
    {
        $this->sumNetAccounting = $sumNetAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumTaxAccounting(): ?float
    {
        return $this->sumTaxAccounting;
    }

    /**
     * @param float|null $sumTaxAccounting
     */
    public function setSumTaxAccounting(?float $sumTaxAccounting): void
    {
        $this->sumTaxAccounting = $sumTaxAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumGrossAccounting(): ?float
    {
        return $this->sumGrossAccounting;
    }

    /**
     * @param float|null $sumGrossAccounting
     */
    public function setSumGrossAccounting(?float $sumGrossAccounting): void
    {
        $this->sumGrossAccounting = $sumGrossAccounting;
    }

    /**
     * @return string|null
     */
    public function getTaxType(): ?string
    {
        return $this->taxType;
    }

    /**
     * @param string|null $taxType
     */
    public function setTaxType(?string $taxType): void
    {
        $this->taxType = $taxType;
    }

    /**
     * @return string|null
     */
    public function getCreditDebit(): ?string
    {
        return $this->creditDebit;
    }

    /**
     * @param string|null $creditDebit
     */
    public function setCreditDebit(?string $creditDebit): void
    {
        $this->creditDebit = $creditDebit;
    }

    /**
     * @return bool|null
     */
    public function getHidden(): ?bool
    {
        return $this->hidden;
    }

    /**
     * @param bool|null $hidden
     */
    public function setHidden(?bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * @return CostCentre|DefaultSevDeskEntity|null
     */
    public function getCostCentre()
    {
        if ($this->costCentre !== null && !$this->costCentre instanceof CostCentre && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(CostCentre::class,
                CostCentre::modelName, "", [], $this->costCentre->getId());
            $this->costCentre = !empty($obj[0]) ? $obj[0] : $this->costCentre;
        }
        return $this->costCentre;
    }

    public function setCostCentre($costCentre): void
    {
        $this->costCentre = $costCentre;
    }

    /**
     * @return object|null
     */
    public function getOrigin(): ?object
    {
        return $this->origin;
    }

    /**
     * @param object|null $origin
     */
    public function setOrigin(?object $origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return string
     */
    public function getVoucherType(): string
    {
        return $this->voucherType;
    }

    /**
     * @param string $voucherType
     */
    public function setVoucherType(string $voucherType): void
    {
        $this->voucherType = $voucherType;
    }

    /**
     * @return int|null
     */
    public function getRecurringIntervall(): ?int
    {
        return $this->recurringIntervall;
    }

    /**
     * @param int|null $recurringIntervall
     */
    public function setRecurringIntervall(?int $recurringIntervall): void
    {
        $this->recurringIntervall = $recurringIntervall;
    }

    /**
     * @return DateTime|null
     */
    public function getRecurringStartDate(): ?DateTime
    {
        return $this->recurringStartDate;
    }

    /**
     * @param DateTime|null $recurringStartDate
     */
    public function setRecurringStartDate(?DateTime $recurringStartDate): void
    {
        $this->recurringStartDate = $recurringStartDate;
    }

    /**
     * @return DateTime|null
     */
    public function getRecurringNextVoucher(): ?DateTime
    {
        return $this->recurringNextVoucher;
    }

    /**
     * @param DateTime|null $recurringNextVoucher
     */
    public function setRecurringNextVoucher(?DateTime $recurringNextVoucher): void
    {
        $this->recurringNextVoucher = $recurringNextVoucher;
    }

    /**
     * @return DateTime|null
     */
    public function getRecurringLastVoucher(): ?DateTime
    {
        return $this->recurringLastVoucher;
    }

    /**
     * @param DateTime|null $recurringLastVoucher
     */
    public function setRecurringLastVoucher(?DateTime $recurringLastVoucher): void
    {
        $this->recurringLastVoucher = $recurringLastVoucher;
    }

    /**
     * @return DateTime|null
     */
    public function getRecurringEndDate(): ?DateTime
    {
        return $this->recurringEndDate;
    }

    /**
     * @param DateTime|null $recurringEndDate
     */
    public function setRecurringEndDate(?DateTime $recurringEndDate): void
    {
        $this->recurringEndDate = $recurringEndDate;
    }

    /**
     * @return bool|null
     */
    public function getEnshrined(): ?bool
    {
        return $this->enshrined;
    }

    /**
     * @param bool|null $enshrined
     */
    public function setEnshrined(?bool $enshrined): void
    {
        $this->enshrined = $enshrined;
    }

    /**
     * @return string|null
     */
    public function getInSource(): ?string
    {
        return $this->inSource;
    }

    /**
     * @param string|null $inSource
     */
    public function setInSource(?string $inSource): void
    {
        $this->inSource = $inSource;
    }

    /**
     * @return TaxSet|DefaultSevDeskEntity|null
     */
    public function getTaxSet()
    {
        if ($this->taxSet !== null && !$this->taxSet instanceof TaxSet && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(TaxSet::class,
                TaxSet::modelName, "", [], $this->taxSet->getId());
            $this->taxSet = !empty($obj[0]) ? $obj[0] : $this->taxSet;
        }
        return $this->taxSet;
    }

    public function setTaxSet($taxSet): void
    {
        $this->taxSet = $taxSet;
    }

    /**
     * @return string|null
     */
    public function getIban(): ?string
    {
        return $this->iban;
    }

    /**
     * @param string|null $iban
     */
    public function setIban(?string $iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return string|null
     */
    public function getAccountingSpecialCase(): ?string
    {
        return $this->accountingSpecialCase;
    }

    /**
     * @param string|null $accountingSpecialCase
     */
    public function setAccountingSpecialCase(?string $accountingSpecialCase): void
    {
        $this->accountingSpecialCase = $accountingSpecialCase;
    }

    /**
     * @return DateTime|null
     */
    public function getPaymentDeadline(): ?DateTime
    {
        return $this->paymentDeadline;
    }

    /**
     * @param DateTime|null $paymentDeadline
     */
    public function setPaymentDeadline(?DateTime $paymentDeadline): void
    {
        $this->paymentDeadline = $paymentDeadline;
    }

    /**
     * @return float|null
     */
    public function getTip(): ?float
    {
        return $this->tip;
    }

    /**
     * @param float|null $tip
     */
    public function setTip(?float $tip): void
    {
        $this->tip = $tip;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}