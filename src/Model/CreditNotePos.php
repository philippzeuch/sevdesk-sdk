<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class CreditNotePos
 * @package Zeuch\sevDesk\Model
 */
class CreditNotePos extends SevDeskEntity
{
    const modelName = "CreditNotePos";

    protected $objectName = self::modelName;

    /** @var object|null */
    private $additionalInformation;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DefaultSevDeskEntity|null */
    private $creditNote;

    /** @var int|null */
    private $quantity;

    /** @var float|null */
    private $price;

    /** @var string|null */
    private $name;

    /** @var int|null */
    private $priority = 100;

    /** @var DefaultSevDeskEntity|null */
    private $unity;

    /** @var int|null */
    private $positionNumber;

    /** @var string|null */
    private $text;

    /** @var float|null */
    private $discount;

    /** @var float|null */
    private $taxRate;

    /** @var bool|null */
    private $temporary = 0;

    /** @var float|null */
    private $sumNet;

    /** @var float|null */
    private $sumGross;

    /** @var float|null */
    private $sumDiscount;

    /** @var float|null */
    private $sumTax;

    /** @var float|null */
    private $sumNetAccounting;

    /** @var float|null */
    private $sumTaxAccounting;

    /** @var float|null */
    private $sumGrossAccounting;

    /** @var float|null */
    private $priceNet;

    /** @var float|null */
    private $priceGross;

    /** @var float|null */
    private $priceTax;

    /**
     * @return object|null
     */
    public function getAdditionalInformation(): ?object
    {
        return $this->additionalInformation;
    }

    /**
     * @param object|null $additionalInformation
     */
    public function setAdditionalInformation(?object $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return CreditNote|DefaultSevDeskEntity|null
     */
    public function getCreditNote()
    {
        if ($this->creditNote !== null && !$this->creditNote instanceof CreditNote && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(CreditNote::class,
                CreditNote::modelName, "", [], $this->creditNote->getId());
            $this->creditNote = !empty($obj[0]) ? $obj[0] : $this->creditNote;
        }
        return $this->creditNote;
    }

    /**
     * @param DefaultSevDeskEntity|null $creditNote
     */
    public function setCreditNote($creditNote): void
    {
        $this->creditNote = $creditNote;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int|null $priority
     */
    public function setPriority(?int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return Unity|DefaultSevDeskEntity|null
     */
    public function getUnity()
    {
        if ($this->unity !== null && !$this->unity instanceof Unity && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Unity::class,
                Unity::modelName, "", [], $this->unity->getId());
            $this->unity = !empty($obj[0]) ? $obj[0] : $this->unity;
        }
        return $this->unity;
    }

    /**
     * @param DefaultSevDeskEntity|null $unity
     */
    public function setUnity($unity): void
    {
        $this->unity = $unity;
    }

    /**
     * @return int|null
     */
    public function getPositionNumber(): ?int
    {
        return $this->positionNumber;
    }

    /**
     * @param int|null $positionNumber
     */
    public function setPositionNumber(?int $positionNumber): void
    {
        $this->positionNumber = $positionNumber;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return float|null
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float|null $discount
     */
    public function setDiscount($discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    /**
     * @param float|null $taxRate
     */
    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return bool|null
     */
    public function getTemporary(): ?bool
    {
        return $this->temporary;
    }

    /**
     * @param bool|null $temporary
     */
    public function setTemporary(?bool $temporary): void
    {
        $this->temporary = $temporary;
    }

    /**
     * @return float|null
     */
    public function getSumNet(): ?float
    {
        return $this->sumNet;
    }

    /**
     * @param float|null $sumNet
     */
    public function setSumNet(?float $sumNet): void
    {
        $this->sumNet = $sumNet;
    }

    /**
     * @return float|null
     */
    public function getSumGross(): ?float
    {
        return $this->sumGross;
    }

    /**
     * @param float|null $sumGross
     */
    public function setSumGross(?float $sumGross): void
    {
        $this->sumGross = $sumGross;
    }

    /**
     * @return float|null
     */
    public function getSumDiscount(): ?float
    {
        return $this->sumDiscount;
    }

    /**
     * @param float|null $sumDiscount
     */
    public function setSumDiscount(?float $sumDiscount): void
    {
        $this->sumDiscount = $sumDiscount;
    }

    /**
     * @return float|null
     */
    public function getSumTax(): ?float
    {
        return $this->sumTax;
    }

    /**
     * @param float|null $sumTax
     */
    public function setSumTax(?float $sumTax): void
    {
        $this->sumTax = $sumTax;
    }

    /**
     * @return float|null
     */
    public function getSumNetAccounting(): ?float
    {
        return $this->sumNetAccounting;
    }

    /**
     * @param float|null $sumNetAccounting
     */
    public function setSumNetAccounting(?float $sumNetAccounting): void
    {
        $this->sumNetAccounting = $sumNetAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumTaxAccounting(): ?float
    {
        return $this->sumTaxAccounting;
    }

    /**
     * @param float|null $sumTaxAccounting
     */
    public function setSumTaxAccounting(?float $sumTaxAccounting): void
    {
        $this->sumTaxAccounting = $sumTaxAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumGrossAccounting(): ?float
    {
        return $this->sumGrossAccounting;
    }

    /**
     * @param float|null $sumGrossAccounting
     */
    public function setSumGrossAccounting(?float $sumGrossAccounting): void
    {
        $this->sumGrossAccounting = $sumGrossAccounting;
    }

    /**
     * @return float|null
     */
    public function getPriceNet(): ?float
    {
        return $this->priceNet;
    }

    /**
     * @param float|null $priceNet
     */
    public function setPriceNet(?float $priceNet): void
    {
        $this->priceNet = $priceNet;
    }

    /**
     * @return float|null
     */
    public function getPriceGross(): ?float
    {
        return $this->priceGross;
    }

    /**
     * @param float|null $priceGross
     */
    public function setPriceGross(?float $priceGross): void
    {
        $this->priceGross = $priceGross;
    }

    /**
     * @return float|null
     */
    public function getPriceTax(): ?float
    {
        return $this->priceTax;
    }

    /**
     * @param float|null $priceTax
     */
    public function setPriceTax(?float $priceTax): void
    {
        $this->priceTax = $priceTax;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}