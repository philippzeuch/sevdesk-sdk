<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class VoucherPos
 * @package Zeuch\sevDesk\Model
 */
class VoucherPos extends SevDeskEntity
{
    const modelName = "VoucherPos";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DefaultSevDeskEntity|null */
    private $voucher;

    /** @var DefaultSevDeskEntity|null */
    private $accountingType;

    /** @var DefaultSevDeskEntity|null */
    private $estimatedAccountingType;

    /** @var float|null */
    private $taxRate;

    /** @var float|null */
    private $sum;

    /** @var bool|null */
    private $net;

    /** @var bool|null */
    private $isAsset;

    /** @var float|null */
    private $sumNet;

    /** @var float|null */
    private $sumTax;

    /** @var float|null */
    private $sumGross;

    /** @var float|null */
    private $sumNetAccounting;

    /** @var float|null */
    private $sumTaxAccounting;

    /** @var float|null */
    private $sumGrossAccounting;

    /** @var string|null */
    private $comment;

    /** @var bool|null */
    private $isGwg;

    /** @var float|null */
    private $cateringTaxRate;

    /** @var float|null */
    private $cateringTip;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return Voucher|DefaultSevDeskEntity|null
     */
    public function getVoucher()
    {
        if ($this->voucher !== null && !$this->voucher instanceof Voucher && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Voucher::class,
                Voucher::modelName, "", [], $this->voucher->getId());
            $this->voucher = !empty($obj[0]) ? $obj[0] : $this->voucher;
        }
        return $this->voucher;
    }

    /**
     * @param DefaultSevDeskEntity|null $voucher
     */
    public function setVoucher($voucher): void
    {
        $this->voucher = $voucher;
    }

    /**
     * @return AccountingType|DefaultSevDeskEntity|null
     */
    public function getAccountingType()
    {
        if ($this->accountingType !== null && !$this->accountingType instanceof AccountingType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingType::class,
                AccountingType::modelName, "", [], $this->accountingType->getId());
            $this->accountingType = !empty($obj[0]) ? $obj[0] : $this->accountingType;
        }
        return $this->accountingType;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingType
     */
    public function setAccountingType($accountingType): void
    {
        $this->accountingType = $accountingType;
    }

    /**
     * @return AccountingType|DefaultSevDeskEntity|null
     */
    public function getEstimatedAccountingType()
    {
        if ($this->estimatedAccountingType !== null && !$this->estimatedAccountingType instanceof AccountingType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingType::class,
                AccountingType::modelName, "", [], $this->estimatedAccountingType->getId());
            $this->estimatedAccountingType = !empty($obj[0]) ? $obj[0] : $this->estimatedAccountingType;
        }
        return $this->estimatedAccountingType;
    }

    /**
     * @param DefaultSevDeskEntity|null $estimatedAccountingType
     */
    public function setEstimatedAccountingType($estimatedAccountingType): void
    {
        $this->estimatedAccountingType = $estimatedAccountingType;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    /**
     * @param float|null $taxRate
     */
    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return float|null
     */
    public function getSum(): ?float
    {
        return $this->sum;
    }

    /**
     * @param float|null $sum
     */
    public function setSum(?float $sum): void
    {
        $this->sum = $sum;
    }

    /**
     * @return bool|null
     */
    public function getNet(): ?bool
    {
        return $this->net;
    }

    /**
     * @param bool|null $net
     */
    public function setNet(?bool $net): void
    {
        $this->net = $net;
    }

    /**
     * @return bool|null
     */
    public function getIsAsset(): ?bool
    {
        return $this->isAsset;
    }

    /**
     * @param bool|null $isAsset
     */
    public function setIsAsset(?bool $isAsset): void
    {
        $this->isAsset = $isAsset;
    }

    /**
     * @return float|null
     */
    public function getSumNet(): ?float
    {
        return $this->sumNet;
    }

    /**
     * @param float|null $sumNet
     */
    public function setSumNet(?float $sumNet): void
    {
        $this->sumNet = $sumNet;
    }

    /**
     * @return float|null
     */
    public function getSumTax(): ?float
    {
        return $this->sumTax;
    }

    /**
     * @param float|null $sumTax
     */
    public function setSumTax(?float $sumTax): void
    {
        $this->sumTax = $sumTax;
    }

    /**
     * @return float|null
     */
    public function getSumGross(): ?float
    {
        return $this->sumGross;
    }

    /**
     * @param float|null $sumGross
     */
    public function setSumGross(?float $sumGross): void
    {
        $this->sumGross = $sumGross;
    }

    /**
     * @return float|null
     */
    public function getSumNetAccounting(): ?float
    {
        return $this->sumNetAccounting;
    }

    /**
     * @param float|null $sumNetAccounting
     */
    public function setSumNetAccounting(?float $sumNetAccounting): void
    {
        $this->sumNetAccounting = $sumNetAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumTaxAccounting(): ?float
    {
        return $this->sumTaxAccounting;
    }

    /**
     * @param float|null $sumTaxAccounting
     */
    public function setSumTaxAccounting(?float $sumTaxAccounting): void
    {
        $this->sumTaxAccounting = $sumTaxAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumGrossAccounting(): ?float
    {
        return $this->sumGrossAccounting;
    }

    /**
     * @param float|null $sumGrossAccounting
     */
    public function setSumGrossAccounting(?float $sumGrossAccounting): void
    {
        $this->sumGrossAccounting = $sumGrossAccounting;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return bool|null
     */
    public function getIsGwg(): ?bool
    {
        return $this->isGwg;
    }

    /**
     * @param bool|null $isGwg
     */
    public function setIsGwg(?bool $isGwg): void
    {
        $this->isGwg = $isGwg;
    }

    /**
     * @return float|null
     */
    public function getCateringTaxRate(): ?float
    {
        return $this->cateringTaxRate;
    }

    /**
     * @param float|null $cateringTaxRate
     */
    public function setCateringTaxRate(?float $cateringTaxRate): void
    {
        $this->cateringTaxRate = $cateringTaxRate;
    }

    /**
     * @return float|null
     */
    public function getCateringTip(): ?float
    {
        return $this->cateringTip;
    }

    /**
     * @param float|null $cateringTip
     */
    public function setCateringTip(?float $cateringTip): void
    {
        $this->cateringTip = $cateringTip;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}