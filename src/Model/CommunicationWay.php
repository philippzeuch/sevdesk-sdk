<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class CommunicationWay extends SevDeskEntity
{
    const modelName = "CommunicationWay";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DefaultSevDeskEntity|null */
    private $contact;

    /** @var string|null */
    private $type;

    /** @var string|null */
    private $value;

    /** @var DefaultSevDeskEntity|null */
    private $key;

    /** @var bool|null */
    private $main;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return Contact|DefaultSevDeskEntity|null
     */
    public function getContact()
    {
        if ($this->contact !== null && !$this->contact instanceof Contact && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Contact::class,
                Contact::modelName, "", [], $this->contact->getId());
            $this->contact = !empty($obj[0]) ? $obj[0] : $this->contact;
        }
        return $this->contact;
    }

    /**
     * @param DefaultSevDeskEntity|null $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return CommunicationWayKey|DefaultSevDeskEntity|null
     */
    public function getKey()
    {
        if ($this->key !== null && !$this->key instanceof CommunicationWayKey && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(CommunicationWayKey::class,
                CommunicationWayKey::modelName, "", [], $this->key->getId());
            $this->key = !empty($obj[0]) ? $obj[0] : $this->key;
        }
        return $this->key;
    }

    /**
     * @param DefaultSevDeskEntity|null $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    /**
     * @return bool|null
     */
    public function getMain(): ?bool
    {
        return $this->main;
    }

    /**
     * @param bool|null $main
     */
    public function setMain(?bool $main): void
    {
        $this->main = $main;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}