<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class CheckAccountTransaction
 * @package Zeuch\sevDesk\Model
 */
class CheckAccountTransaction extends SevDeskEntity
{
    const modelName = "CheckAccountTransaction";

    protected $objectName = self::modelName;

    /** @var string */
    private $additionalInformation;

    /** @var DateTime */
    private $create;

    /** @var DateTime */
    private $update;

    /** @var DateTime */
    private $valueDate;

    /** @var DateTime */
    private $entryDate;

    /** @var float */
    private $amount;

    /** @var string */
    private $feeAmount;

    /** @var string */
    private $gvCode;

    /** @var string */
    private $entryText;

    /** @var string */
    private $primaNotaNo;

    /** @var string */
    private $paymtPurpose;

    /** @var string */
    private $payeePayerBankCode;

    /** @var string */
    private $payeePayerAcctNo;

    /** @var string */
    private $payeePayerName;

    /** @var DefaultSevDeskEntity|null */
    private $checkAccount;

    /** @var string */
    private $status;

    /** @var string */
    private $score;

    /** @var string */
    private $compareHash;

    /** @var bool */
    private $enshrined;

    /** @var string */
    private $obonoReceiptUuid;

    /**
     * @return string|null
     */
    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    /**
     * @param string|null $additionalInformation
     */
    public function setAdditionalInformation(?string $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return DateTime|null
     */
    public function getValueDate(): ?DateTime
    {
        return $this->valueDate;
    }

    /**
     * @param DateTime|null $valueDate
     */
    public function setValueDate(?DateTime $valueDate): void
    {
        $this->valueDate = $valueDate;
    }

    /**
     * @return DateTime|null
     */
    public function getEntryDate(): ?DateTime
    {
        return $this->entryDate;
    }

    /**
     * @param DateTime|null $entryDate
     */
    public function setEntryDate(?DateTime $entryDate): void
    {
        $this->entryDate = $entryDate;
    }

    /**
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->amount;
    }

    /**
     * @param float|null $amount
     */
    public function setAmount(?float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getFeeAmount(): ?string
    {
        return $this->feeAmount;
    }

    /**
     * @param string|null $feeAmount
     */
    public function setFeeAmount(?string $feeAmount): void
    {
        $this->feeAmount = $feeAmount;
    }

    /**
     * @return string|null
     */
    public function getGvCode(): ?string
    {
        return $this->gvCode;
    }

    /**
     * @param string|null $gvCode
     */
    public function setGvCode(?string $gvCode): void
    {
        $this->gvCode = $gvCode;
    }

    /**
     * @return string|null
     */
    public function getEntryText(): ?string
    {
        return $this->entryText;
    }

    /**
     * @param string|null $entryText
     */
    public function setEntryText(?string $entryText): void
    {
        $this->entryText = $entryText;
    }

    /**
     * @return string|null
     */
    public function getPrimaNotaNo(): ?string
    {
        return $this->primaNotaNo;
    }

    /**
     * @param string|null $primaNotaNo
     */
    public function setPrimaNotaNo(?string $primaNotaNo): void
    {
        $this->primaNotaNo = $primaNotaNo;
    }

    /**
     * @return string|null
     */
    public function getPaymtPurpose(): ?string
    {
        return $this->paymtPurpose;
    }

    /**
     * @param string|null $paymtPurpose
     */
    public function setPaymtPurpose(?string $paymtPurpose): void
    {
        $this->paymtPurpose = $paymtPurpose;
    }

    /**
     * @return string|null
     */
    public function getPayeePayerBankCode(): ?string
    {
        return $this->payeePayerBankCode;
    }

    /**
     * @param string|null $payeePayerBankCode
     */
    public function setPayeePayerBankCode(?string $payeePayerBankCode): void
    {
        $this->payeePayerBankCode = $payeePayerBankCode;
    }

    /**
     * @return string|null
     */
    public function getPayeePayerAcctNo(): ?string
    {
        return $this->payeePayerAcctNo;
    }

    /**
     * @param string|null $payeePayerAcctNo
     */
    public function setPayeePayerAcctNo(?string $payeePayerAcctNo): void
    {
        $this->payeePayerAcctNo = $payeePayerAcctNo;
    }

    /**
     * @return string|null
     */
    public function getPayeePayerName(): ?string
    {
        return $this->payeePayerName;
    }

    /**
     * @param string|null $payeePayerName
     */
    public function setPayeePayerName(?string $payeePayerName): void
    {
        $this->payeePayerName = $payeePayerName;
    }

    /**
     * @return CheckAccount|null
     */
    public function getCheckAccount()
    {
        if ($this->checkAccount !== null && !$this->checkAccount instanceof CheckAccount && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(CheckAccount::class,
                CheckAccount::modelName, "", [], $this->checkAccount->getId());
            $this->checkAccount = !empty($obj[0]) ? $obj[0] : $this->checkAccount;
        }
        return $this->checkAccount;
    }

    /**
     * @param DefaultSevDeskEntity|null $checkAccount
     */
    public function setCheckAccount($checkAccount): void
    {
        $this->checkAccount = $checkAccount;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getScore(): ?string
    {
        return $this->score;
    }

    /**
     * @param string|null $score
     */
    public function setScore(?string $score): void
    {
        $this->score = $score;
    }

    /**
     * @return string|null
     */
    public function getCompareHash(): ?string
    {
        return $this->compareHash;
    }

    /**
     * @param string|null $compareHash
     */
    public function setCompareHash(?string $compareHash): void
    {
        $this->compareHash = $compareHash;
    }

    /**
     * @return bool|null
     */
    public function isEnshrined(): ?bool
    {
        return $this->enshrined;
    }

    /**
     * @param bool|null $enshrined
     */
    public function setEnshrined(?bool $enshrined): void
    {
        $this->enshrined = $enshrined;
    }

    /**
     * @return string|null
     */
    public function getObonoReceiptUuid(): ?string
    {
        return $this->obonoReceiptUuid;
    }

    /**
     * @param string|null $obonoReceiptUuid
     */
    public function setObonoReceiptUuid(?string $obonoReceiptUuid): void
    {
        $this->obonoReceiptUuid = $obonoReceiptUuid;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}