<?php

namespace Zeuch\sevDesk\Model;

class SevClient extends SevDeskEntity
{
    const modelName = "SevClient";

    private $name;
    private $templateMainColor;
    private $templateSubColor;
    private $status;
    private $addressStreet;
    private $addressCity;
    private $addressZip;
    private $contactPhone;
    private $contactEmail;
    private $vatNumber;
    private $ceoName;
    private $website;
    private $bank;
    private $bankNumber;
    private $bankAccountNumber;
    private $bankIban;
    private $bankBic;
    private $taxNumber;
    private $showNet;

    // TODO viele viele weitere Felder (hinzufügen sofern man sie benötigt...)
    
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getTemplateMainColor()
    {
        return $this->templateMainColor;
    }

    public function getTemplateSubColor()
    {
        return $this->templateSubColor;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    public function getAddressCity()
    {
        return $this->addressCity;
    }

    public function getAddressZip()
    {
        return $this->addressZip;
    }

    public function getContactPhone()
    {
        return $this->contactPhone;
    }

    public function getContactEmail()
    {
        return $this->contactEmail;
    }

    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    public function getCeoName()
    {
        return $this->ceoName;
    }

    public function getWebsite()
    {
        return $this->website;
    }

    public function getBank()
    {
        return $this->bank;
    }

    public function getBankNumber()
    {
        return $this->bankNumber;
    }

    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    public function getBankIban()
    {
        return $this->bankIban;
    }

    public function getBankBic()
    {
        return $this->bankBic;
    }

    public function getTaxNumber()
    {
        return $this->taxNumber;
    }

    public function getShowNet()
    {
        return $this->showNet;
    }
}