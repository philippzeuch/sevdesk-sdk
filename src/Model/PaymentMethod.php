<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class PaymentMethod
 * @package Zeuch\sevDesk\Model
 */
class PaymentMethod extends SevDeskEntity
{
    const modelName = "PaymentMethod";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $text;

    /** @var object|null */
    private $additionalInformation;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return object|null
     */
    public function getAdditionalInformation(): ?object
    {
        return $this->additionalInformation;
    }

    /**
     * @param object|null $additionalInformation
     */
    public function setAdditionalInformation(?object $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
