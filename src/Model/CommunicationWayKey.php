<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class CommunicationWayKey (READ-ONLY!)
 * @package Zeuch\sevDesk\Model
 */
class CommunicationWayKey extends SevDeskEntity
{
    const modelName = "CommunicationWayKey";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $translationCode;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getTranslationCode(): ?string
    {
        return $this->translationCode;
    }

    /**
     * @param string|null $translationCode
     */
    public function setTranslationCode(?string $translationCode): void
    {
        $this->translationCode = $translationCode;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}