<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class AccountingChart
 * @package Zeuch\sevDesk\Model
 */
class AccountingChart extends SevDeskEntity
{
    const modelName = "AccountingChart";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    public $create;

    /** @var DateTime|null */
    public $update;

    /** @var string|null */
    public $name;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getParent() {return null;}

    public function setParent($parent) {}

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}