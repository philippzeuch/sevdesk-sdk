<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class Discounts
 * @package Zeuch\sevDesk\Model
 */
class Discounts extends SevDeskEntity
{
    const modelName = "Discounts";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DefaultSevDeskEntity|null */
    private $object;

    /** @var bool|null */
    private $discount;

    /** @var string|null */
    private $text;

    /** @var bool|null */
    private $percentage;

    /** @var float|null */
    private $value;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getObject(): ?DefaultSevDeskEntity
    {
        return $this->object;
    }

    /**
     * @param DefaultSevDeskEntity|null $object
     */
    public function setObject($object): void
    {
        $this->object = $object;
    }

    /**
     * @return bool|null
     */
    public function getDiscount(): ?bool
    {
        return $this->discount;
    }

    /**
     * @param bool|null $discount
     */
    public function setDiscount(?bool $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return bool|null
     */
    public function getPercentage(): ?bool
    {
        return $this->percentage;
    }

    /**
     * @param bool|null $percentage
     */
    public function setPercentage(?bool $percentage): void
    {
        $this->percentage = $percentage;
    }

    /**
     * @return float|null
     */
    public function getValue(): ?float
    {
        return $this->value;
    }

    /**
     * @param float|null $value
     */
    public function setValue(?float $value): void
    {
        $this->value = $value;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}