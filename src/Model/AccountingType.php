<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class AccountingType
 * @package Zeuch\sevDesk\Model
 */
class AccountingType extends SevDeskEntity
{
    const modelName = "AccountingType";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var int|null */
    private $skr03;

    /** @var int|null */
    private $skr03Deprecation;

    /** @var DefaultSevDeskEntity|null */
    private $accountingChart;

    /** @var int|null */
    private $skr04;

    /** @var int|null */
    private $skr04Deprecation;

    /** @var int|null */
    private $skrAt;

    /** @var int|null */
    private $skrAtDeprecation;

    /** @var int|null */
    private $skrCh;

    /** @var int|null */
    private $skrChDeprecation;

    /** @var int|null */
    private $skrGr;

    /** @var int|null */
    private $skrGrDeprecation;

    /** @var string|null */
    private $type;

    /** @var string|null */
    private $translationCode;

    /** @var string|null */
    private $descriptionTranslationCode;

    /** @var string|null */
    private $connotationTranslationCode;

    /** @var string|null */
    private $bookingType;

    /** @var string|null */
    private $assetType;

    /** @var int|null */
    private $status;

    /** @var DefaultSevDeskEntity|null */
    private $accountingSystemNumber;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getSkr03(): ?int
    {
        return $this->skr03;
    }

    /**
     * @param int|null $skr03
     */
    public function setSkr03(?int $skr03): void
    {
        $this->skr03 = $skr03;
    }

    /**
     * @return int|null
     */
    public function getSkr03Deprecation(): ?int
    {
        return $this->skr03Deprecation;
    }

    /**
     * @param int|null $skr03Deprecation
     */
    public function setSkr03Deprecation(?int $skr03Deprecation): void
    {
        $this->skr03Deprecation = $skr03Deprecation;
    }

    /**
     * @return AccountingChart|DefaultSevDeskEntity|null
     */
    public function getAccountingChart()
    {
        if ($this->accountingChart !== null && !$this->accountingChart instanceof AccountingChart && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingChart::class,
                AccountingChart::modelName, "", [], $this->accountingChart->getId());
            $this->accountingChart = !empty($obj[0]) ? $obj[0] : $this->accountingChart;
        }
        return $this->accountingChart;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingChart
     */
    public function setAccountingChart($accountingChart): void
    {
        $this->accountingChart = $accountingChart;
    }

    /**
     * @return int|null
     */
    public function getSkr04(): ?int
    {
        return $this->skr04;
    }

    /**
     * @param int|null $skr04
     */
    public function setSkr04(?int $skr04): void
    {
        $this->skr04 = $skr04;
    }

    /**
     * @return int|null
     */
    public function getSkr04Deprecation(): ?int
    {
        return $this->skr04Deprecation;
    }

    /**
     * @param int|null $skr04Deprecation
     */
    public function setSkr04Deprecation(?int $skr04Deprecation): void
    {
        $this->skr04Deprecation = $skr04Deprecation;
    }

    /**
     * @return int|null
     */
    public function getSkrAt(): ?int
    {
        return $this->skrAt;
    }

    /**
     * @param int|null $skrAt
     */
    public function setSkrAt(?int $skrAt): void
    {
        $this->skrAt = $skrAt;
    }

    /**
     * @return int|null
     */
    public function getSkrAtDeprecation(): ?int
    {
        return $this->skrAtDeprecation;
    }

    /**
     * @param int|null $skrAtDeprecation
     */
    public function setSkrAtDeprecation(?int $skrAtDeprecation): void
    {
        $this->skrAtDeprecation = $skrAtDeprecation;
    }

    /**
     * @return int|null
     */
    public function getSkrCh(): ?int
    {
        return $this->skrCh;
    }

    /**
     * @param int|null $skrCh
     */
    public function setSkrCh(?int $skrCh): void
    {
        $this->skrCh = $skrCh;
    }

    /**
     * @return int|null
     */
    public function getSkrChDeprecation(): ?int
    {
        return $this->skrChDeprecation;
    }

    /**
     * @param int|null $skrChDeprecation
     */
    public function setSkrChDeprecation(?int $skrChDeprecation): void
    {
        $this->skrChDeprecation = $skrChDeprecation;
    }

    /**
     * @return int|null
     */
    public function getSkrGr(): ?int
    {
        return $this->skrGr;
    }

    /**
     * @param int|null $skrGr
     */
    public function setSkrGr(?int $skrGr): void
    {
        $this->skrGr = $skrGr;
    }

    /**
     * @return int|null
     */
    public function getSkrGrDeprecation(): ?int
    {
        return $this->skrGrDeprecation;
    }

    /**
     * @param int|null $skrGrDeprecation
     */
    public function setSkrGrDeprecation(?int $skrGrDeprecation): void
    {
        $this->skrGrDeprecation = $skrGrDeprecation;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getTranslationCode(): ?string
    {
        return $this->translationCode;
    }

    /**
     * @param string|null $translationCode
     */
    public function setTranslationCode(?string $translationCode): void
    {
        $this->translationCode = $translationCode;
    }

    /**
     * @return string|null
     */
    public function getDescriptionTranslationCode(): ?string
    {
        return $this->descriptionTranslationCode;
    }

    /**
     * @param string|null $descriptionTranslationCode
     */
    public function setDescriptionTranslationCode(?string $descriptionTranslationCode): void
    {
        $this->descriptionTranslationCode = $descriptionTranslationCode;
    }

    /**
     * @return string|null
     */
    public function getConnotationTranslationCode(): ?string
    {
        return $this->connotationTranslationCode;
    }

    /**
     * @param string|null $connotationTranslationCode
     */
    public function setConnotationTranslationCode(?string $connotationTranslationCode): void
    {
        $this->connotationTranslationCode = $connotationTranslationCode;
    }

    /**
     * @return string|null
     */
    public function getBookingType(): ?string
    {
        return $this->bookingType;
    }

    /**
     * @param string|null $bookingType
     */
    public function setBookingType(?string $bookingType): void
    {
        $this->bookingType = $bookingType;
    }

    /**
     * @return string|null
     */
    public function getAssetType(): ?string
    {
        return $this->assetType;
    }

    /**
     * @param string|null $assetType
     */
    public function setAssetType(?string $assetType): void
    {
        $this->assetType = $assetType;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return AccountingSystemNumber|DefaultSevDeskEntity|null
     */
    public function getAccountingSystemNumber()
    {
        if ($this->accountingSystemNumber !== null && !$this->accountingSystemNumber instanceof AccountingSystemNumber && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingSystemNumber::class,
                AccountingSystemNumber::modelName, "", [], $this->accountingSystemNumber->getId());
            $this->accountingSystemNumber = !empty($obj[0]) ? $obj[0] : $this->accountingSystemNumber;
        }
        return $this->accountingSystemNumber;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingSystemNumber
     */
    public function setAccountingSystemNumber($accountingSystemNumber): void
    {
        $this->accountingSystemNumber = $accountingSystemNumber;
    }

    /**
     * @return AccountingType|null
     */
    public function getParent()
    {
        if ($this->parent !== null && !$this->parent instanceof AccountingType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingType::class,
                AccountingType::modelName, "", [], $this->parent->getId());
            $this->parent = !empty($obj[0]) ? $obj[0] : $this->parent;
        }
        return $this->parent;
    }

    /**
     * @param DefaultSevDeskEntity|null $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}