<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class AccountingSystemNumber extends SevDeskEntity
{
    const modelName = "AccountingSystemNumber";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var int|null */
    private $number;

    /** @var int|null */
    private $numberDepreciation;

    /** @var DefaultSevDeskEntity|null */
    private $accountingType;

    /** @var DefaultSevDeskEntity|null */
    private $accountingSystem;

    /** @var string|null */
    private $bookingType;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return int|null
     */
    public function getNumber(): ?int
    {
        return $this->number;
    }

    /**
     * @param int|null $number
     */
    public function setNumber(?int $number): void
    {
        $this->number = $number;
    }

    /**
     * @return int|null
     */
    public function getNumberDepreciation(): ?int
    {
        return $this->numberDepreciation;
    }

    /**
     * @param int|null $numberDepreciation
     */
    public function setNumberDepreciation(?int $numberDepreciation): void
    {
        $this->numberDepreciation = $numberDepreciation;
    }

    /**
     * @return AccountingType|DefaultSevDeskEntity|null
     */
    public function getAccountingType()
    {
        if ($this->accountingType !== null && !$this->accountingType instanceof AccountingType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingType::class,
                AccountingType::modelName, "", [], $this->accountingType->getId());
            $this->accountingType = !empty($obj[0]) ? $obj[0] : $this->accountingType;
        }
        return $this->accountingType;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingType
     */
    public function setAccountingType($accountingType): void
    {
        $this->accountingType = $accountingType;
    }

    /**
     * @return AccountingSystem|DefaultSevDeskEntity|null
     */
    public function getAccountingSystem()
    {
        if ($this->accountingSystem !== null && !$this->accountingSystem instanceof AccountingSystem && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingSystem::class,
                AccountingSystem::modelName, "", [], $this->accountingSystem->getId());
            $this->accountingSystem = !empty($obj[0]) ? $obj[0] : $this->accountingSystem;
        }
        return $this->accountingSystem;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingSystem
     */
    public function setAccountingSystem($accountingSystem): void
    {
        $this->accountingSystem = $accountingSystem;
    }

    /**
     * @return string|null
     */
    public function getBookingType(): ?string
    {
        return $this->bookingType;
    }

    /**
     * @param string|null $bookingType
     */
    public function setBookingType(?string $bookingType): void
    {
        $this->bookingType = $bookingType;
    }

    /**
     * @inheritDoc
     */
    public function getParent() {return null;}

    /**
     * @inheritDoc
     */
    public function setParent($parent) {}

    /**
     * @inheritDoc
     */
    public function jsonSerialize() {
        return get_object_vars($this);
    }
}