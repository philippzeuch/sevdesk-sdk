<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class Part
 * @package Zeuch\sevDesk\Model
 */
class Part extends SevDeskEntity
{
    const modelName = "Part";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $partNumber;

    /** @var string|null */
    private $text;

    /** @var DefaultSevDeskEntity|null */
    private $category;

    /** @var float|null */
    private $stock;

    /** @var DefaultSevDeskEntity|null */
    private $unity;

    /** @var float|null */
    private $pricePartner;

    /** @var float|null */
    private $priceCustomer;

    /** @var float|null */
    private $price;

    /** @var DefaultSevDeskEntity|null */
    private $secondUnity;

    /** @var float|null */
    private $secondUnityFactor;

    /** @var float|null */
    private $pricePurchase;

    /** @var float|null */
    private $taxRate;

    /** @var string|null */
    private $image;

    /** @var int|null */
    private $status;

    /** @var string|null */
    private $characteristics;

    /** @var DefaultSevDeskEntity|null */
    private $origin;

    /** @var string|null */
    private $characteristicsString;

    /** @var string|null */
    private $internalComment;

    /** @var DefaultSevDeskEntity|null */
    private $entryType;

    /** @var DefaultSevDeskEntity|null */
    private $accountingType;

    /** @var float|null */
    private $priceNet;

    /** @var float|null */
    private $priceGross;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getPartNumber(): ?string
    {
        return $this->partNumber;
    }

    /**
     * @param string|null $partNumber
     */
    public function setPartNumber(?string $partNumber): void
    {
        $this->partNumber = $partNumber;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return Category|DefaultSevDeskEntity|null
     */
    public function getCategory()
    {
        if ($this->category !== null && !$this->category instanceof Category && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Category::class,
                Category::modelName, "", [], $this->category->getId());
            $this->category = !empty($obj[0]) ? $obj[0] : $this->category;
        }
        return $this->category;
    }

    /**
     * @param DefaultSevDeskEntity|null $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return float|null
     */
    public function getStock(): ?float
    {
        return $this->stock;
    }

    /**
     * @param float|null $stock
     */
    public function setStock(?float $stock): void
    {
        $this->stock = $stock;
    }

    /**
     * @return Unity|DefaultSevDeskEntity|null
     */
    public function getUnity()
    {
        if ($this->unity !== null && !$this->unity instanceof Unity && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Unity::class,
                Unity::modelName, "", [], $this->unity->getId());
            $this->unity = !empty($obj[0]) ? $obj[0] : $this->unity;
        }
        return $this->unity;
    }

    /**
     * @param DefaultSevDeskEntity|null $unity
     */
    public function setUnity($unity): void
    {
        $this->unity = $unity;
    }

    /**
     * @return float|null
     */
    public function getPricePartner(): ?float
    {
        return $this->pricePartner;
    }

    /**
     * @param float|null $pricePartner
     */
    public function setPricePartner(?float $pricePartner): void
    {
        $this->pricePartner = $pricePartner;
    }

    /**
     * @return float|null
     */
    public function getPriceCustomer(): ?float
    {
        return $this->priceCustomer;
    }

    /**
     * @param float|null $priceCustomer
     */
    public function setPriceCustomer(?float $priceCustomer): void
    {
        $this->priceCustomer = $priceCustomer;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return Unity|DefaultSevDeskEntity|null
     */
    public function getSecondUnity()
    {
        if ($this->secondUnity !== null && !$this->secondUnity instanceof Unity && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Unity::class,
                Unity::modelName, "", [], $this->secondUnity->getId());
            $this->secondUnity = !empty($obj[0]) ? $obj[0] : $this->secondUnity;
        }
        return $this->secondUnity;
    }

    /**
     * @param DefaultSevDeskEntity|null|mixed $secondUnity
     */
    public function setSecondUnity($secondUnity): void
    {
        $this->secondUnity = $secondUnity;
    }

    /**
     * @return float|null
     */
    public function getSecondUnityFactor(): ?float
    {
        return $this->secondUnityFactor;
    }

    /**
     * @param float|null $secondUnityFactor
     */
    public function setSecondUnityFactor(?float $secondUnityFactor): void
    {
        $this->secondUnityFactor = $secondUnityFactor;
    }

    /**
     * @return float|null
     */
    public function getPricePurchase(): ?float
    {
        return $this->pricePurchase;
    }

    /**
     * @param float|null $pricePurchase
     */
    public function setPricePurchase(?float $pricePurchase): void
    {
        $this->pricePurchase = $pricePurchase;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    /**
     * @param float|null $taxRate
     */
    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string|null $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getCharacteristics(): ?string
    {
        return $this->characteristics;
    }

    /**
     * @param string|null $characteristics
     */
    public function setCharacteristics(?string $characteristics): void
    {
        $this->characteristics = $characteristics;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param DefaultSevDeskEntity|null $origin
     */
    public function setOrigin($origin): void
    {
        $this->origin = $origin;
    }

    /**
     * @return string|null
     */
    public function getCharacteristicsString(): ?string
    {
        return $this->characteristicsString;
    }

    /**
     * @param string|null $characteristicsString
     */
    public function setCharacteristicsString(?string $characteristicsString): void
    {
        $this->characteristicsString = $characteristicsString;
    }

    /**
     * @return string|null
     */
    public function getInternalComment()
    {
        return $this->internalComment;
    }

    /**
     * @param string|null $internalComment
     */
    public function setInternalComment(?string $internalComment): void
    {
        $this->internalComment = $internalComment;
    }

    /**
     * @return EntryType|DefaultSevDeskEntity|null
     */
    public function getEntryType()
    {
        if ($this->entryType !== null && !$this->entryType instanceof EntryType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(EntryType::class,
                EntryType::modelName, "", [], $this->entryType->getId());
            $this->entryType = !empty($obj[0]) ? $obj[0] : $this->entryType;
        }
        return $this->entryType;
    }

    /**
     * @param DefaultSevDeskEntity|null|mixed $entryType
     */
    public function setEntryType($entryType): void
    {
        $this->entryType = $entryType;
    }

    /**
     * @return AccountingType|DefaultSevDeskEntity|null
     */
    public function getAccountingType()
    {
        if ($this->accountingType !== null && !$this->accountingType instanceof AccountingType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingType::class,
                AccountingType::modelName, "", [], $this->accountingType->getId());
            $this->accountingType = !empty($obj[0]) ? $obj[0] : $this->accountingType;
        }
        return $this->accountingType;
    }

    /**
     * @param DefaultSevDeskEntity|null|mixed $accountingType
     */
    public function setAccountingType($accountingType): void
    {
        $this->accountingType = $accountingType;
    }

    /**
     * @return float|null
     */
    public function getPriceNet(): ?float
    {
        return $this->priceNet;
    }

    /**
     * @param float|null $priceNet
     */
    public function setPriceNet(?float $priceNet): void
    {
        $this->priceNet = $priceNet;
    }

    /**
     * @return float|null
     */
    public function getPriceGross(): ?float
    {
        return $this->priceGross;
    }

    /**
     * @param float|null $priceGross
     */
    public function setPriceGross(?float $priceGross): void
    {
        $this->priceGross = $priceGross;
    }

    public function getTyp()
    {
        if ($this->internalComment) {
            $comment = json_decode($this->internalComment);
            return $comment ? $comment->typ : null;
        }
        return null;
    }

    public function getFarbe()
    {
        if ($this->internalComment) {
            $comment = json_decode($this->internalComment);
            return $comment ? $comment->farbe : null;
        }
        return null;
    }

    public function getGroesse()
    {
        if ($this->internalComment) {
            $comment = json_decode($this->internalComment);
            return $comment ? $comment->groesse : null;
        }
        return null;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function equals(?Part $part)
    {
        return $part !== null && $this->getName() === $part->getName() && $this->getPartNumber() === $part->getPartNumber()
            && $this->getPrice() === $part->getPrice() && $this->getPriceGross() === $part->getPriceGross()
            && $this->getText() === $part->getText() && $this->getGroesse() === $part->getGroesse()
            && $this->getFarbe() === $part->getFarbe() && $this->getTyp() === $part->getTyp();
    }
}