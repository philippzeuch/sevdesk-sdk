<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class ContactAddress extends SevDeskEntity
{
    const modelName = "ContactAddress";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DefaultSevDeskEntity|null */
    private $contact;

    /** @var string|null */
    private $street;

    /** @var string|null */
    private $zip;

    /** @var string|null */
    private $city;

    /** @var DefaultSevDeskEntity|null */
    private $country;

    /** @var DefaultSevDeskEntity|null */
    private $category;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $name2;

    /** @var string|null */
    private $name3;

    /** @var string|null */
    private $name4;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return Contact|DefaultSevDeskEntity|null
     */
    public function getContact()
    {
        if ($this->contact !== null && !$this->contact instanceof Contact && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Contact::class,
                Contact::modelName, "", [], $this->contact->getId());
            $this->contact = !empty($obj[0]) ? $obj[0] : $this->contact;
        }
        return $this->contact;
    }

    /**
     * @param DefaultSevDeskEntity|null $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     */
    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->zip;
    }

    /**
     * @param string|null $zip
     */
    public function setZip(?string $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return StaticCountry|DefaultSevDeskEntity|null
     */
    public function getCountry()
    {
        if ($this->country !== null && !$this->country instanceof StaticCountry && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(StaticCountry::class,
                StaticCountry::modelName, "", [], $this->country->getId());
            $this->country = !empty($obj[0]) ? $obj[0] : $this->country;
        }
        return $this->country;
    }

    /**
     * @param DefaultSevDeskEntity|null $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return Category|DefaultSevDeskEntity|null
     */
    public function getCategory($reload = true)
    {
        if ($reload && $this->category !== null && !$this->category instanceof Category && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Category::class,
                Category::modelName, "", [], $this->category->getId());
            $this->category = !empty($obj[0]) ? $obj[0] : $this->category;
        }
        return $this->category;
    }

    /**
     * @param DefaultSevDeskEntity|null $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getName2(): ?string
    {
        return $this->name2;
    }

    /**
     * @param string|null $name2
     */
    public function setName2(?string $name2): void
    {
        $this->name2 = $name2;
    }

    /**
     * @return string|null
     */
    public function getName3(): ?string
    {
        return $this->name3;
    }

    /**
     * @param string|null $name3
     */
    public function setName3(?string $name3): void
    {
        $this->name3 = $name3;
    }

    /**
     * @return string|null
     */
    public function getName4(): ?string
    {
        return $this->name4;
    }

    /**
     * @param string|null $name4
     */
    public function setName4(?string $name4): void
    {
        $this->name4 = $name4;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function toString()
    {
        $zip = !empty($this->getZip()) ? ", " . $this->getZip() : "";
        $city = !empty($this->getCity()) ? (!empty($zip) ? " " . $this->getCity() : ", " . $this->getCity()) : "";
        $country = !empty($city) && $this->getCountry() !== null ? ", " . ($this->getCountry()->getName()) : "";
        return $this->getStreet() . $zip . $city . $country;
    }
}