<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class CheckAccountTransactionLog
 * @package Zeuch\sevDesk\Model
 */
class CheckAccountTransactionLog extends SevDeskEntity
{
    const modelName = "CheckAccountTransactionLog";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DefaultSevDeskEntity|null */
    private $checkAccountTransaction;

    /** @var int|null */
    private $fromStatus;

    /** @var int|null */
    private $toStatus;

    /** @var float|null */
    private $amountPaid;

    /** @var DateTime|null */
    private $bookingDate;

    /** @var DefaultSevDeskEntity|null */
    private $object;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return CheckAccountTransaction|DefaultSevDeskEntity|null
     */
    public function getCheckAccountTransaction()
    {
        if ($this->checkAccountTransaction !== null
            && !$this->checkAccountTransaction instanceof CheckAccountTransaction && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(CheckAccountTransaction::class,
                CheckAccountTransaction::modelName, "", [], $this->checkAccountTransaction->getId());
            $this->checkAccountTransaction = !empty($obj[0]) ? $obj[0] : $this->checkAccountTransaction;
        }
        return $this->checkAccountTransaction;
    }

    /**
     * @param DefaultSevDeskEntity|null $checkAccountTransaction
     */
    public function setCheckAccountTransaction($checkAccountTransaction): void
    {
        $this->checkAccountTransaction = $checkAccountTransaction;
    }

    /**
     * @return int|null
     */
    public function getFromStatus(): ?int
    {
        return $this->fromStatus;
    }

    /**
     * @param int|null $fromStatus
     */
    public function setFromStatus(?int $fromStatus): void
    {
        $this->fromStatus = $fromStatus;
    }

    /**
     * @return int|null
     */
    public function getToStatus(): ?int
    {
        return $this->toStatus;
    }

    /**
     * @param int|null $toStatus
     */
    public function setToStatus(?int $toStatus): void
    {
        $this->toStatus = $toStatus;
    }

    /**
     * @return float|null
     */
    public function getAmountPaid(): ?float
    {
        return $this->amountPaid;
    }

    /**
     * @param float|null $amountPaid
     */
    public function setAmountPaid(?float $amountPaid): void
    {
        $this->amountPaid = $amountPaid;
    }

    /**
     * @return DateTime|null
     */
    public function getBookingDate(): ?DateTime
    {
        return $this->bookingDate;
    }

    /**
     * @param DateTime|null $bookingDate
     */
    public function setBookingDate(?DateTime $bookingDate): void
    {
        $this->bookingDate = $bookingDate;
    }

    /**
     * @return Invoice|Voucher|DefaultSevDeskEntity|null
     */
    public function getObject()
    {
        if ($this->object !== null && $this->object instanceof DefaultSevDeskEntity && $this->repo !== null) {
            $resultType = Invoice::class;
            if ($this->object->getObjectName() == Voucher::modelName) {
                $resultType = Voucher::class;
            } else if ($this->object->getObjectName() == CreditNote::modelName) {
                $resultType = CreditNote::class;
            }
            $obj = $this->repo->getFromSevDesk($resultType, $this->object->getObjectName(), "", [], $this->object->getId());
            $this->object = !empty($obj[0]) ? $obj[0] : $this->object;
        }
        return $this->object;
    }

    /**
     * @param DefaultSevDeskEntity|null $object
     */
    public function setObject($object): void
    {
        $this->object = $object;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}