<?php

namespace Zeuch\sevDesk\Model;

class Country extends SevDeskEntity
{
    const modelName = "StaticCountry";

    protected $objectName = self::modelName;

    public $code; //String

    public $name; //String

    public $nameEn; //String

    public $translationCode; //String

    public $locale; //String

    public $priority; //int

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;
    }

    /**
     * @return string
     */
    public function getTranslationCode()
    {
        return $this->translationCode;
    }

    public function setTranslationCode($translationCode)
    {
        $this->translationCode = $translationCode;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}