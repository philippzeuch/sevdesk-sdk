<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class OrderPos extends SevDeskEntity
{
    const modelName = "OrderPos";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    public $create;

    /** @var DateTime|null */
    public $update;

    /** @var DefaultSevDeskEntity|null */
    public $order;

    /** @var DefaultSevDeskEntity|null */
    public $part;

    /** @var int|null */
    public $quantity;

    /** @var float|null */
    public $price;

    /** @var string|null */
    public $name;

    /** @var int|null */
    public $priority;

    /** @var DefaultSevDeskEntity|null */
    public $unity;

    /** @var int|null */
    public $positionNumber;

    /** @var string|null */
    public $text;

    /** @var float|null */
    public $discount;

    /** @var bool|null */
    public $optional;

    /** @var bool|null */
    public $optionalChargeable;

    /** @var float|null */
    public $taxRate = 0;

    /** @var float|null */
    public $sumNet;

    /** @var float|null */
    public $sumGross;

    /** @var float|null */
    public $sumDiscount;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return Order|DefaultSevDeskEntity|null
     */
    public function getOrder()
    {
        if ($this->order !== null && !$this->order instanceof Order && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Order::class,
                Order::modelName, "", [], $this->order->getId());
            $this->order = !empty($obj[0]) ? $obj[0] : $this->order;
        }
        return $this->order;
    }

    /**
     * @param DefaultSevDeskEntity|null $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }

    /**
     * @return Part|DefaultSevDeskEntity|null
     */
    public function getPart()
    {
        if ($this->part !== null && !$this->part instanceof Part && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Part::class,
                Part::modelName, "", [], $this->part->getId());
            $this->part = !empty($obj[0]) ? $obj[0] : $this->part;
        }
        return $this->part;
    }

    /**
     * @param DefaultSevDeskEntity|null $part
     */
    public function setPart($part): void
    {
        $this->part = $part;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     */
    public function setQuantity(?int $quantity): void
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float|null $price
     */
    public function setPrice(?float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int|null $priority
     */
    public function setPriority(?int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return Unity|DefaultSevDeskEntity|null
     */
    public function getUnity()
    {
        if ($this->unity !== null && !$this->unity instanceof Unity && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Unity::class,
                Unity::modelName, "", [], $this->unity->getId());
            $this->unity = !empty($obj[0]) ? $obj[0] : $this->unity;
        }
        return $this->unity;
    }

    /**
     * @param DefaultSevDeskEntity|null $unity
     */
    public function setUnity($unity): void
    {
        $this->unity = $unity;
    }

    /**
     * @return int|null
     */
    public function getPositionNumber(): ?int
    {
        return $this->positionNumber;
    }

    /**
     * @param int|null $positionNumber
     */
    public function setPositionNumber(?int $positionNumber): void
    {
        $this->positionNumber = $positionNumber;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return float|null
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float|null $discount
     */
    public function setDiscount(?float $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return bool|null
     */
    public function getOptional(): ?bool
    {
        return $this->optional;
    }

    /**
     * @param bool|null $optional
     */
    public function setOptional(?bool $optional): void
    {
        $this->optional = $optional;
    }

    /**
     * @return bool|null
     */
    public function getOptionalChargeable(): ?bool
    {
        return $this->optionalChargeable;
    }

    /**
     * @param bool|null $optionalChargeable
     */
    public function setOptionalChargeable(?bool $optionalChargeable): void
    {
        $this->optionalChargeable = $optionalChargeable;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    /**
     * @param float|null $taxRate
     */
    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return float|null
     */
    public function getSumNet(): ?float
    {
        return $this->sumNet;
    }

    /**
     * @param float|null $sumNet
     */
    public function setSumNet(?float $sumNet): void
    {
        $this->sumNet = $sumNet;
    }

    /**
     * @return float|null
     */
    public function getSumGross(): ?float
    {
        return $this->sumGross;
    }

    /**
     * @param float|null $sumGross
     */
    public function setSumGross(?float $sumGross): void
    {
        $this->sumGross = $sumGross;
    }

    /**
     * @return float|null
     */
    public function getSumDiscount(): ?float
    {
        return $this->sumDiscount;
    }

    /**
     * @param float|null $sumDiscount
     */
    public function setSumDiscount(?float $sumDiscount): void
    {
        $this->sumDiscount = $sumDiscount;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}