<?php

namespace Zeuch\sevDesk\Model;

class StaticCountry extends SevDeskEntity
{
    const modelName = "StaticCountry";

    protected $objectName = self::modelName;

    /** @var string|null */
    private $code;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $nameEn;

    /** @var string|null */
    private $translationCode;

    /** @var string|null */
    private $locale;

    /** @var int|null */
    private $priority;

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getNameEn(): ?string
    {
        return $this->nameEn;
    }

    /**
     * @param string|null $nameEn
     */
    public function setNameEn(?string $nameEn): void
    {
        $this->nameEn = $nameEn;
    }

    /**
     * @return string|null
     */
    public function getTranslationCode(): ?string
    {
        return $this->translationCode;
    }

    /**
     * @param string|null $translationCode
     */
    public function setTranslationCode(?string $translationCode): void
    {
        $this->translationCode = $translationCode;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     */
    public function setLocale(?string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int|null $priority
     */
    public function setPriority(?int $priority): void
    {
        $this->priority = $priority;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}