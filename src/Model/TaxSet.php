<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class TaxSet extends SevDeskEntity
{
    const modelName = "TaxSet";

    protected $objectName = self::modelName;

    public $create; //Date

    public $update; //Date

    public $sevClient; //

    public $text; //String

    public $taxRate; //int

    public $code; //int

    public $displayText; //String

    public $vatReportFieldNet; //String

    public $vatReportFieldTax; //String

    public $showInvoice; //boolean

    public $showDebitVoucher; //boolean

    public $accountingExportVatField; //String

    public $showCreditVoucher; //boolean

    public $onlyForVatDec; //boolean

    /**
     * @return DateTime
     */
    public function getCreate() {
        return $this->create;
    }

    public function setCreate($create) {
        $this->create = $create;
    }

    /**
     * @return DateTime
     */
    public function getUpdate() {
        return $this->update;
    }
    public function setUpdate($update) {
        $this->update = $update;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @return double
     */
    public function getTaxRate() {
        return $this->taxRate;
    }

    public function setTaxRate($taxRate) {
        $this->taxRate = $taxRate;
    }

    /**
     * @return double
     */
    public function getCode() {
        return $this->code;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getDisplayText() {
        return $this->displayText;
    }

    public function setDisplayText($displayText) {
        $this->displayText = $displayText;
    }

    /**
     * @return string
     */
    public function getVatReportFieldNet() {
        return $this->vatReportFieldNet;
    }

    public function setVatReportFieldNet($vatReportFieldNet) {
        $this->vatReportFieldNet = $vatReportFieldNet;
    }

    /**
     * @return string
     */
    public function getVatReportFieldTax() {
        return $this->vatReportFieldTax;
    }

    public function setVatReportFieldTax($vatReportFieldTax) {
        $this->vatReportFieldTax = $vatReportFieldTax;
    }

    /**
     * @return string
     */
    public function getAccountingExportVatField() {
        return $this->accountingExportVatField;
    }

    public function setAccountingExportVatField($accountingExportVatField) {
        $this->accountingExportVatField = $accountingExportVatField;
    }

    /**
     * @return boolean
     */
    public function getShowInvoice() {
        return $this->showInvoice;
    }

    public function setShowInvoice($showInvoice) {
        $this->showInvoice = $showInvoice;
    }

    /**
     * @return boolean
     */
    public function getShowDebitVoucher() {
        return $this->showDebitVoucher;
    }

    public function setShowDebitVoucher($showDebitVoucher) {
        $this->showDebitVoucher = $showDebitVoucher;
    }

    /**
     * @return boolean
     */
    public function getShowCreditVoucher() {
        return $this->showCreditVoucher;
    }

    public function setShowCreditVoucher($showCreditVoucher) {
        $this->showCreditVoucher = $showCreditVoucher;
    }

    /**
     * @return boolean
     */
    public function getOnlyForVatDec() {
        return $this->onlyForVatDec;
    }
    public function setOnlyForVatDec($onlyForVatDec) {
        $this->onlyForVatDec = $onlyForVatDec;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}