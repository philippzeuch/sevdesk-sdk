<?php

namespace Zeuch\sevDesk\Model;

use JsonSerializable;
use Psr\Http\Message\ResponseInterface;
use Zeuch\sevDesk\Repo\SevDeskRepo;

abstract class SevDeskEntity implements JsonSerializable
{
    const modelName = "";

    /** @var int|null */
    protected $id;

    /** @var string|null */
    protected $objectName;

    /** @var DefaultSevDeskEntity|null */
    protected $sevClient;

    /** @var DefaultSevDeskEntity|null */
    protected $parent;

    /** @var string|null */
    public $updateType;

    /** @var array  */
    protected $excludedForPersistence = [
        'modelName', 'sevClient', 'persistByFunction',
        'repo', 'excludedForPersistence', 'updateType'
    ];

    protected $persistByFunction = [];

    protected $repo;

    public function __debugInfo()
    {
        $result = (array) $this;
        $result["\0*\0repo"] = $result["\0*\0repo"] != null ? "Hier das MEGA grosse Repo Objekt" : null;
        return $result;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getObjectName(): ?string
    {
        return $this->objectName;
    }

    /**
     * @param string $objectName
     */
    public function setObjectName(?string $objectName): void
    {
        $this->objectName = $objectName;
    }

    /**
     * @return SevDeskRepo|null
     */
    public function getRepo(): ?SevDeskRepo
    {
        return $this->repo;
    }

    /**
     * @param \Zeuch\sevDesk\Repo\SevDeskRepo|null $repo
     */
    public function setRepo($repo): void
    {
        $this->repo = $repo;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getSevClient(): ?DefaultSevDeskEntity
    {
        return $this->sevClient; // Das "SevClient" Model kann ich mir sparen, da es uns nichts bringt
    }

    /**
     * @param DefaultSevDeskEntity|null $sevClient
     */
    public function setSevClient(?DefaultSevDeskEntity $sevClient): void
    {
        $this->sevClient = $sevClient;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getParent() {
        return $this->parent;
    }

    /**
     * @param DefaultSevDeskEntity|null $parent
     */
    public function setParent($parent) {
        $this->parent = $parent;
    }

    /**
     * Manche Felder in Entities sind von uns separat angelegt, obwohl sie im Model von sevDesk nicht vorkommen.<br>
     * Diese gehören i.d.R zu den Feldern, welche mittels einer separaten Methode, also einem separatem Request
     * gesetzt werden müssen (ziemlich behindert). Diese Feld-Namen müssen dann in dem Array <b>persistByFunction</b>
     * enthalten sein.<br>
     * Mit dieser Methode werden diese Felder abgearbeitet.
     * @param $field
     * @return ResponseInterface|null
     */
    public function persistFunctionField($field) {
        /** @var ResponseInterface $response */
        $response = null;
        if (!empty($field) && !empty($this->persistByFunction[$field]) && $this->repo != null) {
            $response = $this->repo->{$this->persistByFunction[$field]}($this, $field);
        }
        return $response;
    }

    /**
     * @return string|null
     */
    public function getUpdateType(): ?string
    {
        return $this->updateType;
    }

    /**
     * @param string|null $updateType
     */
    public function setUpdateType(?string $updateType): void
    {
        $this->updateType = $updateType;
    }

    /**
     * @return array
     */
    public function getExcludedForPersistence(): array
    {
        $formattedArray = array();
        foreach ($this->excludedForPersistence as $field) {
            $formattedArray[$field] = null;
        }
        return $formattedArray;
    }

    public static function getClass()
    {
        return get_called_class();
    }

    /**
     * @return array
     */
    public function getPersistByFunction(): array
    {
        return $this->persistByFunction;
    }

    /**
     * @param array $persistByFunction
     */
    public function setPersistByFunction(array $persistByFunction): void
    {
        $this->persistByFunction = $persistByFunction;
    }

    public function __sleep()
    {
        $this->repo = null;
        return array_keys((array) $this);
    }
}