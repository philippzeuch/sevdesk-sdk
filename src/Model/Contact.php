<?php

namespace Zeuch\sevDesk\Model;

use Zeuch\sevDesk\Repo\ContactRepo;
use DateTime;

/**
 * Class Contact
 * @package Zeuch\sevDesk\Model
 */
class Contact extends SevDeskEntity
{
    const modelName = "Contact";

    protected $objectName = self::modelName;

    /** @var DefaultSevDeskEntity|null */
    private $address;

    /** @var object|null */
    private $additionalInformation;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $status;

    /** @var integer|null */
    private $customerNumber; // Identifizierendes Merkmal

    /** @var string|null */
    private $surename;

    /** @var string|null */
    private $familyname;

    /** @var string|null */
    private $titel;

    /** @var DefaultSevDeskEntity|null */
    private $category;

    /** @var string|null */
    private $description;

    /** @var string|null */
    private $academicTitle;

    /** @var string|null */
    private $gender;

    /** @var string|null */
    private $name2;

    /** @var DateTime|null */
    private $birthday;

    /** @var string|null */
    private $vatNumber;

    /** @var string|null */
    private $bankAccount;

    /** @var string|null */
    private $bankNumber;

    /** @var DefaultSevDeskEntity|null */
    private $paymentMethod;

    /** @var DefaultSevDeskEntity|null */
    private $entryType;

    /** @var integer|null */
    private $defaultCashbackTime;

    /** @var integer|null */
    private $defaultCashbackPercent;

    /** @var integer|null */
    private $defaultTimeToPay;

    /** @var string|null */
    private $taxNumber;

    /** @var string|null */
    private $taxOffice;

    /** @var string|null */
    private $exemptVat;

    /** @var string|null */
    private $taxType;

    /** @var float|null */
    private $defaultDiscountAmount;

    /** @var float|null */
    private $defaultDiscountPercentage;

    /**
     * Achtung: Dieser Wert muss separat persistiert werden!
     * @var DefaultSevDeskEntity|null
     */
    private $email;

    /**
     * Achtung: Dieser Wert muss separat persistiert werden!
     * @var DefaultSevDeskEntity|null
     */
    private $telefon;

    public function __construct()
    {
        array_push($this->excludedForPersistence, 'email');
        array_push($this->excludedForPersistence, 'telefon');
        $this->persistByFunction['email'] = 'persistEmail';
        $this->persistByFunction['telefon'] = 'persistPhone';
    }

    /**
     * @return object|null
     */
    public function getAdditionalInformation(): ?object
    {
        return $this->additionalInformation;
    }

    /**
     * @param object|null $additionalInformation
     */
    public function setAdditionalInformation(?object $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = trim($name);
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getCustomerNumber(): ?int
    {
        return $this->customerNumber;
    }

    /**
     * @param int|null $customerNumber
     */
    public function setCustomerNumber(?int $customerNumber): void
    {
        $this->customerNumber = $customerNumber;
    }

    /**
     * @return string|null
     */
    public function getSurename(): ?string
    {
        return $this->surename;
    }

    /**
     * @param string|null $surename
     */
    public function setSurename(?string $surename): void
    {
        $this->surename = trim($surename);
    }

    /**
     * @return string|null
     */
    public function getFamilyname(): ?string
    {
        return $this->familyname;
    }

    /**
     * @param string|null $familyname
     */
    public function setFamilyname(?string $familyname): void
    {
        $this->familyname = trim($familyname);
    }

    /**
     * @return string|null
     */
    public function getTitel(): ?string
    {
        return $this->titel;
    }

    /**
     * @param string|null $titel
     */
    public function setTitel(?string $titel): void
    {
        $this->titel = $titel;
    }

    /**
     * @return Category|DefaultSevDeskEntity|null
     */
    public function getCategory($plain = false)
    {
        if (!$plain && $this->category !== null && !$this->category instanceof Category && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Category::class,
                Category::modelName, "", [], $this->category->getId());
            $this->category = !empty($obj[0]) ? $obj[0] : $this->category;
        }
        return $this->category;
    }

    /**
     * @param DefaultSevDeskEntity|null $category
     */
    public function setCategory($category): void
    {
        $this->category = $category;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getAcademicTitle(): ?string
    {
        return $this->academicTitle;
    }

    /**
     * @param string|null $academicTitle
     */
    public function setAcademicTitle(?string $academicTitle): void
    {
        $this->academicTitle = $academicTitle;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     */
    public function setGender(?string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getName2(): ?string
    {
        return $this->name2;
    }

    /**
     * @param string|null $name2
     */
    public function setName2(?string $name2): void
    {
        $this->name2 = $name2;
    }

    /**
     * @return DateTime|null
     */
    public function getBirthday(): ?DateTime
    {
        return $this->birthday;
    }

    /**
     * @param DateTime|null $birthday
     */
    public function setBirthday(?DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return $this->vatNumber;
    }

    /**
     * @param string|null $vatNumber
     */
    public function setVatNumber(?string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return string|null
     */
    public function getBankAccount(): ?string
    {
        return $this->bankAccount;
    }

    /**
     * @param string|null $bankAccount
     */
    public function setBankAccount(?string $bankAccount): void
    {
        $this->bankAccount = $bankAccount;
    }

    /**
     * @return string|null
     */
    public function getBankNumber(): ?string
    {
        return $this->bankNumber;
    }

    /**
     * @param string|null $bankNumber
     */
    public function setBankNumber(?string $bankNumber): void
    {
        $this->bankNumber = $bankNumber;
    }

    /**
     * @return PaymentMethod|DefaultSevDeskEntity|null
     */
    public function getPaymentMethod()
    {
        if ($this->paymentMethod !== null && !$this->paymentMethod instanceof PaymentMethod && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(PaymentMethod::class,
                PaymentMethod::modelName, "", [], $this->paymentMethod->getId());
            $this->paymentMethod = !empty($obj[0]) ? $obj[0] : $this->paymentMethod;
        }
        return $this->paymentMethod;
    }

    /**
     * @param DefaultSevDeskEntity|null $paymentMethod
     */
    public function setPaymentMethod($paymentMethod): void
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return EntryType|DefaultSevDeskEntity|null
     */
    public function getEntryType()
    {
        if ($this->entryType !== null && !$this->entryType instanceof EntryType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(EntryType::class,
                EntryType::modelName, "", [], $this->entryType->getId());
            $this->entryType = !empty($obj[0]) ? $obj[0] : $this->entryType;
        }
        return $this->entryType;
    }

    /**
     * @param DefaultSevDeskEntity|null $entryType
     */
    public function setEntryType($entryType): void
    {
        $this->entryType = $entryType;
    }

    /**
     * @return int|null
     */
    public function getDefaultCashbackTime(): ?int
    {
        return $this->defaultCashbackTime;
    }

    /**
     * @param int|null $defaultCashbackTime
     */
    public function setDefaultCashbackTime(?int $defaultCashbackTime): void
    {
        $this->defaultCashbackTime = $defaultCashbackTime;
    }

    /**
     * @return int|null
     */
    public function getDefaultCashbackPercent(): ?int
    {
        return $this->defaultCashbackPercent;
    }

    /**
     * @param int|null $defaultCashbackPercent
     */
    public function setDefaultCashbackPercent(?int $defaultCashbackPercent): void
    {
        $this->defaultCashbackPercent = $defaultCashbackPercent;
    }

    /**
     * @return int|null
     */
    public function getDefaultTimeToPay(): ?int
    {
        return $this->defaultTimeToPay;
    }

    /**
     * @param int|null $defaultTimeToPay
     */
    public function setDefaultTimeToPay(?int $defaultTimeToPay): void
    {
        $this->defaultTimeToPay = $defaultTimeToPay;
    }

    /**
     * @return string|null
     */
    public function getTaxNumber(): ?string
    {
        return $this->taxNumber;
    }

    /**
     * @param string|null $taxNumber
     */
    public function setTaxNumber(?string $taxNumber): void
    {
        $this->taxNumber = $taxNumber;
    }

    /**
     * @return string|null
     */
    public function getTaxOffice(): ?string
    {
        return $this->taxOffice;
    }

    /**
     * @param string|null $taxOffice
     */
    public function setTaxOffice(?string $taxOffice): void
    {
        $this->taxOffice = $taxOffice;
    }

    /**
     * @return string|null
     */
    public function getExemptVat(): ?string
    {
        return $this->exemptVat;
    }

    /**
     * @param string|null $exemptVat
     */
    public function setExemptVat(?string $exemptVat): void
    {
        $this->exemptVat = $exemptVat;
    }

    /**
     * @return string|null
     */
    public function getTaxType(): ?string
    {
        return $this->taxType;
    }

    /**
     * @param string|null $taxType
     */
    public function setTaxType(?string $taxType): void
    {
        $this->taxType = $taxType;
    }

    /**
     * @return float|null
     */
    public function getDefaultDiscountAmount(): ?float
    {
        return $this->defaultDiscountAmount;
    }

    /**
     * @param float|null $defaultDiscountAmount
     */
    public function setDefaultDiscountAmount(?float $defaultDiscountAmount): void
    {
        $this->defaultDiscountAmount = $defaultDiscountAmount;
    }

    /**
     * @return float|null
     */
    public function getDefaultDiscountPercentage(): ?float
    {
        return $this->defaultDiscountPercentage;
    }

    /**
     * @param float|null $defaultDiscountPercentage
     */
    public function setDefaultDiscountPercentage(?float $defaultDiscountPercentage): void
    {
        $this->defaultDiscountPercentage = $defaultDiscountPercentage;
    }

    /**
     * Hole die Haupt-Adresse des Kontakts
     * @return ContactAddress|DefaultSevDeskEntity
     */
    public function getAddress()
    {
        if ($this->address !== null && !$this->address instanceof ContactAddress && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(ContactAddress::class, Contact::modelName, "getMainAddress", [], $this->id);
            $this->address = !empty($obj[0]) ? $obj[0] : $this->address;
        }
        return $this->address;
    }

    /**
     * @param DefaultSevDeskEntity|null $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }


    /**
     * @return Contact
     */
    public function getParent()
    {
        if ($this->parent !== null && !$this->parent instanceof Contact && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Contact::class, Contact::modelName, "", [], $this->parent->getId());
            $this->parent = !empty($obj[0]) ? $obj[0] : $this->parent;
        }
        return $this->parent;
    }

    /**
     * @param DefaultSevDeskEntity|null $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return CommunicationWay|null
     */
    public function getEmail(): ?CommunicationWay
    {
        if ($this->email !== null && !$this->email instanceof CommunicationWay && $this->repo !== null) {
            /** @var ContactRepo $repo */
            $repo = $this->repo;
            $obj = $repo->getContactEmail($this->id);
            $this->email = $obj != null ? $obj : $this->email;
        }
        return $this->email;
    }

    /**
     * @param DefaultSevDeskEntity|null $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return CommunicationWay|null
     */
    public function getTelefon(): ?CommunicationWay
    {
        if ($this->telefon !== null && !$this->telefon instanceof CommunicationWay && $this->repo !== null) {
            /** @var ContactRepo $repo */
            $repo = $this->repo;
            $obj = $repo->getContactTelefon($this->id);
            $this->telefon = $obj !== null ? $obj : $this->telefon;
        }
        return $this->telefon;
    }

    /**
     * @param DefaultSevDeskEntity|null $telefon
     */
    public function setTelefon($telefon): void
    {
        $this->telefon = $telefon;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}