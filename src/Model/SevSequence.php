<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class SevSequence extends SevDeskEntity
{
    const modelName = "SevSequence";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $forObject;

    /** @var string|null */
    private $format;

    /** @var int|null */
    private $nextSequence;

    /** @var string|null */
    private $type;

    /** @var string|null */
    private $additionalInformation;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getForObject(): ?string
    {
        return $this->forObject;
    }

    /**
     * @param string|null $forObject
     */
    public function setForObject(?string $forObject): void
    {
        $this->forObject = $forObject;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     */
    public function setFormat(?string $format): void
    {
        $this->format = $format;
    }

    /**
     * @return int|null
     */
    public function getNextSequence(): ?int
    {
        return $this->nextSequence;
    }

    /**
     * @param int|null $nextSequence
     */
    public function setNextSequence(?int $nextSequence): void
    {
        $this->nextSequence = $nextSequence;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    /**
     * @param string|null $additionalInformation
     */
    public function setAdditionalInformation(?string $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}