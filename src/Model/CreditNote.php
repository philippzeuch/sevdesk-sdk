<?php

namespace Zeuch\sevDesk\Model;

use DateTime;
use Zeuch\sevDesk\Utils\TaxType;

/**
 * Class CreditNote
 * @package Zeuch\sevDesk\Model
 */
class CreditNote extends SevDeskEntity
{
    const modelName = "CreditNote";

    protected $objectName = self::modelName;

    /** @var object|null */
    private $additionalInformation;

    /** @var string|null */
    private $creditNoteNumber;

    /** @var DefaultSevDeskEntity|null */
    private $contact;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var DateTime|null */
    private $creditNoteDate;

    /** @var string|null */
    private $header;

    /** @var string|null */
    private $headText;

    /** @var string|null */
    private $footText;

    /** @var string|null */
    private $timeToPay;

    /** @var string|null */
    private $discountTime = "0";

    /** @var float|null */
    private $discount = 0;

    /** @var string|null */
    private $addressName;

    /** @var string|null */
    private $addressStreet;

    /** @var string|null */
    private $addressZip;

    /** @var string|null */
    private $addressCity;

    /** @var DefaultSevDeskEntity|null */
    private $addressCountry;

    /** @var string|null */
    private $payDate;

    /** @var DefaultSevDeskEntity|null */
    private $createUser;

    /** @var DateTime|null */
    private $deliveryDate;

    /** @var int|null */
    private $status;

    /** @var string|null */
    private $smallSettlement;

    /** @var DefaultSevDeskEntity|null */
    private $contactPerson;

    /** @var float|null */
    private $taxRate = 0;

    /** @var string|null */
    private $taxText = "0";

    /** @var int|null */
    private $dunningLevel;

    /** @var string|null */
    private $addressParentName;

    /** @var string|null */
    private $taxType = TaxType::DEFAULT;

    /** @var DateTime|null */
    private $sendDate;

    /** @var string|null */
    private $creditNoteType;

    /** @var string|null */
    private $accountIntervall;

    /** @var string|null */
    private $accountLastCreditNote;

    /** @var string|null */
    private $accountNextCreditNote;

    /** @var float|null */
    private $reminderTotal;

    /** @var float|null */
    private $reminderDebit;

    /** @var string|null */
    private $reminderDeadline;

    /** @var float|null */
    private $reminderCharge;

    /** @var string|null */
    private $addressParentName2;

    /** @var string|null */
    private $addressName2;

    /** @var string|null */
    private $addressGender;

    /** @var string|null */
    private $accountEndDate;

    /** @var string|null */
    private $address;

    /** @var string|null */
    private $currency = "EUR";

    /** @var float|null */
    private $sumNet;

    /** @var float|null */
    private $sumTax;

    /** @var float|null */
    private $sumGross;

    /** @var float|null */
    private $sumDiscounts;

    /** @var float|null */
    private $sumNetForeignCurrency;

    /** @var float|null */
    private $sumTaxForeignCurrency;

    /** @var float|null */
    private $sumGrossForeignCurrency;

    /** @var float|null */
    private $sumDiscountsForeignCurrency;

    /** @var float|null */
    private $sumNetAccounting;

    /** @var float|null */
    private $sumTaxAccounting;

    /** @var float|null */
    private $sumGrossAccounting;

    /** @var string|null */
    private $customerInternalNote;

    /** @var string|null */
    private $taxNumber;

    /** @var string|null */
    private $vatNumber;

    /** @var bool|null */
    private $showNet = 1;

    /** @var bool|null */
    private $enshrined = true;

    /** @var string|null */
    private $sendType;

    /** @var string|null */
    private $bookingCategory;

    /** @var string|null */
    private $deliveryDateUntil;

    /** @var string|null */
    private $datevConnectOnline;

    /** @var DefaultSevDeskEntity|null */
    private $refSrcInvoice;

    /** @var DefaultSevDeskEntity|null */
    private $accountingType;

    /**
     * @return object|null
     */
    public function getAdditionalInformation(): ?object
    {
        return $this->additionalInformation;
    }

    /**
     * @param object|null $additionalInformation
     */
    public function setAdditionalInformation(?object $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return string|null
     */
    public function getCreditNoteNumber(): ?string
    {
        return $this->creditNoteNumber;
    }

    /**
     * @param string|null $creditNoteNumber
     */
    public function setCreditNoteNumber(?string $creditNoteNumber): void
    {
        $this->creditNoteNumber = $creditNoteNumber;
    }

    /**
     * @return Contact|DefaultSevDeskEntity|null
     */
    public function getContact()
    {
        if ($this->contact !== null && !$this->contact instanceof Contact && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Contact::class,
                Contact::modelName, "", [], $this->contact->getId());
            $this->contact = !empty($obj[0]) ? $obj[0] : $this->contact;
        }
        return $this->contact;
    }

    /**
     * @param DefaultSevDeskEntity|null $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return DateTime|null
     */
    public function getCreditNoteDate(): ?DateTime
    {
        return $this->creditNoteDate;
    }

    /**
     * @param DateTime|null $creditNoteDate
     */
    public function setCreditNoteDate(?DateTime $creditNoteDate): void
    {
        $this->creditNoteDate = $creditNoteDate;
    }

    /**
     * @return string|null
     */
    public function getHeader(): ?string
    {
        return $this->header;
    }

    /**
     * @param string|null $header
     */
    public function setHeader(?string $header): void
    {
        $this->header = $header;
    }

    /**
     * @return string|null
     */
    public function getHeadText(): ?string
    {
        return $this->headText;
    }

    /**
     * @param string|null $headText
     */
    public function setHeadText(?string $headText): void
    {
        $this->headText = $headText;
    }

    /**
     * @return string|null
     */
    public function getFootText(): ?string
    {
        return $this->footText;
    }

    /**
     * @param string|null $footText
     */
    public function setFootText(?string $footText): void
    {
        $this->footText = $footText;
    }

    /**
     * @return string|null
     */
    public function getTimeToPay(): ?string
    {
        return $this->timeToPay;
    }

    /**
     * @param string|null $timeToPay
     */
    public function setTimeToPay(?string $timeToPay): void
    {
        $this->timeToPay = $timeToPay;
    }

    /**
     * @return string|null
     */
    public function getDiscountTime(): ?string
    {
        return $this->discountTime;
    }

    /**
     * @param string|null $discountTime
     */
    public function setDiscountTime(?string $discountTime): void
    {
        $this->discountTime = $discountTime;
    }

    /**
     * @return float|null
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float|null $discount
     */
    public function setDiscount(?float $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return string|null
     */
    public function getAddressName(): ?string
    {
        return $this->addressName;
    }

    /**
     * @param string|null $addressName
     */
    public function setAddressName(?string $addressName): void
    {
        $this->addressName = $addressName;
    }

    /**
     * @return string|null
     */
    public function getAddressStreet(): ?string
    {
        return $this->addressStreet;
    }

    /**
     * @param string|null $addressStreet
     */
    public function setAddressStreet(?string $addressStreet): void
    {
        $this->addressStreet = $addressStreet;
    }

    /**
     * @return string|null
     */
    public function getAddressZip(): ?string
    {
        return $this->addressZip;
    }

    /**
     * @param string|null $addressZip
     */
    public function setAddressZip(?string $addressZip): void
    {
        $this->addressZip = $addressZip;
    }

    /**
     * @return string|null
     */
    public function getAddressCity(): ?string
    {
        return $this->addressCity;
    }

    /**
     * @param string|null $addressCity
     */
    public function setAddressCity(?string $addressCity): void
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return Country|DefaultSevDeskEntity|null
     */
    public function getAddressCountry()
    {
        if ($this->addressCountry !== null && !$this->addressCountry instanceof Country && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Country::class,
                Country::modelName, "", [], $this->addressCountry->getId());
            $this->addressCountry = !empty($obj[0]) ? $obj[0] : $this->addressCountry;
        }
        return $this->addressCountry;
    }

    /**
     * @param DefaultSevDeskEntity|null $addressCountry
     */
    public function setAddressCountry($addressCountry): void
    {
        $this->addressCountry = $addressCountry;
    }

    /**
     * @return string|null
     */
    public function getPayDate(): ?string
    {
        return $this->payDate;
    }

    /**
     * @param string|null $payDate
     */
    public function setPayDate(?string $payDate): void
    {
        $this->payDate = $payDate;
    }

    /**
     * @return SevUser|DefaultSevDeskEntity|null
     */
    public function getCreateUser()
    {
        if ($this->createUser !== null && !$this->createUser instanceof SevUser && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(SevUser::class,
                SevUser::modelName, "", [], $this->createUser->getId());
            $this->createUser = !empty($obj[0]) ? $obj[0] : $this->createUser;
        }
        return $this->createUser;
    }

    /**
     * @param DefaultSevDeskEntity|null $createUser
     */
    public function setCreateUser($createUser): void
    {
        $this->createUser = $createUser;
    }

    /**
     * @return DateTime|null
     */
    public function getDeliveryDate(): ?DateTime
    {
        return $this->deliveryDate;
    }

    /**
     * @param DateTime|null $deliveryDate
     */
    public function setDeliveryDate(?DateTime $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param int|null $status
     */
    public function setStatus(?int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getSmallSettlement(): ?string
    {
        return $this->smallSettlement;
    }

    /**
     * @param string|null $smallSettlement
     */
    public function setSmallSettlement(?string $smallSettlement): void
    {
        $this->smallSettlement = $smallSettlement;
    }

    /**
     * @return SevUser|DefaultSevDeskEntity|null
     */
    public function getContactPerson()
    {
        if ($this->contactPerson !== null && !$this->contactPerson instanceof SevUser && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(SevUser::class,
                SevUser::modelName, "", [], $this->contactPerson->getId());
            $this->contactPerson = !empty($obj[0]) ? $obj[0] : $this->contactPerson;
        }
        return $this->contactPerson;
    }

    /**
     * @param DefaultSevDeskEntity|null $contactPerson
     */
    public function setContactPerson($contactPerson): void
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return float|null
     */
    public function getTaxRate(): ?float
    {
        return $this->taxRate;
    }

    /**
     * @param float|null $taxRate
     */
    public function setTaxRate(?float $taxRate): void
    {
        $this->taxRate = $taxRate;
    }

    /**
     * @return string|null
     */
    public function getTaxText(): ?string
    {
        return $this->taxText;
    }

    /**
     * @param string|null $taxText
     */
    public function setTaxText(?string $taxText): void
    {
        $this->taxText = $taxText;
    }

    /**
     * @return int|null
     */
    public function getDunningLevel(): ?int
    {
        return $this->dunningLevel;
    }

    /**
     * @param int|null $dunningLevel
     */
    public function setDunningLevel(?int $dunningLevel): void
    {
        $this->dunningLevel = $dunningLevel;
    }

    /**
     * @return string|null
     */
    public function getAddressParentName(): ?string
    {
        return $this->addressParentName;
    }

    /**
     * @param string|null $addressParentName
     */
    public function setAddressParentName(?string $addressParentName): void
    {
        $this->addressParentName = $addressParentName;
    }

    /**
     * @return string|null
     */
    public function getTaxType(): ?string
    {
        return $this->taxType;
    }

    /**
     * @param string|null $taxType
     */
    public function setTaxType(?string $taxType): void
    {
        $this->taxType = $taxType;
    }

    /**
     * @return DateTime|null
     */
    public function getSendDate(): ?DateTime
    {
        return $this->sendDate;
    }

    /**
     * @param DateTime|null $sendDate
     */
    public function setSendDate(?DateTime $sendDate): void
    {
        $this->sendDate = $sendDate;
    }

    /**
     * @return string|null
     */
    public function getCreditNoteType(): ?string
    {
        return $this->creditNoteType;
    }

    /**
     * @param string|null $creditNoteType
     */
    public function setCreditNoteType(?string $creditNoteType): void
    {
        $this->creditNoteType = $creditNoteType;
    }

    /**
     * @return string|null
     */
    public function getAccountIntervall(): ?string
    {
        return $this->accountIntervall;
    }

    /**
     * @param string|null $accountIntervall
     */
    public function setAccountIntervall(?string $accountIntervall): void
    {
        $this->accountIntervall = $accountIntervall;
    }

    /**
     * @return string|null
     */
    public function getAccountLastCreditNote(): ?string
    {
        return $this->accountLastCreditNote;
    }

    /**
     * @param string|null $accountLastCreditNote
     */
    public function setAccountLastCreditNote(?string $accountLastCreditNote): void
    {
        $this->accountLastCreditNote = $accountLastCreditNote;
    }

    /**
     * @return string|null
     */
    public function getAccountNextCreditNote(): ?string
    {
        return $this->accountNextCreditNote;
    }

    /**
     * @param string|null $accountNextCreditNote
     */
    public function setAccountNextCreditNote(?string $accountNextCreditNote): void
    {
        $this->accountNextCreditNote = $accountNextCreditNote;
    }

    /**
     * @return float|null
     */
    public function getReminderTotal(): ?float
    {
        return $this->reminderTotal;
    }

    /**
     * @param float|null $reminderTotal
     */
    public function setReminderTotal(?float $reminderTotal): void
    {
        $this->reminderTotal = $reminderTotal;
    }

    /**
     * @return float|null
     */
    public function getReminderDebit(): ?float
    {
        return $this->reminderDebit;
    }

    /**
     * @param float|null $reminderDebit
     */
    public function setReminderDebit(?float $reminderDebit): void
    {
        $this->reminderDebit = $reminderDebit;
    }

    /**
     * @return string|null
     */
    public function getReminderDeadline(): ?string
    {
        return $this->reminderDeadline;
    }

    /**
     * @param string|null $reminderDeadline
     */
    public function setReminderDeadline(?string $reminderDeadline): void
    {
        $this->reminderDeadline = $reminderDeadline;
    }

    /**
     * @return float|null
     */
    public function getReminderCharge(): ?float
    {
        return $this->reminderCharge;
    }

    /**
     * @param float|null $reminderCharge
     */
    public function setReminderCharge(?float $reminderCharge): void
    {
        $this->reminderCharge = $reminderCharge;
    }

    /**
     * @return string|null
     */
    public function getAddressParentName2(): ?string
    {
        return $this->addressParentName2;
    }

    /**
     * @param string|null $addressParentName2
     */
    public function setAddressParentName2(?string $addressParentName2): void
    {
        $this->addressParentName2 = $addressParentName2;
    }

    /**
     * @return string|null
     */
    public function getAddressName2(): ?string
    {
        return $this->addressName2;
    }

    /**
     * @param string|null $addressName2
     */
    public function setAddressName2(?string $addressName2): void
    {
        $this->addressName2 = $addressName2;
    }

    /**
     * @return string|null
     */
    public function getAddressGender(): ?string
    {
        return $this->addressGender;
    }

    /**
     * @param string|null $addressGender
     */
    public function setAddressGender(?string $addressGender): void
    {
        $this->addressGender = $addressGender;
    }

    /**
     * @return string|null
     */
    public function getAccountEndDate(): ?string
    {
        return $this->accountEndDate;
    }

    /**
     * @param string|null $accountEndDate
     */
    public function setAccountEndDate(?string $accountEndDate): void
    {
        $this->accountEndDate = $accountEndDate;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return float|null
     */
    public function getSumNet(): ?float
    {
        return $this->sumNet;
    }

    /**
     * @param float|null $sumNet
     */
    public function setSumNet(?float $sumNet): void
    {
        $this->sumNet = $sumNet;
    }

    /**
     * @return float|null
     */
    public function getSumTax(): ?float
    {
        return $this->sumTax;
    }

    /**
     * @param float|null $sumTax
     */
    public function setSumTax(?float $sumTax): void
    {
        $this->sumTax = $sumTax;
    }

    /**
     * @return float|null
     */
    public function getSumGross(): ?float
    {
        return $this->sumGross;
    }

    /**
     * @param float|null $sumGross
     */
    public function setSumGross(?float $sumGross): void
    {
        $this->sumGross = $sumGross;
    }

    /**
     * @return float|null
     */
    public function getSumDiscounts(): ?float
    {
        return $this->sumDiscounts;
    }

    /**
     * @param float|null $sumDiscounts
     */
    public function setSumDiscounts(?float $sumDiscounts): void
    {
        $this->sumDiscounts = $sumDiscounts;
    }

    /**
     * @return float|null
     */
    public function getSumNetForeignCurrency(): ?float
    {
        return $this->sumNetForeignCurrency;
    }

    /**
     * @param float|null $sumNetForeignCurrency
     */
    public function setSumNetForeignCurrency(?float $sumNetForeignCurrency): void
    {
        $this->sumNetForeignCurrency = $sumNetForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumTaxForeignCurrency(): ?float
    {
        return $this->sumTaxForeignCurrency;
    }

    /**
     * @param float|null $sumTaxForeignCurrency
     */
    public function setSumTaxForeignCurrency(?float $sumTaxForeignCurrency): void
    {
        $this->sumTaxForeignCurrency = $sumTaxForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumGrossForeignCurrency(): ?float
    {
        return $this->sumGrossForeignCurrency;
    }

    /**
     * @param float|null $sumGrossForeignCurrency
     */
    public function setSumGrossForeignCurrency(?float $sumGrossForeignCurrency): void
    {
        $this->sumGrossForeignCurrency = $sumGrossForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumDiscountsForeignCurrency(): ?float
    {
        return $this->sumDiscountsForeignCurrency;
    }

    /**
     * @param float|null $sumDiscountsForeignCurrency
     */
    public function setSumDiscountsForeignCurrency(?float $sumDiscountsForeignCurrency): void
    {
        $this->sumDiscountsForeignCurrency = $sumDiscountsForeignCurrency;
    }

    /**
     * @return float|null
     */
    public function getSumNetAccounting(): ?float
    {
        return $this->sumNetAccounting;
    }

    /**
     * @param float|null $sumNetAccounting
     */
    public function setSumNetAccounting(?float $sumNetAccounting): void
    {
        $this->sumNetAccounting = $sumNetAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumTaxAccounting(): ?float
    {
        return $this->sumTaxAccounting;
    }

    /**
     * @param float|null $sumTaxAccounting
     */
    public function setSumTaxAccounting(?float $sumTaxAccounting): void
    {
        $this->sumTaxAccounting = $sumTaxAccounting;
    }

    /**
     * @return float|null
     */
    public function getSumGrossAccounting(): ?float
    {
        return $this->sumGrossAccounting;
    }

    /**
     * @param float|null $sumGrossAccounting
     */
    public function setSumGrossAccounting(?float $sumGrossAccounting): void
    {
        $this->sumGrossAccounting = $sumGrossAccounting;
    }

    /**
     * @return string|null
     */
    public function getCustomerInternalNote(): ?string
    {
        return $this->customerInternalNote;
    }

    /**
     * @param string|null $customerInternalNote
     */
    public function setCustomerInternalNote(?string $customerInternalNote): void
    {
        $this->customerInternalNote = $customerInternalNote;
    }

    /**
     * @return string|null
     */
    public function getTaxNumber(): ?string
    {
        return $this->taxNumber;
    }

    /**
     * @param string|null $taxNumber
     */
    public function setTaxNumber(?string $taxNumber): void
    {
        $this->taxNumber = $taxNumber;
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return $this->vatNumber;
    }

    /**
     * @param string|null $vatNumber
     */
    public function setVatNumber(?string $vatNumber): void
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return bool|null
     */
    public function getShowNet(): ?bool
    {
        return $this->showNet;
    }

    /**
     * @param bool|null $showNet
     */
    public function setShowNet(?bool $showNet): void
    {
        $this->showNet = $showNet;
    }

    /**
     * @return bool|null
     */
    public function getEnshrined(): ?bool
    {
        return $this->enshrined;
    }

    /**
     * @param bool|null $enshrined
     */
    public function setEnshrined(?bool $enshrined): void
    {
        $this->enshrined = $enshrined;
    }

    /**
     * @return string|null
     */
    public function getSendType(): ?string
    {
        return $this->sendType;
    }

    /**
     * @param string|null $sendType
     */
    public function setSendType(?string $sendType): void
    {
        $this->sendType = $sendType;
    }

    /**
     * @return string|null
     */
    public function getBookingCategory(): ?string
    {
        return $this->bookingCategory;
    }

    /**
     * @param string|null $bookingCategory
     */
    public function setBookingCategory(?string $bookingCategory): void
    {
        $this->bookingCategory = $bookingCategory;
    }

    /**
     * @return string|null
     */
    public function getDeliveryDateUntil(): ?string
    {
        return $this->deliveryDateUntil;
    }

    /**
     * @param string|null $deliveryDateUntil
     */
    public function setDeliveryDateUntil(?string $deliveryDateUntil): void
    {
        $this->deliveryDateUntil = $deliveryDateUntil;
    }

    /**
     * @return string|null
     */
    public function getDatevConnectOnline(): ?string
    {
        return $this->datevConnectOnline;
    }

    /**
     * @param string|null $datevConnectOnline
     */
    public function setDatevConnectOnline(?string $datevConnectOnline): void
    {
        $this->datevConnectOnline = $datevConnectOnline;
    }

    /**
     * @return Invoice|DefaultSevDeskEntity|null
     */
    public function getRefSrcInvoice()
    {
        if ($this->refSrcInvoice !== null && !$this->refSrcInvoice instanceof Invoice && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(Invoice::class,
                Invoice::modelName, "", [], $this->refSrcInvoice->getId());
            $this->refSrcInvoice = !empty($obj[0]) ? $obj[0] : $this->refSrcInvoice;
        }
        return $this->refSrcInvoice;
    }

    /**
     * @param DefaultSevDeskEntity|null $refSrcInvoice
     */
    public function setRefSrcInvoice($refSrcInvoice): void
    {
        $this->refSrcInvoice = $refSrcInvoice;
    }

    /**
     * @return AccountingType|DefaultSevDeskEntity|null
     */
    public function getAccountingType()
    {
        if ($this->accountingType !== null && !$this->accountingType instanceof AccountingType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingType::class,
                AccountingType::modelName, "", [], $this->accountingType->getId());
            $this->accountingType = !empty($obj[0]) ? $obj[0] : $this->accountingType;
        }
        return $this->accountingType;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingType
     */
    public function setAccountingType($accountingType): void
    {
        $this->accountingType = $accountingType;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}