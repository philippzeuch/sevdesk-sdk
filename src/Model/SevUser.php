<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class SevUser
 * @package Zeuch\sevDesk\Model
 */
class SevUser extends SevDeskEntity
{
    const modelName = "SevUser";

    protected $objectName = self::modelName;

    /** @var object|null */
    public $additionalInformation;

    /** @var DateTime|null */
    public $create;

    /** @var DateTime|null */
    public $update;

    /** @var string|null */
    public $fullname;

    /** @var string|null */
    public $firstName;

    /** @var string|null */
    public $lastName;

    /** @var string|null */
    public $username;

    /** @var string|null */
    public $status;

    /** @var string|null */
    public $email;

    /** @var string|null */
    public $gender;

    /** @var string|null */
    public $role;

    /** @var string|null */
    public $memberCode;

    /** @var DateTime|null */
    public $lastLogin;

    /** @var string|null */
    public $lastLoginIp;

    /** @var string|null */
    public $welcomeScreenSeen;

    /** @var string|null */
    public $smtpName;

    /** @var string|null */
    public $smtpMail;

    /** @var string|null */
    public $smtpUser;

    /** @var string|null */
    public $smtpPort;

    /** @var string|null */
    public $smtpSsl;

    /** @var string|null */
    public $smtpHost;

    /** @var string|null */
    public $languageCode;

    /** @var string|null */
    public $twoFactorAuth;

    /** @var string|null */
    public $forcePasswordChange;

    /** @var string|null */
    public $clientOwner;

    /** @var string|null */
    public $defaultReceiveMailCopy;

    /** @var string|null */
    public $hideMapsDirections;

    /** @var DateTime|null */
    public $startDate;

    /** @var DateTime|null */
    public $endDate;

    /** @var DateTime|null */
    public $lastPasswordChange;

    /**
     * @return object|null
     */
    public function getAdditionalInformation(): ?object
    {
        return $this->additionalInformation;
    }

    /**
     * @param object|null $additionalInformation
     */
    public function setAdditionalInformation(?object $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getFullname(): ?string
    {
        return $this->fullname;
    }

    /**
     * @param string|null $fullname
     */
    public function setFullname(?string $fullname): void
    {
        $this->fullname = $fullname;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     */
    public function setUsername(?string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     */
    public function setGender(?string $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string|null $role
     */
    public function setRole(?string $role): void
    {
        $this->role = $role;
    }

    /**
     * @return string|null
     */
    public function getMemberCode(): ?string
    {
        return $this->memberCode;
    }

    /**
     * @param string|null $memberCode
     */
    public function setMemberCode(?string $memberCode): void
    {
        $this->memberCode = $memberCode;
    }

    /**
     * @return DateTime|null
     */
    public function getLastLogin(): ?DateTime
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTime|null $lastLogin
     */
    public function setLastLogin(?DateTime $lastLogin): void
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return string|null
     */
    public function getLastLoginIp(): ?string
    {
        return $this->lastLoginIp;
    }

    /**
     * @param string|null $lastLoginIp
     */
    public function setLastLoginIp(?string $lastLoginIp): void
    {
        $this->lastLoginIp = $lastLoginIp;
    }

    /**
     * @return string|null
     */
    public function getWelcomeScreenSeen(): ?string
    {
        return $this->welcomeScreenSeen;
    }

    /**
     * @param string|null $welcomeScreenSeen
     */
    public function setWelcomeScreenSeen(?string $welcomeScreenSeen): void
    {
        $this->welcomeScreenSeen = $welcomeScreenSeen;
    }

    /**
     * @return string|null
     */
    public function getSmtpName(): ?string
    {
        return $this->smtpName;
    }

    /**
     * @param string|null $smtpName
     */
    public function setSmtpName(?string $smtpName): void
    {
        $this->smtpName = $smtpName;
    }

    /**
     * @return string|null
     */
    public function getSmtpMail(): ?string
    {
        return $this->smtpMail;
    }

    /**
     * @param string|null $smtpMail
     */
    public function setSmtpMail(?string $smtpMail): void
    {
        $this->smtpMail = $smtpMail;
    }

    /**
     * @return string|null
     */
    public function getSmtpUser(): ?string
    {
        return $this->smtpUser;
    }

    /**
     * @param string|null $smtpUser
     */
    public function setSmtpUser(?string $smtpUser): void
    {
        $this->smtpUser = $smtpUser;
    }

    /**
     * @return string|null
     */
    public function getSmtpPort(): ?string
    {
        return $this->smtpPort;
    }

    /**
     * @param string|null $smtpPort
     */
    public function setSmtpPort(?string $smtpPort): void
    {
        $this->smtpPort = $smtpPort;
    }

    /**
     * @return string|null
     */
    public function getSmtpSsl(): ?string
    {
        return $this->smtpSsl;
    }

    /**
     * @param string|null $smtpSsl
     */
    public function setSmtpSsl(?string $smtpSsl): void
    {
        $this->smtpSsl = $smtpSsl;
    }

    /**
     * @return string|null
     */
    public function getSmtpHost(): ?string
    {
        return $this->smtpHost;
    }

    /**
     * @param string|null $smtpHost
     */
    public function setSmtpHost(?string $smtpHost): void
    {
        $this->smtpHost = $smtpHost;
    }

    /**
     * @return string|null
     */
    public function getLanguageCode(): ?string
    {
        return $this->languageCode;
    }

    /**
     * @param string|null $languageCode
     */
    public function setLanguageCode(?string $languageCode): void
    {
        $this->languageCode = $languageCode;
    }

    /**
     * @return string|null
     */
    public function getTwoFactorAuth(): ?string
    {
        return $this->twoFactorAuth;
    }

    /**
     * @param string|null $twoFactorAuth
     */
    public function setTwoFactorAuth(?string $twoFactorAuth): void
    {
        $this->twoFactorAuth = $twoFactorAuth;
    }

    /**
     * @return string|null
     */
    public function getForcePasswordChange(): ?string
    {
        return $this->forcePasswordChange;
    }

    /**
     * @param string|null $forcePasswordChange
     */
    public function setForcePasswordChange(?string $forcePasswordChange): void
    {
        $this->forcePasswordChange = $forcePasswordChange;
    }

    /**
     * @return string|null
     */
    public function getClientOwner(): ?string
    {
        return $this->clientOwner;
    }

    /**
     * @param string|null $clientOwner
     */
    public function setClientOwner(?string $clientOwner): void
    {
        $this->clientOwner = $clientOwner;
    }

    /**
     * @return string|null
     */
    public function getDefaultReceiveMailCopy(): ?string
    {
        return $this->defaultReceiveMailCopy;
    }

    /**
     * @param string|null $defaultReceiveMailCopy
     */
    public function setDefaultReceiveMailCopy(?string $defaultReceiveMailCopy): void
    {
        $this->defaultReceiveMailCopy = $defaultReceiveMailCopy;
    }

    /**
     * @return string|null
     */
    public function getHideMapsDirections(): ?string
    {
        return $this->hideMapsDirections;
    }

    /**
     * @param string|null $hideMapsDirections
     */
    public function setHideMapsDirections(?string $hideMapsDirections): void
    {
        $this->hideMapsDirections = $hideMapsDirections;
    }

    /**
     * @return DateTime|null
     */
    public function getStartDate(): ?DateTime
    {
        return $this->startDate;
    }

    /**
     * @param DateTime|null $startDate
     */
    public function setStartDate(?DateTime $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @return DateTime|null
     */
    public function getEndDate(): ?DateTime
    {
        return $this->endDate;
    }

    /**
     * @param DateTime|null $endDate
     */
    public function setEndDate(?DateTime $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @return DateTime|null
     */
    public function getLastPasswordChange(): ?DateTime
    {
        return $this->lastPasswordChange;
    }

    /**
     * @param DateTime|null $lastPasswordChange
     */
    public function setLastPasswordChange(?DateTime $lastPasswordChange): void
    {
        $this->lastPasswordChange = $lastPasswordChange;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}