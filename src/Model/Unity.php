<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class Unity
 * @package Zeuch\sevDesk\Model
 */
class Unity extends SevDeskEntity
{
    const modelName = "Unity";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    public $create;

    /** @var string|null */
    public $name;

    /** @var string|null */
    public $translationCode;

    /** @var DefaultSevDeskEntity|null */
    public $entryType;

    /** @var object|null */
    public $additionalInformation;

    /** @var string|null */
    public $unitySystem;

    /**
     * @return DateTime|null
     */
    public function getCreate()
    {
        return $this->create;
    }

    public function setCreate($create)
    {
        $this->create = $create;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getTranslationCode()
    {
        return $this->translationCode;
    }

    public function setTranslationCode($translationCode)
    {
        $this->translationCode = $translationCode;
    }

    /**
     * @return EntryType|null
     */
    public function getEntryType()
    {
        if ($this->entryType !== null && !$this->entryType instanceof EntryType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(EntryType::class,
                EntryType::modelName, "", [], $this->entryType->getId());
            $this->entryType = !empty($obj[0]) ? $obj[0] : $this->entryType;
        }
        return $this->entryType;
    }

    /**
     * @param DefaultSevDeskEntity|null $entryType
     */
    public function setEntryType($entryType)
    {
        $this->entryType = $entryType;
    }

    /**
     * @return object|null
     */
    public function getAdditionalInformation(): ?object
    {
        return $this->additionalInformation;
    }

    /**
     * @param object|null $additionalInformation
     */
    public function setAdditionalInformation(?object $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return string|null
     */
    public function getUnitySystem(): ?string
    {
        return $this->unitySystem;
    }

    /**
     * @param string|null $unitySystem
     */
    public function setUnitySystem(?string $unitySystem): void
    {
        $this->unitySystem = $unitySystem;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}