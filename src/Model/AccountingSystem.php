<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class AccountingSystem
 * @package Zeuch\sevDesk\Model
 */
class AccountingSystem extends SevDeskEntity
{
    const modelName = "AccountingSystem";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var DefaultSevDeskEntity|null */
    private $accountingChart;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return AccountingChart|DefaultSevDeskEntity|null
     */
    public function getAccountingChart()
    {
        if ($this->accountingChart !== null && !$this->accountingChart instanceof AccountingChart && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(AccountingChart::class,
                AccountingChart::modelName, "", [], $this->accountingChart->getId());
            $this->accountingChart = !empty($obj[0]) ? $obj[0] : $this->accountingChart;
        }
        return $this->accountingChart;
    }

    /**
     * @param DefaultSevDeskEntity|null $accountingChart
     */
    public function setAccountingChart($accountingChart): void
    {
        $this->accountingChart = $accountingChart;
    }

    /**
     * @inheritDoc
     */
    public function getParent() {return null;}

    /**
     * @inheritDoc
     */
    public function setParent($parent) {}

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}