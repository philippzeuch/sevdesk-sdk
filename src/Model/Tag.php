<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class Tag extends SevDeskEntity
{
    const modelName = "Tag";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $color;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}