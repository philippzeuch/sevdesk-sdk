<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class Category extends SevDeskEntity
{
    const modelName = "Category";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    public $create;

    /** @var DateTime|null */
    public $update;

    /** @var string|null */
    public $name;

    /** @var string|null */
    public $objectType;

    /** @var integer|null */
    public $priority;

    /** @var string|null */
    public $code;

    /** @var string|null */
    public $color;

    /** @var string|null */
    public $postingAccount;

    /** @var string|null */
    public $type;

    /** @var string|null */
    public $translationCode;

    /** @var DefaultSevDeskEntity|null */
    public $entryType;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getObjectType(): ?string
    {
        return $this->objectType;
    }

    /**
     * @param string|null $objectType
     */
    public function setObjectType(?string $objectType): void
    {
        $this->objectType = $objectType;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int|null $priority
     */
    public function setPriority(?int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string|null
     */
    public function getPostingAccount(): ?string
    {
        return $this->postingAccount;
    }

    /**
     * @param string|null $postingAccount
     */
    public function setPostingAccount(?string $postingAccount): void
    {
        $this->postingAccount = $postingAccount;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getTranslationCode(): ?string
    {
        return $this->translationCode;
    }

    /**
     * @param string|null $translationCode
     */
    public function setTranslationCode(?string $translationCode): void
    {
        $this->translationCode = $translationCode;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getEntryType(): ?DefaultSevDeskEntity
    {
        if ($this->entryType !== null && !$this->entryType instanceof EntryType && $this->repo !== null) {
            $obj = $this->repo->getFromSevDesk(EntryType::class,
                EntryType::modelName, "", [], $this->entryType->getId());
            $this->entryType = !empty($obj[0]) ? $obj[0] : $this->entryType;
        }
        return $this->entryType;
    }

    /**
     * @param DefaultSevDeskEntity|null $entryType
     */
    public function setEntryType(?DefaultSevDeskEntity $entryType): void
    {
        $this->entryType = $entryType;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}