<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

class TagRelation extends SevDeskEntity
{
    const modelName = "TagRelation";

    protected $objectName = self::modelName;

    /** @var DateTime|null */
    private $create;

    /** @var DefaultSevDeskEntity|null */
    private $tag;

    /** @var DefaultSevDeskEntity|null */
    private $object;

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return Tag|DefaultSevDeskEntity|null
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param DefaultSevDeskEntity|null $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return DefaultSevDeskEntity|null
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * @param DefaultSevDeskEntity|null $object
     */
    public function setObject($object): void
    {
        $this->object = $object;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}