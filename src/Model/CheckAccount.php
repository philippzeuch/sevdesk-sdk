<?php

namespace Zeuch\sevDesk\Model;

use DateTime;

/**
 * Class CheckAccount
 * @package Zeuch\sevDesk\Model
 */
class CheckAccount extends SevDeskEntity
{
    const modelName = "CheckAccount";

    protected $objectName = self::modelName;

    /** @var string|null */
    private $additionalInformation;

    /** @var DateTime|null */
    private $create;

    /** @var DateTime|null */
    private $update;

    /** @var string|null */
    private $name;

    /** @var string|null */
    private $type;

    /** @var string|null */
    private $importType;

    /** @var string|null */
    private $currency;

    /** @var string|null */
    private $checkAccId;

    /** @var string|null */
    private $defaultAccount;

    /** @var string|null */
    private $status;

    /** @var string|null */
    private $pin;

    /** @var string|null */
    private $translationCode;

    /** @var string|null */
    private $bankServer;

    /** @var float|null */
    private $balance;

    /** @var int|null */
    private $accountingNumber;

    /** @var string|null */
    private $iban;

    /** @var string|null */
    private $bic;

    /** @var string|null */
    private $baseAccount;

    /** @var string|null */
    private $priority;

    /** @var string|null */
    private $countryCode;

    /** @var string|null */
    private $autoMapTransaction;

    /** @var string|null */
    private $lastSync;

    /**
     * @return string|null
     */
    public function getAdditionalInformation(): ?string
    {
        return $this->additionalInformation;
    }

    /**
     * @param string|null $additionalInformation
     */
    public function setAdditionalInformation(?string $additionalInformation): void
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * @return DateTime|null
     */
    public function getCreate(): ?DateTime
    {
        return $this->create;
    }

    /**
     * @param DateTime|null $create
     */
    public function setCreate(?DateTime $create): void
    {
        $this->create = $create;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdate(): ?DateTime
    {
        return $this->update;
    }

    /**
     * @param DateTime|null $update
     */
    public function setUpdate(?DateTime $update): void
    {
        $this->update = $update;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return string|null
     */
    public function getImportType(): ?string
    {
        return $this->importType;
    }

    /**
     * @param string|null $importType
     */
    public function setImportType(?string $importType): void
    {
        $this->importType = $importType;
    }

    /**
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->currency;
    }

    /**
     * @param string|null $currency
     */
    public function setCurrency(?string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string|null
     */
    public function getCheckAccId(): ?string
    {
        return $this->checkAccId;
    }

    /**
     * @param string|null $checkAccId
     */
    public function setCheckAccId(?string $checkAccId): void
    {
        $this->checkAccId = $checkAccId;
    }

    /**
     * @return string|null
     */
    public function getDefaultAccount(): ?string
    {
        return $this->defaultAccount;
    }

    /**
     * @param string|null $defaultAccount
     */
    public function setDefaultAccount(?string $defaultAccount): void
    {
        $this->defaultAccount = $defaultAccount;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getPin(): ?string
    {
        return $this->pin;
    }

    /**
     * @param string|null $pin
     */
    public function setPin(?string $pin): void
    {
        $this->pin = $pin;
    }

    /**
     * @return string|null
     */
    public function getTranslationCode(): ?string
    {
        return $this->translationCode;
    }

    /**
     * @param string|null $translationCode
     */
    public function setTranslationCode(?string $translationCode): void
    {
        $this->translationCode = $translationCode;
    }

    /**
     * @return string|null
     */
    public function getBankServer(): ?string
    {
        return $this->bankServer;
    }

    /**
     * @param string|null $bankServer
     */
    public function setBankServer(?string $bankServer): void
    {
        $this->bankServer = $bankServer;
    }

    /**
     * @return float|null
     */
    public function getBalance(): ?float
    {
        return $this->balance;
    }

    /**
     * @param float|null $balance
     */
    public function setBalance(?float $balance): void
    {
        $this->balance = $balance;
    }

    /**
     * @return int|null
     */
    public function getAccountingNumber(): ?int
    {
        return $this->accountingNumber;
    }

    /**
     * @param int|null $accountingNumber
     */
    public function setAccountingNumber(?int $accountingNumber): void
    {
        $this->accountingNumber = $accountingNumber;
    }

    /**
     * @return string|null
     */
    public function getIban(): ?string
    {
        return $this->iban;
    }

    /**
     * @param string|null $iban
     */
    public function setIban(?string $iban): void
    {
        $this->iban = $iban;
    }

    /**
     * @return string|null
     */
    public function getBic(): ?string
    {
        return $this->bic;
    }

    /**
     * @param string|null $bic
     */
    public function setBic(?string $bic): void
    {
        $this->bic = $bic;
    }

    /**
     * @return string|null
     */
    public function getBaseAccount(): ?string
    {
        return $this->baseAccount;
    }

    /**
     * @param string|null $baseAccount
     */
    public function setBaseAccount(?string $baseAccount): void
    {
        $this->baseAccount = $baseAccount;
    }

    /**
     * @return string|null
     */
    public function getPriority(): ?string
    {
        return $this->priority;
    }

    /**
     * @param string|null $priority
     */
    public function setPriority(?string $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return string|null
     */
    public function getCountryCode(): ?string
    {
        return $this->countryCode;
    }

    /**
     * @param string|null $countryCode
     */
    public function setCountryCode(?string $countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return string|null
     */
    public function getAutoMapTransaction(): ?string
    {
        return $this->autoMapTransaction;
    }

    /**
     * @param string|null $autoMapTransaction
     */
    public function setAutoMapTransaction(?string $autoMapTransaction): void
    {
        $this->autoMapTransaction = $autoMapTransaction;
    }

    /**
     * @return string|null
     */
    public function getLastSync(): ?string
    {
        return $this->lastSync;
    }

    /**
     * @param string|null $lastSync
     */
    public function setLastSync(?string $lastSync): void
    {
        $this->lastSync = $lastSync;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}