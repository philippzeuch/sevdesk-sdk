<?php

namespace Zeuch\sevDesk;

use DI\Definition\Helper\FactoryDefinitionHelper;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Zeuch\sevDesk\Repo\AccountingChartRepo;
use Zeuch\sevDesk\Repo\AccountingSystemRepo;
use Zeuch\sevDesk\Repo\AccountingTypeRepo;
use Zeuch\sevDesk\Repo\CategoryRepo;
use Zeuch\sevDesk\Repo\CheckAccountRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionLogRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;
use Zeuch\sevDesk\Repo\CommunicationWayKeyRepo;
use Zeuch\sevDesk\Repo\CommunicationWayRepo;
use Zeuch\sevDesk\Repo\ContactAddressRepo;
use Zeuch\sevDesk\Repo\ContactRepo;
use Zeuch\sevDesk\Repo\CreditNotePosRepo;
use Zeuch\sevDesk\Repo\CreditNoteRepo;
use Zeuch\sevDesk\Repo\DiscountsRepo;
use Zeuch\sevDesk\Repo\InvoicePosRepo;
use Zeuch\sevDesk\Repo\InvoiceRepo;
use Zeuch\sevDesk\Repo\OrderPosRepo;
use Zeuch\sevDesk\Repo\OrderRepo;
use Zeuch\sevDesk\Repo\PartRepo;
use Zeuch\sevDesk\Repo\PaymentMethodRepo;
use Zeuch\sevDesk\Repo\SevClientRepo;
use Zeuch\sevDesk\Repo\SevSequenceRepo;
use Zeuch\sevDesk\Repo\SevUserRepo;
use Zeuch\sevDesk\Repo\StaticCountryRepo;
use Zeuch\sevDesk\Repo\TagRelationRepo;
use Zeuch\sevDesk\Repo\TagRepo;
use Zeuch\sevDesk\Repo\UnityRepo;
use Zeuch\sevDesk\Repo\VoucherPosRepo;
use Zeuch\sevDesk\Repo\VoucherRepo;
use Zeuch\sevDesk\Service\CheckAccountTransactionService;
use Zeuch\sevDesk\Service\CreditNoteService;
use Zeuch\sevDesk\Service\DiscountsService;
use Zeuch\sevDesk\Service\InvoiceService;
use Zeuch\sevDesk\Service\VoucherService;
use Zeuch\sevDesk\Utils\Cache\CacheManager;
use function DI\create;
use function DI\get;

class DefaultSevDeskApplicationContext {
    public static function get() : array {
        return [
            // Services
            CheckAccountTransactionService::class => create(CheckAccountTransactionService::class)
                ->constructor(get(ContainerInterface::class)),
            CreditNoteService::class => create(CreditNoteService::class)->constructor(get(ContainerInterface::class)),
            DiscountsService::class => create(DiscountsService::class)->constructor(get(ContainerInterface::class)),
            InvoiceService::class => create(InvoiceService::class)->constructor(get(ContainerInterface::class)),
            VoucherService::class => create(VoucherService::class)->constructor(get(ContainerInterface::class)),
            CacheManager::class => create(CacheManager::class),

            // Repos
            AccountingChartRepo::class => create(AccountingChartRepo::class)->constructor(get(ContainerInterface::class)),
            AccountingSystemRepo::class => create(AccountingSystemRepo::class)->constructor(get(ContainerInterface::class)),
            AccountingTypeRepo::class => create(AccountingTypeRepo::class)->constructor(get(ContainerInterface::class)),
            CategoryRepo::class => create(CategoryRepo::class)->constructor(get(ContainerInterface::class)),
            CheckAccountRepo::class => create(CheckAccountRepo::class)->constructor(get(ContainerInterface::class)),
            CheckAccountTransactionLogRepo::class => create(CheckAccountTransactionLogRepo::class)->constructor(get(ContainerInterface::class)),
            CheckAccountTransactionRepo::class => create(CheckAccountTransactionRepo::class)->constructor(get(ContainerInterface::class)),
            CommunicationWayKeyRepo::class => create(CommunicationWayKeyRepo::class)->constructor(get(ContainerInterface::class)),
            CommunicationWayRepo::class => create(CommunicationWayRepo::class)->constructor(get(ContainerInterface::class)),
            ContactAddressRepo::class => create(ContactAddressRepo::class)->constructor(get(ContainerInterface::class)),
            ContactRepo::class => create(ContactRepo::class)->constructor(get(ContainerInterface::class)),
            CreditNoteRepo::class => create(CreditNoteRepo::class)->constructor(get(ContainerInterface::class)),
            CreditNotePosRepo::class => create(CreditNotePosRepo::class)->constructor(get(ContainerInterface::class)),
            DiscountsRepo::class => create(DiscountsRepo::class)->constructor(get(ContainerInterface::class)),
            InvoicePosRepo::class => create(InvoicePosRepo::class)->constructor(get(ContainerInterface::class)),
            InvoiceRepo::class => create(InvoiceRepo::class)->constructor(get(ContainerInterface::class)),
            OrderPosRepo::class => create(OrderPosRepo::class)->constructor(get(ContainerInterface::class)),
            OrderRepo::class => create(OrderRepo::class)->constructor(get(ContainerInterface::class)),
            PartRepo::class => create(PartRepo::class)->constructor(get(ContainerInterface::class)),
            PaymentMethodRepo::class => create(PaymentMethodRepo::class)->constructor(get(ContainerInterface::class)),
            SevSequenceRepo::class => create(SevSequenceRepo::class)->constructor(get(ContainerInterface::class)),
            SevUserRepo::class => create(SevUserRepo::class)->constructor(get(ContainerInterface::class)),
            StaticCountryRepo::class => create(StaticCountryRepo::class)->constructor(get(ContainerInterface::class)),
            TagRelationRepo::class => create(TagRelationRepo::class)->constructor(get(ContainerInterface::class)),
            TagRepo::class => create(TagRepo::class)->constructor(get(ContainerInterface::class)),
            UnityRepo::class => create(UnityRepo::class)->constructor(get(ContainerInterface::class)),
            VoucherPosRepo::class => create(VoucherPosRepo::class)->constructor(get(ContainerInterface::class)),
            VoucherRepo::class => create(VoucherRepo::class)->constructor(get(ContainerInterface::class)),
            SevClientRepo::class => create(SevClientRepo::class)->constructor(get(ContainerInterface::class)),

            // Logging
            LoggerInterface::class => new FactoryDefinitionHelper(function (ContainerInterface $c) {
                $logLevel = trim($c->get('LOG_LEVEL'));
                $logger = new Logger("default");
                $defaultLogFormatter = new LineFormatter("[%datetime%] %level_name%: %message%\n", "Y-m-d h:m:s");
                $defaultLogFormatter->ignoreEmptyContextAndExtra();
                $logger->pushProcessor(function ($record) {
                    $context = $record['context'];
                    $record['message'] = preg_replace_callback('/{(.+?)}/ix',function($match) use ($context) {
                        return !empty($context[$match[1]]) ? "'" . $context[$match[1]] . "'" : $match[0];
                    }, $record['message']);

                    return $record;
                });

                if ($c->has("LOG_PATH")) {
                    $logPath = rtrim(trim($c->get("LOG_PATH")), "/\\");
                    $fileLogHandler = new RotatingFileHandler($logPath . "/app.log", 30, $logLevel);
                    $fileLogHandler->setFormatter($defaultLogFormatter);
                    $fileLogHandler->setFilenameFormat("{date}-{filename}", "Y-m-d");
                    $logger->pushHandler($fileLogHandler);
                }
                $consoleLogHandler = new StreamHandler("php://stdout", $logLevel);
                $consoleLogHandler->setFormatter($defaultLogFormatter);
                $logger->pushHandler($consoleLogHandler);

                return $logger;
            })
        ];
    }
}