<?php

namespace Zeuch\sevDesk\Repo;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\Order;
use Zeuch\sevDesk\Model\OrderPos;
use DateTime;

class OrderPosRepo extends SevDeskRepo
{
    /** @var UnityRepo */
    private $unityRepo;

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
        $this->unityRepo = $c->get(UnityRepo::class);
    }

    public function createNewInstance()
    {
        $orderPos = new OrderPos();
        $orderPos->setCreate(new DateTime());
        $orderPos->setTaxRate(((float) $this->getContainer()->get("MWST")) * 100);
        $orderPos->setUnity($this->unityRepo->getPieceUnity());

        $orderPos->setRepo($this);

        return $orderPos;
    }

    /**
     * @param Order $order
     * @return OrderPos[]
     */
    public function getByOrder(Order $order)
    {
        return parent::getFromSevDesk(OrderPos::class, Order::modelName, "getPositions", [], $order->getId());
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, OrderPos::class, OrderPos::modelName, $additionalParams);
    }

    /** @return OrderPos[] */
    public function get($id = null)
    {
        return parent::getFromSevDesk(OrderPos::class, OrderPos::modelName, "", [], $id);
    }
}