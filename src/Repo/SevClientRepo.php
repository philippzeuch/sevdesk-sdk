<?php

namespace Zeuch\sevDesk\Repo;

use Exception;
use Zeuch\sevDesk\Model\SevClient;
use Zeuch\sevDesk\Model\SevDeskEntity;

class SevClientRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        throw new Exception("Operation not supported!");
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(SevClient::class, SevClient::modelName, "", [], $id)[0];
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        throw new Exception("Operation not supported!");
    }

    public function delete(SevDeskEntity $entity)
    {
        throw new Exception("Operation not supported!");
    }

    public function saveOrUpdate($entity)
    {
        throw new Exception("Operation not supported!");
    }
}