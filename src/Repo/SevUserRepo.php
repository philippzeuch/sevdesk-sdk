<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\SevUser;

class SevUserRepo extends SevDeskRepo
{
    public function createNewInstance()
    {
        return new SevUser();
    }

    /**
     * Finde eine SevDesk Entität anhand irgendeiner Zeichenfolge
     * @param $searchTerm string
     * @param $additionalParams
     * @return array|mixed
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, SevUser::class, SevUser::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(SevUser::class, SevUser::modelName, "", [], $id);
    }
}