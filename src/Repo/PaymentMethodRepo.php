<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\PaymentMethod;
use DateTime;

class PaymentMethodRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $paymentMethod = new PaymentMethod();
        $paymentMethod->setCreate(new DateTime());

        return $paymentMethod;
    }

    public function getBar()
    {
        /** @var PaymentMethod[] $entities */
        $entities = $this->getFromSevDesk(PaymentMethod::class, PaymentMethod::modelName, "", [
            'name' => 'Bar'
        ]);
        $entities = array_filter($entities, function (?PaymentMethod $entity) {return $entity->getName() == "Bar";});
        if (!empty($entities)) {
            $entities = array_values($entities);
            return $entities[0];
        }
        return null;
    }

    public function getKarte()
    {
        /** @var PaymentMethod[] $entities */
        $entities = $this->getFromSevDesk(PaymentMethod::class, PaymentMethod::modelName, "", [
            'name' => 'Karte'
        ]);
        $entities = array_filter($entities, function (?PaymentMethod $entity) {return $entity->getName() == "Karte";});
        if (!empty($entities)) {
            $entities = array_values($entities);
            return $entities[0];
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, PaymentMethod::class, PaymentMethod::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(PaymentMethod::class, PaymentMethod::modelName, "", [], $id);
    }
}