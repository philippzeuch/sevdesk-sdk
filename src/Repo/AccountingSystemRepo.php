<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\AccountingSystem;
use DateTime;

class AccountingSystemRepo extends SevDeskRepo
{
    public function createNewInstance()
    {
        $system = new AccountingSystem();
        $system->setCreate(new DateTime());

        $system->setRepo($this);

        return $system;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        parent::sevDeskSearch($searchTerm, AccountingSystem::class, AccountingSystem::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(AccountingSystem::class, AccountingSystem::modelName, "", [], $id);
    }
}