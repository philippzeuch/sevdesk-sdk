<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Model\SevClient;
use Zeuch\sevDesk\Utils\BookingType;
use Zeuch\sevDesk\Utils\InvoiceStatus;
use Zeuch\sevDesk\Utils\InvoiceType;
use Zeuch\sevDesk\Model\Order;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use Psr\Http\Message\ResponseInterface;
use Zeuch\sevDesk\Utils\TaxType;

class InvoiceRepo extends SevDeskRepo
{
    /**
     * @return Invoice
     */
    public function createNewInstance()
    {
        $invoice = new Invoice();
        $invoice->setUpdateType(SevDeskUpdateType::NEW);
        $invoice->setCreate(new DateTime());
        $invoice->setInvoiceDate(new DateTime());
        $invoice->setDeliveryDate(new DateTime());
        $invoice->setStatus(InvoiceStatus::$CREATED);
        $invoice->setInvoiceType(InvoiceType::$RE);
        $invoice->setContactPerson($this->getDefaultSevUser());
        $invoice->setCreateUser($this->getDefaultSevUser());
        $invoice->setTaxType(TaxType::DEFAULT);
        $invoice->setTaxRate(((float)$this->getContainer()->get("MWST")) * 100);

        $invoice->setHeadText("<p>Sehr geehrte Damen und Herren,</p><p>vielen Dank für Ihren Auftrag und das damit verbundene Vertrauen!<br>Hiermit stelle ich Ihnen die folgenden Leistungen in Rechnung:</p>");
        $invoice->setFootText("<p>Der Rechnungsbetrag ist sofort fällig.</p><p>Mit freundlichen Grüßen<br>[%KONTAKTPERSON%]</p>");
        $invoice->setInvoiceNumber($this->getNextInvoiceNumber($invoice->getInvoiceType()));
        /** @var SevClient $sevClient */
        $sevClient = $this->getContainer()->get(SevClientRepo::class)->get();
        $invoice->setShowNet($sevClient->getShowNet());
        $invoice->setAddressCountry($this->getContainer()->get(StaticCountryRepo::class)->getGermany());

        $invoice->setRepo($this);

        return $invoice;
    }

    /**
     * @param $rechnungsNummer
     * @return Invoice|null
     */
    public function findByRechnungsNummer($rechnungsNummer): ?Invoice
    {
        return array_values(array_filter(parent::getFromSevDesk(Invoice::class, Invoice::modelName, "", [
            'invoiceNumber' => $rechnungsNummer
        ]), /** @param Invoice $entity */ function ($entity) use ($rechnungsNummer) {
            return $entity->getInvoiceNumber() === $rechnungsNummer;
        }))[0];
    }

    public function findByOrder(Order $order): ?Invoice
    {
        $response = parent::request("GET", Order::modelName, "getRelatedObjects",
            ['embed' => Invoice::modelName], null, $order->getId());

        $stdClass = json_decode($response->getBody()->getContents());
        $objects = (array)$stdClass->objects;

        foreach ($objects as $object) {
            if ($object->objectName === Invoice::modelName) {
                $jsonDecoder = $this->getJsonMapper();
                $returnEntity = $jsonDecoder->map($object, new Invoice());
                $returnEntity->setRepo($this);

                return $returnEntity;
            }
        }

        return null;
    }

    public function bookAmount(Invoice $invoice, float $amount, DateTime $date, CheckAccount $account,
                               ?CheckAccountTransaction $zielTransaktion, string $type = BookingType::N)
    {
        $body = [
            'type'    => $type,
            'ammount' => $amount,
            'date' => $date->format('Y-m-d\TH:m:s.u\Z'),
            'checkAccount' => [
                'objectName' => CheckAccount::modelName,
                'id'         => $account->getId()
            ],
            'createFeed' => true
        ];
        if ($zielTransaktion !== null) {
            $body['checkAccountTransaction'] = [
                'id'         => $zielTransaktion->getId(),
                'objectName' => CheckAccountTransaction::modelName
            ];
        }

        $response = parent::request(SevDeskUpdateType::UPDATE, Invoice::modelName, "bookAmmount", [],
            http_build_query($body), $invoice->getId());

        if ($response !== null) {
            return $response->getStatusCode();
        }
        return 500;
    }

    public function markAsSent(Invoice $invoice): Invoice
    {
        $invoice = $this->persist($invoice, Invoice::modelName,
            "markAsSent", [], $invoice->getId(), SevDeskUpdateType::UPDATE);
        $invoice->setSendType("VPDF");
        $invoice->setUpdateType(SevDeskUpdateType::UPDATE);

        return $this->saveOrUpdate($invoice);
    }

    /**
     * @param string $invoiceType
     * @return mixed|ResponseInterface|null
     */
    private function getNextInvoiceNumber(string $invoiceType)
    {
        return $this->getFromSevDesk(null, Invoice::modelName, "Factory/getNextInvoiceNumber", [
            'invoiceType' => $invoiceType,
            'useNextNumber' => true
        ]);
    }

    /**
     * @param Invoice $invoice
     * @return float
     */
    public function getOffeneMahngebuehren(Invoice $invoice)
    {
        $amount = 0.0;
        if ($invoice !== null) {
            foreach ($this->getMahnungen($invoice) as $mahnung) {
                $amount += $mahnung->getReminderDebit();
            }
        }
        return $amount;
    }

    public function getMahngebuehrenGesamt(Invoice $invoice)
    {
        return $this->getFromSevDesk(null, Invoice::modelName, "Factory/getTotalInvoiceReminderAmount", [
            'invoice' => [
                'id' => $invoice->getId(),
                'objectName' => $invoice->getObjectName()
            ]
        ]);
    }

    /**
     * @param Invoice $invoice Die Rechnung, zu welcher alle Mahnungen gesucht werden sollen
     * @return Invoice[] Mahnungen
     */
    public function getMahnungen(Invoice $invoice)
    {
        return $this->getFromSevDesk(Invoice::class, Invoice::modelName, "getDunnings", [], $invoice->getId());
    }

    public function createInvoiceFromOrder(Order $order): Invoice
    {
        $response = parent::request(SevDeskUpdateType::NEW, Invoice::modelName, "Factory/createInvoiceFromOrder", [], http_build_query([
            'order' => [
                'id' => $order->getId(),
                'objectName' => $order::modelName,
                'optArray' => null,
                'optAddress' => null,
                'type' => 'percentage',
                'amount' => 100
            ]
        ]));
        return parent::transformResultToEntity($response, Invoice::class);
    }


    public function changeStatus($invoiceId)
    {
        return self::transformResultToEntity(self::request(SevDeskUpdateType::UPDATE, Invoice::modelName, "changeStatus",
            [], http_build_query([
                'value' => 200
            ]), $invoiceId), Invoice::class);
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Invoice::class, Invoice::modelName, $additionalParams);
    }

    /**
     * @param $id
     * @return Invoice
     */
    public function get($id = null)
    {
        return parent::getFromSevDesk(Invoice::class, Invoice::modelName, "", [], $id);
    }
}