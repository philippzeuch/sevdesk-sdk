<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\VoucherPos;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class VoucherPosRepo extends SevDeskRepo
{
    public function createNewInstance()
    {
        $voucherPos = new VoucherPos();
        $voucherPos->setCreate(new DateTime());
        $voucherPos->setUpdate(new DateTime());
        $voucherPos->setUpdateType(SevDeskUpdateType::NEW);
        $voucherPos->setTaxRate(((float) $this->getContainer()->get("MWST")) * 100);

        $voucherPos->setRepo($this);

        return $voucherPos;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, VoucherPos::class, VoucherPos::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(VoucherPos::class, VoucherPos::modelName, "", [], $id);
    }
}