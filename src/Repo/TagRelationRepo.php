<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\TagRelation;

class TagRelationRepo extends SevDeskRepo
{
    public function createNewInstance()
    {
        return new TagRelation();
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(TagRelation::class, TagRelation::modelName, "", [], $id)[0];
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, TagRelation::class, TagRelation::modelName, $additionalParams);
    }
}