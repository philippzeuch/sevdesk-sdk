<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\AccountingChart;
use DateTime;

class AccountingChartRepo extends SevDeskRepo
{
    public function createNewInstance()
    {
        $accountingChart = new AccountingChart();
        $accountingChart->setCreate(new DateTime());
        $accountingChart->setUpdate(new DateTime());

        $accountingChart->setRepo($this);

        return $accountingChart;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, AccountingChart::class, AccountingChart::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(AccountingChart::class, AccountingChart::modelName, "", [], $id);
    }
}