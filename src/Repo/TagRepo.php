<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Tag;

class TagRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        return new Tag();
    }

    public function getByName($name)
    {
        return array_values(array_filter(parent::getFromSevDesk(Tag::class, Tag::modelName, "", [
            'name' => $name
        ]), /** @param Tag $tag */ function ($tag) use ($name) {
            return $tag->getName() === $name;
        }));
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(Tag::class, Tag::modelName, "", [], $id)[0];
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Tag::class, Tag::modelName, $additionalParams);
    }
}