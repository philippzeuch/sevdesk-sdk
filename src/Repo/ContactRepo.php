<?php

namespace Zeuch\sevDesk\Repo;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\CommunicationWay;
use Zeuch\sevDesk\Model\Contact;
use Zeuch\sevDesk\Utils\ContactStatus;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;
use Zeuch\sevDesk\Utils\CategoryTranslationCode;
use Zeuch\sevDesk\Utils\TaxType;

class ContactRepo extends SevDeskRepo {

    /** @var CategoryRepo */
    private $categoryRepo;

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
        $this->categoryRepo = $c->get(CategoryRepo::class);
    }

    /**
     * @return Contact
     */
    public function createNewInstance()
    {
        $contact = new Contact();
        $contact->setCreate(new DateTime());
        $contact->setStatus(ContactStatus::ACTIVE);
        $contact->setTaxType(TaxType::DEFAULT);
        $contact->setCategory($this->categoryRepo->getByTranslationCode(CategoryTranslationCode::CATEGORY_CUSTOMER));
        $contact->setRepo($this);

        return $contact;
    }

    /**
     * Hole dir einen Kundenkontakt über die API von sevDesk
     * @param array $queryParams
     * @param int $id
     * @return Contact[]
     */
    public function getEntities(?int $id = -1, array $queryParams = []) : array
    {
        return parent::getFromSevDesk(Contact::class, Contact::modelName, "", $queryParams, $id);
    }

    /**
     * @param Contact|mixed $contact
     * @return Contact|\Zeuch\sevDesk\Model\SevDeskEntity|null
     * @throws GuzzleException
     */
    public function saveOrUpdate($contact)
    {
        if ($contact !== null) {
            if (empty($contact->getCustomerNumber()) && $contact->getParent() == null) {
                $kundennummer = $this->getNextCustomerNumber();
                $contact->setCustomerNumber(
                    is_numeric($kundennummer) ? $kundennummer : 0000
                );
            }
            try {
                return parent::saveOrUpdate($contact);
            } catch (GuzzleException $e) {
                if ($e->getCode() === 151 || $e->getCode() === 400 || $e->getCode() === 500) {
                    $contact->setId(null);
                    $contact->setUpdateType(SevDeskUpdateType::NEW);
                    return parent::saveOrUpdate($contact);
                }
            }
        }
        return null;
    }

    public function getNextCustomerNumber() {
        return (int) $this->getFromSevDesk(null, Contact::modelName, "Factory/getNextCustomerNumber");
    }

    public function getContactEmail($contactId)
    {
        if ($contactId !== null) {
            /** @var CommunicationWay $object */
            $object = $this->getFromSevDesk(CommunicationWay::class, Contact::modelName,
                "getMainEmail", [], $contactId)[0];
            return $object;
        }
        return null;
    }

    public function getContactTelefon($contactId)
    {
        if ($contactId !== null) {
            /** @var CommunicationWay $object */
            $object = $this->getFromSevDesk(CommunicationWay::class, Contact::modelName,
                "getMainMobile", [], $contactId)[0];
            if ($object == null) {
                /** @var CommunicationWay $object */
                $object = $this->getFromSevDesk(CommunicationWay::class, Contact::modelName,
                    "getMainPhone", [], $contactId)[0];
            }
            return $object;
        }
        return null;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Contact::class, Contact::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return $this->getEntities($id)[0];
    }
}