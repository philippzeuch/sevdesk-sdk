<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Discounts;
use Zeuch\sevDesk\Model\Invoice;
use DateTime;

class DiscountsRepo extends SevDeskRepo
{

    public function createNewInstance() : Discounts
    {
        $discount = new Discounts();
        $discount->setCreate(new DateTime());
        $discount->setRepo($this);

        return $discount;
    }

    /**
     * @param Invoice $invoice
     * @return Discounts[]
     */
    public function findByInvoice(Invoice $invoice)
    {
        return parent::getFromSevDesk(Discounts::class, Invoice::modelName,
            "getDiscounts", [], $invoice->getId());
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Discounts::class, Discounts::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(Discounts::class, Discounts::modelName, "", [], $id);
    }
}