<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Model\CheckAccountTransactionLog;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class CheckAccountTransactionLogRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $checkAccountTransactionLog = new CheckAccountTransactionLog();
        $checkAccountTransactionLog->setUpdateType(SevDeskUpdateType::NEW);
        $checkAccountTransactionLog->setCreate(new DateTime());
        $checkAccountTransactionLog->setRepo($this);

        return $checkAccountTransactionLog;
    }

    /**
     * @param CheckAccountTransaction $transaction
     * @return CheckAccountTransactionLog[]
     */
    public function findByTransaction(CheckAccountTransaction $transaction)
    {
        return parent::getFromSevDesk(CheckAccountTransactionLog::class,
            CheckAccountTransaction::modelName, "getLog", [], $transaction->getId());
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CheckAccountTransactionLog::class,
            CheckAccountTransactionLog::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CheckAccountTransactionLog::class,
            CheckAccountTransactionLog::modelName, "", [], $id);
    }
}