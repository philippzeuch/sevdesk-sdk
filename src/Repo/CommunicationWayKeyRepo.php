<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CommunicationWayKey;
use Zeuch\sevDesk\Utils\CommWayTranslationCode;

class CommunicationWayKeyRepo extends SevDeskRepo
{
    /**
     * Model is read only!
     * @return null
     */
    public function createNewInstance()
    {
        return null;
    }

    public function getMobile()
    {
        return self::getByTranslationCode(CommWayTranslationCode::COMM_WAY_KEY_MOBILE);
    }

    public function getByTranslationCode($code)
    {
        $entities = parent::getFromSevDesk(CommunicationWayKey::class, CommunicationWayKey::modelName, "", [
            'translationCode' => $code
        ]);
        return array_values(array_filter($entities, /** @param CommunicationWayKey $key */ function($key) use ($code) {
            return $key->getTranslationCode() === $code;
        }))[0];
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CommunicationWayKey::class, CommunicationWayKey::modelName, "", [], $id)[0];
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CommunicationWayKey::class, CommunicationWayKey::modelName, $additionalParams);
    }
}