<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\StaticCountry;

class StaticCountryRepo extends SevDeskRepo
{
    public function createNewInstance()
    {
        return new StaticCountry();
    }

    public function getAll()
    {
        return parent::getFromSevDesk(StaticCountry::class, StaticCountry::modelName, "", [], null);
    }

    public function getGermany(): StaticCountry
    {
        return self::get(1);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(StaticCountry::class, StaticCountry::modelName, "", [], $id)[0];
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, StaticCountry::class, StaticCountry::modelName, $additionalParams);
    }
}