<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Order;
use Zeuch\sevDesk\Model\SevClient;
use Zeuch\sevDesk\Utils\OrderType;
use DateTime;
use Zeuch\sevDesk\Utils\TaxType;

class OrderRepo extends SevDeskRepo
{
    /**
     * @return Order
     */
    public function createNewInstance() {
        $entity = new Order();
        $entity->setCreate(new  DateTime());
        $entity->setHeadText("<p style=\"font-size: 1em;\">Sehr geehrte Damen und Herren,</p><p style=\"font-size: 1em;\">vielen Dank für Ihren Auftrag und das damit verbundene Vertrauen!</p>");
        $entity->setFootText("<p style=\"font-size: 1em;\">Für Rückfragen stehen wir Ihnen jederzeit gerne zur Verfügung.<br>Wir bedanken uns sehr für Ihr Vertrauen.</p><p style=\"font-size: 1em;\">Mit freundlichen Grüßen<br>[%KONTAKTPERSON%]</p>");
        /** @var SevClient $sevClient */
        $sevClient = $this->getContainer()->get(SevClientRepo::class)->get();
        $entity->setShowNet($sevClient->getShowNet());
        $entity->setVersion(0);
        $entity->setSmallSettlement(0);
        $entity->setTaxRate(((float) $this->getContainer()->get("MWST")) * 100);
        $entity->setTaxType(TaxType::DEFAULT);
        $entity->setTaxText("Umsatzsteuer ausweisen");
        $entity->setContactPerson($this->getDefaultSevUser());

        $entity->setRepo($this);

        return $entity;
    }

    public function findByOrderNumber(string $orderNumber) : ?Order
    {
        return array_values(array_filter(parent::getFromSevDesk(Order::class, Order::modelName, "", [
            'orderNumber' => $orderNumber
        ]), /** @param Order $entity */ function($entity) use ($orderNumber) {
            return $entity->getOrderNumber() === $orderNumber;
        }))[0];
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        parent::sevDeskSearch($searchTerm, Order::class, Order::modelName, $additionalParams);
    }

    /**
     * @param $id
     * @return Order|null
     */
    public function get($id = null)
    {
        return $this->getFromSevDesk(Order::class, Order::modelName, "", [], $id);
    }

    /**
     * @return string
     */
    public function getNextOrderNumber() {
        return $this->getFromSevDesk(null, Order::modelName, "getCorrectOrderNumber", [
            'type' => OrderType::AUFTRAG
        ]);
    }
}