<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Utils\CheckAccountTransactionStatus;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use Exception;
use GuzzleHttp\Exception\GuzzleException;

class CheckAccountTransactionRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $checkAccountTransaction = new CheckAccountTransaction();
        $checkAccountTransaction->setRepo($this);
        $checkAccountTransaction->setStatus(CheckAccountTransactionStatus::$CREATED);
        $checkAccountTransaction->setCreate(new DateTime());

        $checkAccountTransaction->setValueDate(new DateTime());
        $checkAccountTransaction->setEntryDate(new DateTime());

        return $checkAccountTransaction;
    }


    /**
     * Umbuchung von einem Quellkonto auf ein Zielkonto.
     */
    public function umbuchen(float $betrag, DateTime $datum, CheckAccount $quellKonto, CheckAccount $zielKonto,
                             ?CheckAccountTransaction $quellTransaktion, ?CheckAccountTransaction $zielTransaktion) :?CheckAccountTransaction
    {
        $body = [
            'amount' => $betrag,
            'date'   => $datum->format('Y-m-d\TH:m:s.u\Z'),
            'target' => [
                'objectName' => CheckAccount::modelName,
                'id'         => $zielKonto->getId()
            ],
            'sourceTransaction' => "null",
            'targetTransaction' => "null",
        ];
        if ($quellTransaktion !== null) {
            $body['sourceTransaction'] = [
                'id'         => $quellTransaktion->getId(),
                'objectName' => CheckAccountTransaction::modelName
            ];
        }
        if ($zielTransaktion !== null) {
            $body['targetTransaction'] = [
                'id'         => $zielTransaktion->getId(),
                'objectName' => CheckAccountTransaction::modelName
            ];
        }
        $response = parent::request(SevDeskUpdateType::UPDATE, CheckAccount::modelName, "transfer", [],
            http_build_query($body), $quellKonto->getId());

        return parent::transformResultToEntity($response, CheckAccountTransaction::class);
    }

    /**
     * @param $transaction CheckAccountTransaction
     * @throws Exception
     * @throws GuzzleException
     */
    public function abschliessen($transaction)
    {
        if ($transaction !== null) {
            $remainingAmount = $this->getRemainingAmount($transaction);

            if ($remainingAmount == 0) {
                $this->request(SevDeskUpdateType::UPDATE, CheckAccountTransaction::modelName,
                    "changeStatus", [], "value=400", $transaction->getId());
            } else {
                // TODO: Keine Exception werfen, sondern Transaktion einfach offen lassen
                throw new Exception("Es sind " . $remainingAmount . " EUR noch nicht verbucht.");
            }
        }
    }

    public function getRemainingAmount(CheckAccountTransaction $transaction)
    {
        return $this->getFromSevDesk(null, CheckAccountTransaction::modelName,
            "getRemainingAmount", [], $transaction->getId());
    }

    /**
     * @inheritDoc
     * @return CheckAccountTransaction[]|null
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CheckAccountTransaction::class,
            CheckAccountTransaction::modelName, $additionalParams);
    }

    /**
     * @param string $purpose
     * @return CheckAccountTransaction[]
     */
    public function findByPaymentPurpose(string $purpose)
    {
        return $this->getFromSevDesk(CheckAccountTransaction::class, CheckAccountTransaction::modelName, "", [
            'paymtPurpose' => $purpose
        ]);
    }

    /**
     * Achtung: Scheint wohl nicht so zu funktionieren, wie ich es hier erwarte....
     * @param string $hash
     * @return CheckAccountTransaction[]
     */
    public function findByCompareHash(string $hash)
    {
        return $this->getFromSevDesk(CheckAccountTransaction::class, CheckAccountTransaction::modelName, "", [
            'compareHash' => $hash
        ]);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CheckAccountTransaction::class,
            CheckAccountTransaction::modelName, "", [], $id);
    }
}