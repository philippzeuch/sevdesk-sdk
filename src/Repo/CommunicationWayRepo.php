<?php

namespace Zeuch\sevDesk\Repo;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\CommunicationWay;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;

class CommunicationWayRepo extends SevDeskRepo
{
    /** @var CommunicationWayKeyRepo */
    private $communicationWayKeyRepo;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);
        $this->communicationWayKeyRepo = $container->get(CommunicationWayKeyRepo::class);
    }

    public function createNewInstance(?string $key = null)
    {
        $communicationWay = new CommunicationWay();
        $communicationWay->setCreate(new DateTime());
        $communicationWay->setUpdateType(SevDeskUpdateType::NEW);
        if ($key !== null) {
            $communicationWay->setKey($this->communicationWayKeyRepo->getByTranslationCode($key));
        }

        return $communicationWay;
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CommunicationWay::class, CommunicationWay::modelName, "", [], $id)[0];
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CommunicationWay::class, CommunicationWay::modelName, $additionalParams);
    }

    public function saveOrUpdate($entity)
    {
        try {
            return parent::saveOrUpdate($entity);
        } catch (GuzzleException $e) {
            if ($e->getCode() === 151 || $e->getCode() === 400 || $e->getCode() === 500) {
                $entity->setId(null);
                $entity->setUpdateType(SevDeskUpdateType::NEW);
                return parent::saveOrUpdate($entity);
            }
        }
    }
}