<?php

namespace Zeuch\sevDesk\Repo;

interface IRepo {

    public function createNewInstance();

    public function saveOrUpdate($instance);

    public function get($id = null);

}