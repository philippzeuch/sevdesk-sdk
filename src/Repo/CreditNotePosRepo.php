<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CreditNotePos;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class CreditNotePosRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $creditNotePos = new CreditNotePos();
        $creditNotePos->setCreate(new DateTime());
        $creditNotePos->setUpdateType(SevDeskUpdateType::NEW);
        $creditNotePos->setTaxRate(((float) $this->getContainer()->get("MWST")) * 100);

        $creditNotePos->setRepo($this);

        return $creditNotePos;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CreditNotePos::class, CreditNotePos::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CreditNotePos::class, CreditNotePos::modelName, "", [], $id);
    }
}