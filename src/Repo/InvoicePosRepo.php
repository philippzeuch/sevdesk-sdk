<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Model\InvoicePos;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class InvoicePosRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $invoicePos = new InvoicePos();
        $invoicePos->setCreate(new DateTime());
        $invoicePos->setUpdateType(SevDeskUpdateType::NEW);
        $invoicePos->setTaxRate(((float) $this->getContainer()->get("MWST")) * 100);

        $invoicePos->setRepo($this);

        return $invoicePos;
    }

    /**
     * @param Invoice $invoice
     * @return InvoicePos[]
     */
    public function findByInvoice(Invoice $invoice)
    {
        return parent::getFromSevDesk(InvoicePos::class, Invoice::modelName,
            "getPositions", [], $invoice->getId());
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, InvoicePos::class, InvoicePos::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(InvoicePos::class, InvoicePos::modelName, "", [], $id);
    }
}