<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Category;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use Zeuch\sevDesk\Utils\CategoryTranslationCode;

class CategoryRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $entity = new Category();
        $entity->setCreate(new DateTime());
        $entity->setUpdateType(SevDeskUpdateType::NEW);

        return $entity;
    }

    public function getInvoiceAddressCategory()
    {
        return self::getByTranslationCode(CategoryTranslationCode::CATEGORY_INVOICE_ADDRESS);
    }

    public function getDeliveryAddressCategory()
    {
        return self::getByTranslationCode(CategoryTranslationCode::CATEGORY_DELIVERY_ADDRESS);
    }

    public function getPrivatCategory()
    {
        return self::getByTranslationCode(CategoryTranslationCode::CATEGORY_PRIVAT);
    }

    public function getArbeitCategory()
    {
        return self::getByTranslationCode(CategoryTranslationCode::CATEGORY_WORK);
    }

    /** @return Category */
    public function getByTranslationCode($code)
    {
        $entities = parent::getFromSevDesk(Category::class, Category::modelName, "", [
            'translationCode' => $code
        ]);
        return array_values(array_filter($entities, /** @param Category $category */ function($category) use ($code) {
            return $category->getTranslationCode() === $code;
        }))[0];
    }

    public function getStandardCategoryMock()
    {
        $category = $this->createNewInstance();
        $category->setId(1);
        $category->setTranslationCode(CategoryTranslationCode::CATEGORY_STANDARD);

        return $category;
    }

    public function getKundeCategoryMock()
    {
        $category = $this->createNewInstance();
        $category->setId(3);
        $category->setTranslationCode(CategoryTranslationCode::CATEGORY_CUSTOMER);

        return $category;
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(Category::class, Category::modelName, "", [], $id)[0];
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Category::class, Category::modelName, $additionalParams);
    }
}