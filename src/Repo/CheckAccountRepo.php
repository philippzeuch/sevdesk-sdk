<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CheckAccount;
use DateTime;
use Zeuch\sevDesk\Utils\CheckAccountType;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;

class CheckAccountRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $checkAccount = new CheckAccount();
        $checkAccount->setRepo($this);
        $checkAccount->setCreate(new DateTime());

        return $checkAccount;
    }

    public function getKasse(): ?CheckAccount
    {
        $accounts = $this->getFromSevDesk(CheckAccount::class, CheckAccount::modelName, "", [
            'type' => CheckAccountType::$KASSE
        ]);

        if (!empty($accounts)) {
            return $accounts[0];
        }
        return null;
    }

    public function getByBuchungsKontoNummer(int $nummer): ?CheckAccount
    {
        $found = $this->getFromSevDesk(CheckAccount::class, CheckAccount::modelName, "", [
            'accountingNumber' => $nummer
        ]);

        $found = array_values(array_filter($found, function (CheckAccount $account) use ($nummer) {
            return $account->getAccountingNumber() === $nummer;
        }));

        if (!empty($found)) {
            return $found[0];
        }
        return null;
    }

    public function transaktionenAutomatischZuordnen(CheckAccount $konto): void
    {
        $this->request(SevDeskUpdateType::NEW, CheckAccount::modelName, "mapTransactions",
            [], null, $konto->getId());
        $this->logger->info("Die automatische Zuordnung von Transaktionen wurde in sevDesk angestoßen "
            . "(Zahlungskonto: {kname}).", ['kname' => $konto->getName()]);
        // TODO: Mit der Response noch etwas tun? Es kommt z.B. zurück, wie viele Transaktionen zugeordnet wurden
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CheckAccount::class, CheckAccount::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CheckAccount::class, CheckAccount::modelName, "", [], $id)[0];
    }
}