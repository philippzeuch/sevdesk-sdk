<?php

namespace Zeuch\sevDesk\Repo;

use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;
use Zeuch\sevDesk\Service\SevDeskApiClient;
use Zeuch\sevDesk\Model\SevDeskEntity;
use Zeuch\sevDesk\Utils\ArrayHelper;
use Zeuch\sevDesk\Model\DefaultSevDeskEntity;
use Zeuch\sevDesk\Model\SevUser;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use Composer\CaBundle\CaBundle;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use JsonMapper;
use JsonMapper_Exception;
use Psr\Http\Message\ResponseInterface;
use stdClass;

abstract class SevDeskRepo implements IRepo
{

    /** @var string */
    protected $baseURL;

    /** @var string */
    protected $token;

    /** @var string */
    protected $defaultUserId;

    /** @var LoggerInterface */
    protected $logger;

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->baseURL = $container->get("SEVDESK_BASEURL");
        $this->token = $container->get("SEVDESK_TOKEN");
        $this->defaultUserId = $container->get("SEVDESK_USERID");
        $this->logger = $container->get(LoggerInterface::class);

        if (empty($this->baseURL)) {
            throw new \Exception("Es ist keine Base-Url für die Kommunikation angegeben: 'SEVDESK_BASEURL'.");
        }
        if (empty($this->token)) {
            throw new \Exception("Es ist kein Token für die Kommunikation angegeben: 'SEVDESK_TOKEN'.");
        }
        if (empty($this->defaultUserId)) {
            throw new \Exception("Es ist kein sevDesk User angegeben: 'SEVDESK_USERID'.");
        }
    }


    public function delete(SevDeskEntity $entity)
    {
        $response = self::request(SevDeskUpdateType::REMOVE, $entity::modelName, "", [], null, $entity->getId());
        return $response->getStatusCode() == 200;
    }

    /**
     * @param string $resultType
     * @param string $model
     * @param string $method
     * @param array $queryParams
     * @param mixed $id
     * @return mixed|array
     */
    public function getFromSevDesk($resultType, string $model, string $method = "", array $queryParams = [], $id = -1)
    {
        /** @var $resultType[] $entities */
        $entities = array();

        $queryParams['limit'] = 10000;

        try {
            $response = $this->request('GET', $model, $method, $queryParams, null, $id);
            if ($response !== null && ($response->getStatusCode() == 200 || $response->getStatusCode() == 201)) {
                $stdClass = json_decode($response->getBody()->getContents());

                if (!empty($resultType)) {
                    $jsonDecoder = $this->getJsonMapper();
                    $jsonDecoder->classMap[DefaultSevDeskEntity::class] = function ($class, $jvalue) {
                        return self::isReference($jvalue) ? $class : "Zeuch\\sevDesk\\Model\\$jvalue->objectName";
                    };
                    try {
                        if (!$stdClass->objects instanceof stdClass) {
                            /** @var SevDeskEntity[] $entities */
                            $entities = $jsonDecoder->mapArray($stdClass->objects, array(), $resultType);
                            foreach ($entities as $entity) {
                                if (!is_null($entity) && $entity instanceof SevDeskEntity) {
                                    $entity->setRepo($this);
                                }
                            }
                        } else {
                            // Einzelnes Objekt
                            /** @var SevDeskEntity $entity */
                            $entity = $jsonDecoder->map($stdClass->objects, new $resultType());
                            if ($entity !== null) {
                                $entity->setRepo($this);
                            }
                            $entities[] = $entity;
                        }
                    } catch (JsonMapper_Exception $e) {
                    }
                } else {
                    $entities = $stdClass->objects;
                }
            }
        } catch (GuzzleException $e) {
            error_log($e->getMessage(), 0);
        }

        return $entities;
    }

    /**
     * SevDeskEntities nach sevDesk übertragen (PUT/POST)
     * Diese Methode darf nur in den Repos selbst genutzt werden!
     * @param SevDeskEntity $entity
     * @param string $model
     * @param string $function
     * @param array $queryParams
     * @param int|null $id
     * @param string $method
     * @return mixed|object|null
     * @throws GuzzleException
     */
    protected function persist(?SevDeskEntity $entity, string $model, string $function = "",
           array $queryParams = [], int $id = null, string $method = SevDeskUpdateType::NEW)
    {
        $returnEntity = null;
        $entityAsArray = null;

        if ($entity !== null) {
            $persistByFunction = array_keys($entity->getPersistByFunction());

            $entityAsArray = array_diff_key($this->entityAsArray($entity), $entity->getExcludedForPersistence());

            foreach ($entityAsArray as $key => $value) {
                if (!empty($value) || $value == "0") {
                    if (in_array($key, $persistByFunction)) {
                        // Gesondert persistieren
                        $entity->persistFunctionField($key);
                    } else {
                        // Entity für die Übertragung normalisieren
                        if ($value instanceof SevDeskEntity) {
                            if ($value->getId() == null) {
                                unset($entityAsArray[$key]);
                            } else {
                                $entityAsArray[$key] = array(
                                    'id' => $value->getId(),
                                    'objectName' => $value->getObjectName()
                                );
                            }
                        } else if ($value instanceof DateTime) {
                            $entityAsArray[$key] = $value->format('Y-m-d\TH:m:s.u\Z');
                        }
                    }
                } else { // Leere Felder ignorieren
                    unset($entityAsArray[$key]);
                }
            }

            if ($method == SevDeskUpdateType::NEW && $entity->getId() !== null) {
                $method = SevDeskUpdateType::UPDATE;
            }

            if ($entity->getUpdateType() !== null) {
                $method = $entity->getUpdateType();
            }
            $entity->setId($method == SevDeskUpdateType::NEW ? null : $entity->getId()); // Die ID wegwerfen wenn neu
        }
        $body = $entityAsArray !== null ? http_build_query($entityAsArray) : "";

        $response = $this->request($method, $model, $function, $queryParams, $body, is_null($id) && !is_null($entity) ? $entity->getId() : $id);

        if ($response != null) {
            $returnEntity = self::transformResultToEntity($response, $entity !== null ? get_class($entity) : $model);
        }

        return $returnEntity;
    }

    /**
     * Eine Anfrage per REST an sevDesk stellen
     * @param string $method
     * @param string $model
     * @param string $function
     * @param array $queryParams
     * @param null $body
     * @param mixed $id
     * @return mixed|ResponseInterface|null
     * @throws GuzzleException
     */
    protected function request(string $method, string $model, string $function = "", array $queryParams = [], $body = null, $id = -1)
    {
        $client = $this->getRestClient();
        $response = null;
        if ($client != null && !empty($method) && !empty($model)) {
            $idString = $id !== null && $id != -1 ? $id . '/' : '';
            $postOrPutRequestOptions = array();
            if ($method == 'POST' || $method == 'PUT') {
                $postOrPutRequestOptions = array(
                    'body' => $body,
                    'headers' => [
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ]
                );
            }
            $response = $client->request($method, $model . '/' . $idString . $function, array_merge([
                'query' => $queryParams
            ], $postOrPutRequestOptions));
        }
        return $response;
    }

    protected function transformResultToEntity($response, $targetEntityName)
    {
        $returnEntity = null;
        if ($response->getStatusCode() == 200 || $response->getStatusCode() == 201) {
            $jsonAsStdClass = json_decode($response->getBody()->getContents());

            if (!empty($jsonAsStdClass) && !empty($jsonAsStdClass->objects)) {
                try {
                    $jsonDecoder = $this->getJsonMapper();
                    $returnEntity = $jsonDecoder->map($jsonAsStdClass->objects, new $targetEntityName());
                    $returnEntity->setRepo($this);
                } catch (JsonMapper_Exception $e) {
                    $returnEntity = $jsonAsStdClass;
                }
            }
        }
        return $returnEntity;
    }

    /**
     * Finde eine SevDesk Entität anhand irgendeiner Zeichenfolge
     * ACHTUNG: Funktioniert nur bei "Contacts, Orders, Invoices, Vouchers, Documents and Parts"
     * @param $searchTerm string
     * @param $additionalParams
     * @return array|mixed
     */
    public abstract function findSevDeskEntity($searchTerm, $additionalParams = []);

    /**
     * ACHTUNG: Funktioniert nur bei "Contacts, Orders, Invoices, Vouchers, Documents and Parts"
     * @param $searchTerm string
     * @param $resultType string
     * @param string $modelName
     * @param array $additionalParams
     * @return array|mixed
     */
    protected function sevDeskSearch($searchTerm, $resultType, $modelName = "", $additionalParams = [])
    {
        return $this->getFromSevDesk($resultType, "Search", "search", [
                'term' => $searchTerm,
                'searchType' => strtoupper($modelName)
            ] + $additionalParams);
    }

    public function getDefaultSevUser() {
        $sevUser = $this->getFromSevDesk(SevUser::class, SevUser::modelName, "", [], $this->defaultUserId);
        return $sevUser[0];
    }

    private function getRestClient()
    {
        $handlerStack = HandlerStack::create();
        $logger = $this->getContainer()->get(LoggerInterface::class);
        $handlerStack->push(Middleware::log($logger, new MessageFormatter(), LogLevel::DEBUG));

        $guzzleClient = new SevDeskApiClient($this->token, [
            'base_uri' => $this->baseURL,
            'handler' => $handlerStack,
            RequestOptions::VERIFY => CaBundle::getSystemCaRootBundlePath()
        ]);

        return $guzzleClient;
    }

    private function entityAsArray($entity)
    {
        $entityAsArray = (array)$entity;

        return ArrayHelper::normalize($entityAsArray);
    }

    /**
     * @param $entity
     * @return SevDeskEntity|mixed
     * @throws GuzzleException
     */
    public function saveOrUpdate($entity) {
        return $this->persist($entity, $entity::modelName);
    }

    protected function getContainer() : ContainerInterface {
        return $this->container;
    }

    /**
     * Prüft, ob das übergebene Objekt nur eine Referenz darstellt oder ein richtiges SevDeskEntity ist
     * @param $entity
     * @return bool
     */
    public static function isReference(?stdClass $entity) : bool
    {
        if ($entity !== null) {
            $array = get_object_vars($entity);
            if (count($array) == 2 && isset($array['id']) && isset($array['objectName'])) {
                return true;
            }
        }
        return false;
    }

    protected function getJsonMapper(): JsonMapper
    {
        $mapper = new JsonMapper();
        $logger = $this->getContainer()->get(LoggerInterface::class);
        $mapper->setLogger($logger);
        $mapper->undefinedPropertyHandler = function($object, $key, $jvalue) use ($logger) {
            $logger->log(
                LogLevel::DEBUG,
                'Property {property} does not exist in {class}',
                array('property' => $key, 'class' => get_class($object))
            );
        };
        $mapper->bIgnoreVisibility = true;

        return $mapper;
    }
}