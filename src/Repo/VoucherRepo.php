<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Model\Voucher;
use Zeuch\sevDesk\Utils\BookingType;
use Zeuch\sevDesk\Utils\TaxType;
use Zeuch\sevDesk\Utils\VoucherStatus;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class VoucherRepo extends SevDeskRepo
{
    /**
     * @return Voucher
     */
    public function createNewInstance()
    {
        $voucher = new Voucher();
        $voucher->setCreate(new DateTime());
        $voucher->setVoucherDate(new DateTime());
        $voucher->setUpdateType(SevDeskUpdateType::NEW);
        $voucher->setStatus(VoucherStatus::$CREATED);
        $voucher->setCreateUser($this->getDefaultSevUser());
        $voucher->setVoucherType("VOU");
        $voucher->setTaxType(TaxType::DEFAULT);
        $voucher->setCreditDebit("C");

        $voucher->setRepo($this);

        return $voucher;
    }

    public function changeStatus(Voucher $voucher, $status)
    {
        $response = parent::request(SevDeskUpdateType::UPDATE, Voucher::modelName, "changeStatus", [],
            "value=" . $status, $voucher->getId());

        if ($response !== null) {
            return $response->getStatusCode();
        }
        return 500;
    }

    public function bookAmount(Voucher $voucher, float $amount, DateTime $date, ?CheckAccount $account,
                               ?CheckAccountTransaction $transaction, ?string $bookingType = BookingType::N)
    {
        $response = parent::request(SevDeskUpdateType::UPDATE, Voucher::modelName, "bookAmmount", [], http_build_query([
            'ammount' => $amount,
            'type' => $bookingType,
            'date' => $date->format('Y-m-d\TH:m:s.u\Z'),
            'checkAccount' => [
                'id' => $account == null ?: $account->getId(),
                'objectName' => CheckAccount::modelName
            ],
            'checkAccountTransaction' => [
                'id' => $transaction == null ?: $transaction->getId(),
                'objectName' => CheckAccountTransaction::modelName
            ],
            'createFeed' => true
        ]), $voucher->getId());


        if ($response !== null) {
            return $response->getStatusCode();
        }
        return 500;
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Voucher::class, Voucher::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(Voucher::class, Voucher::modelName, "", [], $id);
    }
}