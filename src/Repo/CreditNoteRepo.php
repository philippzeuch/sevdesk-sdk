<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Model\CreditNote;
use Zeuch\sevDesk\Model\Invoice;
use Zeuch\sevDesk\Model\SevClient;
use Zeuch\sevDesk\Utils\BookingType;
use Zeuch\sevDesk\Utils\InvoiceStatus;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Zeuch\sevDesk\Utils\TaxType;

class CreditNoteRepo extends SevDeskRepo
{

    /**
     * @return CreditNote
     */
    public function createNewInstance()
    {
        $creditNote = new CreditNote();
        $creditNote->setCreditNoteDate(new DateTime());
        $creditNote->setUpdateType(SevDeskUpdateType::NEW);
        $creditNote->setStatus(InvoiceStatus::$CREATED);
        $creditNote->setCreditNoteType("CN");
        $creditNote->setCreateUser($this->getDefaultSevUser());
        $creditNote->setContactPerson($this->getDefaultSevUser());
        $creditNote->setTaxType(TaxType::DEFAULT);
        $creditNote->setTaxRate(((float)$this->getContainer()->get("MWST")) * 100);

        $creditNote->setHeadText('<p>Sehr geehrte Damen und Herren,</p><p>gemäß unserer Vereinbarung schreiben wir Ihnen folgende Leistungen gut:<br></p>');
        $creditNote->setFootText('<p>Für Rückfragen stehen wir Ihnen jederzeit gerne zur Verfügung.<br>Wir bedanken uns sehr für Ihr Vertrauen.</p><p>Mit freundlichen Grüßen<br>[%KONTAKTPERSON%]</p>');
        $creditNote->setCreditNoteNumber($this->getNextCreditNoteNumber());
        /** @var SevClient $sevClient */
        $sevClient = $this->getContainer()->get(SevClientRepo::class)->get();
        $creditNote->setShowNet($sevClient->getShowNet());

        $creditNote->setRepo($this);

        return $creditNote;
    }

    public function findByInvoice(Invoice $invoice)
    {
        $gutschriften = parent::getFromSevDesk(CreditNote::class, CreditNote::modelName, "", [
            'refSrcInvoice' => [
                'id' => $invoice->getId(),
                'objectName' => $invoice->getObjectName()
            ]
        ]);

        return array_values(array_filter($gutschriften, /** @param CreditNote $gutschrift */ function ($gutschrift) use ($invoice) {
            return $gutschrift->getRefSrcInvoice() !== null && $gutschrift->getRefSrcInvoice()->getId() == $invoice->getId();
        }));
    }

    /**
     * @param $creditNoteNumber
     * @return CreditNote[]|array
     */
    public function findByCreditNoteNummer($creditNoteNumber)
    {
        return parent::getFromSevDesk(CreditNote::class, CreditNote::modelName, "", [
            'creditNoteNumber' => $creditNoteNumber
        ]);
    }

    public function bookAmount(CreditNote $creditNote, float $amount, DateTime $date, CheckAccount $account,
                               ?CheckAccountTransaction $zielTransaktion, string $type = BookingType::N)
    {
        $body = [
            'ammount' => $amount,
            'date' => $date->format('Y-m-d\TH:m:s.u\Z'),
            'checkAccount' => [
                'objectName' => CheckAccount::modelName,
                'id' => $account->getId()
            ],
            'createFeed' => true
        ];
        if ($zielTransaktion !== null) {
            $body['checkAccountTransaction'] = [
                'id' => $zielTransaktion->getId(),
                'objectName' => CheckAccountTransaction::modelName
            ];
        }

        $response = parent::request(SevDeskUpdateType::UPDATE, CreditNote::modelName, "bookAmmount", [],
            http_build_query($body), $creditNote->getId());

        if ($response !== null) {
            return $response->getStatusCode();
        }
        return 500;
    }

    /**
     * @param CreditNote $creditNote
     * @return mixed|ResponseInterface|null
     * @throws GuzzleException
     */
    public function markAsSent(CreditNote $creditNote)
    {
        /** @var CreditNote $gutschein */
        $gutschein = $this->persist($creditNote, CreditNote::modelName,
            "markAsSent", [], $creditNote->getId(), SevDeskUpdateType::UPDATE);
        $gutschein->setSendType("VPDF");
        $gutschein->setUpdateType(SevDeskUpdateType::UPDATE);
        return $this->saveOrUpdate($gutschein);
    }

    /**
     * @return mixed|ResponseInterface|null
     */
    private function getNextCreditNoteNumber()
    {
        return $this->getFromSevDesk(null, CreditNote::modelName, "Factory/getNextCreditNoteNumber");
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, CreditNote::class, CreditNote::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(CreditNote::class, CreditNote::modelName, "", [], $id);
    }
}