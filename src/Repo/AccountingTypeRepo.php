<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\AccountingType;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class AccountingTypeRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $accountingType = new AccountingType();
        $accountingType->setCreate(new DateTime());
        $accountingType->setUpdateType(SevDeskUpdateType::NEW);

        return $accountingType;
    }

    /**
     * @return AccountingType
     */
    public function getErloesschmaelerung()
    {
        $types = parent::getFromSevDesk(AccountingType::class, AccountingType::modelName, "", [
            'translationCode' => 'ACCOUNTING_TYPE_SALES_DEDUCTION',
            'name' => 'Erlösschmälerung'
        ]);
        if ($types !== null && count($types) > 0) {
            return $types[0];
        }
        return null;
    }

    /**
     * @return AccountingType
     */
    public function getEigenesBuchungsKontoByNummer($nummer) : ?AccountingType
    {
        /** @var AccountingType[] $types */
        $types = parent::getFromSevDesk(AccountingType::class, AccountingType::modelName, "", [
            'onlyOwn' => true,
            'embed'   => 'accountingSystemNumber'
        ]);
        if ($types !== null && count($types) > 0) {
            foreach ($types as $type) {
                $systemNumber = $type->getAccountingSystemNumber();
                if ($systemNumber->getNumber() == $nummer) {
                    return $type;
                }
            }
        }
        return null;
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, AccountingType::class, AccountingType::modelName, $additionalParams);
    }

    /**
     * @param $id
     * @return AccountingType[]
     */
    public function get($id = null)
    {
        return parent::getFromSevDesk(AccountingType::class, AccountingType::modelName, "", [], $id);
    }
}