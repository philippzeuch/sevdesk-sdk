<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Part;
use Zeuch\sevDesk\Model\SevSequence;

class SevSequenceRepo extends SevDeskRepo
{

    /**
     * Das SevSequence Model ist read-only.
     * @return null
     */
    public function createNewInstance()
    {
        return null;
    }

    /**
     * @param bool $increase
     * @return SevSequence
     */
    public function getPartNumberSequence()
    {
        /** @var SevSequence $sequence */
        $sequence = parent::getFromSevDesk(SevSequence::class, SevSequence::modelName, "Factory/getByType", [
            'objectType' => Part::modelName,
            'format' => 'NUMBER'
        ], null)[0];

        return $sequence;
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(SevSequence::class, SevSequence::modelName, "", [], $id)[0];
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, SevSequence::class, SevSequence::modelName, $additionalParams);
    }
}