<?php

namespace Zeuch\sevDesk\Repo;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\Part;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;

class PartRepo extends SevDeskRepo
{
    /** @var UnityRepo */
    private $unityRepo;

    /** @var CategoryRepo */
    private $categoryRepo;

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
        $this->unityRepo = $c->get(UnityRepo::class);
        $this->categoryRepo = $c->get(CategoryRepo::class);
    }

    public function createNewInstance() : Part
    {
        $part = new Part();
        $this->init($part);

        return $part;
    }

    public function init($instance)
    {
        $instance->setCreate(new DateTime());
        $instance->setTaxRate(((float) $this->getContainer()->get("MWST")) * 100);
        $instance->setUnity($this->unityRepo->getPieceUnity());
        $instance->setCategory($this->categoryRepo->getStandardCategoryMock());
        $instance->setStock(0);

        return $instance;
    }

    /**
     * @param float $bruttoPreis
     * @return Part|null
     */
    public function getVersandkostenDePart($bruttoPreis)
    {
        $entities = parent::getFromSevDesk(Part::class, Part::modelName, "", [
            'partNumber' => 'VKO',
            'priceGross' => $bruttoPreis
        ]);
        return array_values(array_filter($entities, /** @param Part $entity */ function($entity) use ($bruttoPreis) {
            return $entity->getPartNumber() === "VKO" && $entity->getPriceGross() === $bruttoPreis;
        }))[0];
    }

    public function getNextRandomPartNumber($staticPart)
    {
        $partNumber = $staticPart . "-" . self::generateRandomString(4);

        return $this->checkPartNumberAvailability($partNumber) ? $partNumber : $this->getNextRandomPartNumber($staticPart);
    }

    public function checkPartNumberAvailability($partNumber):bool
    {
        return json_decode(parent::request(SevDeskUpdateType::NEW, Part::modelName, "Mapper/checkPartNumberAvailability", [], http_build_query([
            'partNumber' => $partNumber,
            'originPart' => 'null'
        ]))->getBody()->getContents())->objects;
    }

    /**
     * @param null $id
     * @return Part[]
     */
    public function get($id = null)
    {
        return parent::getFromSevDesk(Part::class, Part::modelName, "", [], $id);
    }

    /**
     * Finde eine SevDesk Entität anhand irgendeiner Zeichenfolge
     * @param $searchTerm string
     * @param $additionalParams
     * @return array|mixed
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Part::class, Part::modelName, $additionalParams);
    }

    private static function generateRandomString($length) {
        $include_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        /* Uncomment below to include symbols */
        /* $include_chars .= "[{(!@#$%^/&*_+;?\:)}]"; */
        $charLength = strlen($include_chars);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $include_chars [rand(0, $charLength - 1)];
        }
        return $randomString;
    }

    public function saveOrUpdate($entity)
    {
        try {
            return parent::saveOrUpdate($entity);
        } catch (GuzzleException $e) {
            if ($e->getCode() === 151 || $e->getCode() === 400 || $e->getCode() === 500) {
                $entity->setId(null);
                $entity->setUpdateType(SevDeskUpdateType::NEW);
                return parent::saveOrUpdate($entity);
            }
        }
    }
}