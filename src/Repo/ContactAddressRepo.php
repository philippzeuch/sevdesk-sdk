<?php

namespace Zeuch\sevDesk\Repo;

use Psr\Container\ContainerInterface;
use Zeuch\sevDesk\Model\Category;
use Zeuch\sevDesk\Model\Contact;
use Zeuch\sevDesk\Model\ContactAddress;
use Zeuch\sevDesk\Utils\Cache\Cache;
use Zeuch\sevDesk\Utils\Cache\CacheManager;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;
use GuzzleHttp\Exception\GuzzleException;

class ContactAddressRepo extends SevDeskRepo
{
    /** * @var CategoryRepo */
    private $categoryRepo;

    /** @var Cache */
    private $cache;

    public function __construct(ContainerInterface $c)
    {
        parent::__construct($c);
        $this->cache = $c->get(CacheManager::class)->getCache(self::class);
        $this->categoryRepo = $c->get(CategoryRepo::class);
    }

    public function createNewInstance()
    {
        $entity = new ContactAddress();
        $entity->setCreate(new DateTime());
        $entity->setUpdateType(SevDeskUpdateType::NEW);

        return $entity;
    }

    public function getInvoiceAddress(Contact $contact): ?ContactAddress
    {
        $category = $this->categoryRepo->getInvoiceAddressCategory();
        return $this->getAddressByContactAndCategory($contact, $category);
    }

    public function getDeliveryAddress(Contact $contact): ?ContactAddress
    {
        $category = $this->categoryRepo->getDeliveryAddressCategory();
        return $this->getAddressByContactAndCategory($contact, $category);
    }

    public function getWorkAddress(Contact $contact): ?ContactAddress
    {
        $category = $this->categoryRepo->getArbeitCategory();
        return $this->getAddressByContactAndCategory($contact, $category);
    }

    public function getPrivatAddress(Contact $contact): ?ContactAddress
    {
        $category = $this->categoryRepo->getPrivatCategory();
        return $this->getAddressByContactAndCategory($contact, $category);
    }

    /** @return ContactAddress|null */
    public function get($id = null)
    {
        return parent::getFromSevDesk(ContactAddress::class, ContactAddress::modelName, "", [], $id)[0];
    }

    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, ContactAddress::class, ContactAddress::modelName, $additionalParams);
    }

    private function initAddressCacheByContact(Contact $contact)
    {
        if (empty($this->cache->getContent($contact->getId()))) {
            $this->cache->setContent($contact->getId(), serialize(
                parent::getFromSevDesk(ContactAddress::class, Contact::modelName, "getAddresses", [], $contact->getId())
            ));
        }
    }

    public function destroyCache()
    {
        $this->cache->destroy();
    }

    /**
     * @param Contact $contact
     * @param Category $category
     * @return ContactAddress|null
     */
    private function getAddressByContactAndCategory(Contact $contact, Category $category): ?ContactAddress
    {
        $this->initAddressCacheByContact($contact);
        return array_values(array_filter(unserialize($this->cache->getContent($contact->getId())),
            /** @param ContactAddress $address */ function ($address) use ($category) {
                return $address->getCategory(false)->getId() === $category->getId();
            }))[0];
    }

    public function saveOrUpdate($entity)
    {
        try {
            return parent::saveOrUpdate($entity);
        } catch (GuzzleException $e) {
            if ($e->getCode() === 151 || $e->getCode() === 400 || $e->getCode() === 500) {
                $entity->setId(null);
                $entity->setUpdateType(SevDeskUpdateType::NEW);
                return parent::saveOrUpdate($entity);
            }
        }
    }
}