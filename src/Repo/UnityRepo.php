<?php

namespace Zeuch\sevDesk\Repo;

use Zeuch\sevDesk\Model\Unity;
use Zeuch\sevDesk\Utils\SevDeskUpdateType;
use DateTime;

class UnityRepo extends SevDeskRepo
{

    public function createNewInstance()
    {
        $unity = new Unity();
        $unity->setCreate(new DateTime());
        $unity->setUpdateType(SevDeskUpdateType::NEW);

        $unity->setRepo($this);

        return $unity;
    }

    public function getPieceUnity()
    {
        $untiy = $this->createNewInstance();
        $untiy->setId(1);

        return $untiy;
    }

    /**
     * @inheritDoc
     */
    public function findSevDeskEntity($searchTerm, $additionalParams = [])
    {
        return parent::sevDeskSearch($searchTerm, Unity::class, Unity::modelName, $additionalParams);
    }

    public function get($id = null)
    {
        return parent::getFromSevDesk(Unity::class, Unity::modelName, "", [], $id);
    }
}