# sevdesk-sdk

Ein SDK für die öffentliche HTTP API von sevDesk.
Ich nutze es für Integrationen mit Shopware und [Zettle](https://gitlab.com/philippzeuch/sevdesk-izettle-integration).

Jeder Beitrag in Form von Pull Requests ist herzlich willkommen.

Unter `/example` findet man die `integration.example.php` als Beispiel für die Integration in ein vorhandenes Projekt.

Folgende Properties gibt es:

|Property|Beschreibung|
|----|----|
|MWST|Mehrwertsteuer (z.B. "0.19")|
|LOG_LEVEL|Level des Loggings (Dieses Projekt verwendet Monolog)|
|LOG_PATH|(Optional) Absoluter Pfad für die "Log files". Zusätzlich wird in die Konsole geloggt.|
|SEVDESK_BASEURL|Die URL von der API|
|SEVDESK_TOKEN|Der Token (Ist in sevDesk am Benutzer einzusehen)|
|SEVDESK_USERID|Die technische ID des Benutzers, in welchem Kontext die CRUD Aktionen des SDKs durchgeführt werden sollen. Wird hier genutzt: `\Zeuch\sevDesk\Repo\SevDeskRepo::getDefaultSevUser`|
|SEVDESK_RECHNUNGSNUMMER_REGEX|Optional. Um Rechnungsnummern in Strings zu erkennen, kann man hier einen Regex eintragen. Nutzen kann man dan z.B. diese Methode: `\Zeuch\sevDesk\Service\InvoiceService::extrahiereRechnungsNummern`
|SEVDESK_AUFTRAGSNUMMER_REGEX|Optional. Um Auftragsnummern in Strings zu erkennen, kann man hier einen Regex eintragen. Nutzen kann man dan z.B. diese Methode: `\Zeuch\sevDesk\Service\InvoiceService::extrahiereAuftragsNummern`