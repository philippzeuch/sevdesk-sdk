<?php

use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Utils\CheckAccountTransactionStatus;
use Zeuch\sevDesk\Repo\CheckAccountRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;

include __DIR__ . '/../integration.example.php';

// Prerequisites
/** @var CheckAccountTransactionRepo $transactionRepo */
$transactionRepo = $container->get(CheckAccountTransactionRepo::class);
/** @var CheckAccountRepo $checkAccountRepo */
$checkAccountRepo = $container->get(CheckAccountRepo::class);




$idAltesKonto = null;
$idNeuesKonto = null;

// CheckAccountTransactions von einem CheckAccount in einen anderen CheckAccount verschieben
if ($idAltesKonto !== null && $idNeuesKonto !== null) {
    /** @var CheckAccountTransaction[] $transactions */
    $transactions = $transactionRepo->getFromSevDesk(CheckAccountTransaction::class, CheckAccountTransaction::modelName, "", [
        'checkAccount' => [
            'id' => $idAltesKonto,
            'objectName' => CheckAccount::modelName
        ]
    ]);
    $transactions = array_filter($transactions, function ($transaction) {
        return $transaction->getStatus() !== CheckAccountTransactionStatus::$CREATED;
    });
    $anzahl = count($transactions);
    $verarbeitet = array();
    $altesKonto = $checkAccountRepo->get($idAltesKonto);
    $neuesKonto = $checkAccountRepo->get($idNeuesKonto);
    foreach ($transactions as $transaction) {
        $transaction->setCheckAccount($neuesKonto);
        $sevDeskEntity = $transactionRepo->saveOrUpdate($transaction);
        if ($sevDeskEntity !== null) {
            $verarbeitet[] = $transaction->getId();
        }
    }
    echo count($verarbeitet) . " von " . $anzahl . " Transaktionen verarbeitet!\\n";
    echo "Verarbeitete ids: " . implode(",", $verarbeitet);
}