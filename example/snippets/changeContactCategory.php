<?php

use Zeuch\sevDesk\Repo\CategoryRepo;
use Zeuch\sevDesk\Repo\ContactRepo;

include __DIR__ . '/../integration.example.php';

// Prerequisites
/** @var ContactRepo $contactRepo */
$contactRepo = $container->get(ContactRepo::class);
/** @var CategoryRepo $categoryRepo */
$categoryRepo = $container->get(CategoryRepo::class);




// Kategorien der Kontakte aktualisieren, welche "Sonstige Dokumente" als Kategorie haben
$contacts = $contactRepo->getEntities();
$updatedIds = array();
foreach ($contacts as $contact) {
    if (empty($contact->getCategory(true)->getId()) || $contact->getCategory(true)->getId() === 16) {
        $contact->setCategory($categoryRepo->getKundeCategoryMock());

        $contactRepo->saveOrUpdate($contact);
        echo "Kontakt 'id: {$contact->getId()}; kdNr: {$contact->getCustomerNumber()}' wurde aktualisiert!\n";
        $updatedIds[] = $contact->getId();
    }
}
echo "Es wurden " . count($updatedIds) . " von " . count($contacts) . " Kontakten aktualisiert!\n" . implode(",", $updatedIds);