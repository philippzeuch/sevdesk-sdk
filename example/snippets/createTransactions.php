<?php

use Zeuch\sevDesk\Repo\CheckAccountRepo;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;

require __DIR__ . '/../integration.example.php';

// Prerequisites
/** @var CheckAccountTransactionRepo $transactionRepo */
$transactionRepo = $container->get(CheckAccountTransactionRepo::class);
/** @var CheckAccountRepo $checkAccountRepo */
$checkAccountRepo = $container->get(CheckAccountRepo::class);


// Transaktionen für einen bestimmten CheckAccount erstellen
$kontoId = null;

if ($kontoId !== null) {
    $checkAccount = $checkAccountRepo->get($kontoId);
    if ($checkAccount !== null) {
        $transaction = $transactionRepo->createNewInstance();
        $transaction->setCheckAccount($checkAccount);

        $transaction->setStatus("100");
        $transaction->setPaymtPurpose("224912.2020.71X");
        $transaction->setEntryDate(new DateTime("06/08/2020"));
        $transaction->setValueDate(new DateTime("06/08/2020"));
        $transaction->setAmount(-39.56);

        $transactionRepo->saveOrUpdate($transaction);
    }
}