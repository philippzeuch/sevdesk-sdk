<?php

use Zeuch\sevDesk\Repo\InvoiceRepo;

require __DIR__ . '/../integration.example.php';

// Prerequisites
/** @var InvoiceRepo $invoiceRepo */
$invoiceRepo = $container->get(InvoiceRepo::class);

$rechnungenString = "Hier kann ein langer String mit Rechnungsnummern stehen...";
$rechnungen = explode(" ", $rechnungenString);
$zaehler = 0;

foreach ($rechnungen as $rechnungsNummer) {
    $rechnung = $invoiceRepo->findByRechnungsNummer($rechnungsNummer);

    if ($rechnung !== null) {
        $invoiceRepo->changeStatus($rechnung->getId());
        $zaehler++;
    }
}

echo $zaehler . " von " . count($rechnungen) . " geöffnet!";