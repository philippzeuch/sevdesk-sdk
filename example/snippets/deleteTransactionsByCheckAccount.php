<?php

use Zeuch\sevDesk\Model\CheckAccount;
use Zeuch\sevDesk\Model\CheckAccountTransaction;
use Zeuch\sevDesk\Repo\CheckAccountTransactionRepo;

include __DIR__ . '/../integration.example.php';

// Prerequisites
/** @var CheckAccountTransactionRepo $transactionRepo */
$transactionRepo = $container->get(CheckAccountTransactionRepo::class);




// ID vom CheckAccount
$id = null;

// CheckAccountTransactions von einem CheckAccount löschen
if ($id !== null) {
    /** @var CheckAccountTransaction[] $newTransactions */
    $newTransactions = $transactionRepo->getFromSevDesk(CheckAccountTransaction::class, CheckAccountTransaction::modelName, "", [
        'checkAccount' => [
            'id' => $id,
            'objectName' => CheckAccount::modelName
        ]
    ]);
    $newTransactions = array_filter($newTransactions, function ($transaction) {
        return true; //$transaction->getValueDate() < new DateTime("01/01/2020");
    });
    $anzahl = count($newTransactions);
    $geloescht = array();
    foreach ($newTransactions as $transaction) {
        $removalStatus = $transactionRepo->delete($transaction);
        if ($removalStatus) {
            $geloescht[] = $transaction->getId();
        }
    }
    echo count($geloescht) . " von " . $anzahl . " Transaktionen gelöscht!\\n";
    echo "Gelöschte ids: " . implode(",", $geloescht);
}