<?php

return [
    // Folgende Properties müssen auf jeden Fall in den DI-Container hineingereicht werden
    'MWST' => 0.19,
    'LOG_LEVEL' => "INFO",
    'LOG_PATH' => "/var/logs",

    'SEVDESK_BASEURL' => '',
    'SEVDESK_TOKEN'   => '',
    'SEVDESK_USERID'  => '',

    // Optional
    'SEVDESK_RECHNUNGSNUMMER_REGEX' => 'RE[-—–]\d*[-—–]\d*',
    'SEVDESK_AUFTRAGSNUMMER_REGEX'  => 'AB[-—–]\d*'
];