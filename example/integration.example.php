<?php
require __DIR__ . '/../vendor/autoload.php';

use DI\ContainerBuilder;
use Zeuch\sevDesk\DefaultSevDeskApplicationContext;

$containerBuilder = new ContainerBuilder();
$containerBuilder->useAutowiring(false);
$containerBuilder->useAnnotations(false);
$containerBuilder->addDefinitions(DefaultSevDeskApplicationContext::get());
$containerBuilder->addDefinitions(__DIR__ . "/config.example.php");

$container = $containerBuilder->build();

// Get the desired classes from the container, work with the predefined Services and Repos and have fun...